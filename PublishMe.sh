rm -R publish_web_win
rm -R publish_web_linux
rm -R publish_api_win
rm -R publish_api_linux

cd MountainAir.Web
dotnet publish -c release -o ../publish_web_win -r win10-x64
dotnet publish -c release -o ../publish_web_linux -r linux-x64

cd ../MountainAir.Api

dotnet publish -c release -o ../publish_api_win -r win10-x64
dotnet publish -c release -o ../publish_api_linux -r linux-x64
