﻿using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.AzureStorage;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using NLog;

namespace MountainAir.Api.Controllers;

public class PhotosController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();
    private readonly IAzureBlobManager _azureBlobManager;

    public PhotosController(IAzureBlobManager azureBlobManager, ApplicationDbContext context)
        : base(context)
    {
        this._azureBlobManager = azureBlobManager;
    }

    [ProducesResponseType(typeof(IEnumerable<BlobViewModel>), StatusCodes.Status200OK)]
    [HttpGet, Route("{id}")]
    public async Task<IActionResult> Get(Guid id)
    {
        var results = await _azureBlobManager.GetEstimateFiles(id);
        //var data = results.ToList();

        //var items = data.Where(i => i.Name.EndsWith(".pdf", StringComparison.InvariantCultureIgnoreCase)).ToArray();

        //if (items.Length > 0)
        //{
        //    foreach (var item in items)
        //        data.Remove(item);
        //}

        return Ok(results);
    }

    [ProducesResponseType(typeof(BlobViewModel), StatusCodes.Status200OK)]
    [HttpPost, Route("{estimateId:guid}")]
    public async Task<IActionResult> Post(Guid estimateId, IFormFile file)
    {
        try
        {
            if(file == null)
                return BadRequest("File is null");

            var result = await _azureBlobManager.AddFile(estimateId, file);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return BadRequest(ex);
        }
    }

    [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
    [HttpDelete, Route("{id}/{filename}")]
    public async Task<IActionResult> Delete(Guid id, string filename)
    {
        try
        {
            if (string.IsNullOrEmpty(filename))
                return BadRequest();

            if (!await _azureBlobManager.FileExists(id, filename))
            {
                return Ok();
            }

            await _azureBlobManager.Delete(id, filename);

            return Ok();
        }
        catch (Exception ex)
        {
            //HandleError(ex);
            //string msg = ex.GetBaseException().Message;
            return Ok();
        }
    }
}