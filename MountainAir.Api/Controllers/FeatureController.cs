﻿using EFCore.BulkExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using NLog;

namespace MountainAir.Api.Controllers;

public class FeatureController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();

    public FeatureController(
        ApplicationDbContext context)
        : base(context)
    {
    }
    
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<Feature>), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get()
    {
        try
        {
            var types = await Context.Features.AsNoTracking().ToListAsync();

            return Ok(types);
        }
        catch (Exception e)
        {
            return HandleError(e);
        }
    }
    
    [HttpGet, Route("{id:int}")]
    [ProducesResponseType(typeof(IEnumerable<WarrantyType>), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get(int id)
    {
        try
        {
            var type = await Context.Features
                .AsNoTracking()
                .FirstOrDefaultAsync(i => i.FeatureId == id);

            return Ok(type);
        }
        catch (Exception e)
        {
            return HandleError(e);
        }
    }
    
    [HttpPut, HttpPost]
    [ProducesResponseType(typeof(Feature), StatusCodes.Status200OK)]
    public async Task<IActionResult> AddOrUpdate([FromBody] Feature model)
    {
        try
        {
            var exists = Context.Features.Any(i => i.FeatureId == model.FeatureId);
            if (exists)
            {
                Context.Features.Update(model);
            }
            else
            {
                Context.Features.Add(model);
            }
            await Context.SaveChangesAsync();
            return Ok(model);
        }
        catch (Exception ex)
        {
            return HandleError(ex, model);
        }
    }

    [HttpDelete("{id:int}")]
    [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
    public async Task<IActionResult> Delete(int id)
    {
        try
        {
            await Context.Features.Where(i => i.FeatureId == id).BatchDeleteAsync();
            return Ok();
        }
        catch (Exception ex)
        {
            HandleError(ex);
        }

        return Ok();
    }
}