﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using NLog;

namespace MountainAir.Api.Controllers;

public class EstimateOptionsController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();

    public EstimateOptionsController(ApplicationDbContext context) : base(context)
    {

    }

    [HttpGet("{id}")]
    [ProducesResponseType(typeof(IEnumerable<EstimateOption>), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get(Guid id)
    {
        try
        {
            var data = await Context.EstimateOptions.Where(i => i.EstimateId == id).ToListAsync();

            if (data != null)
                return Ok(data);
            return NotFound();
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    [HttpPut]
    [HttpPost]
    [ProducesResponseType(typeof(EstimateOption), StatusCodes.Status200OK)]
    public async Task<IActionResult> Put([FromBody]EstimateOption value)
    {
        try
        {
            if (string.IsNullOrEmpty(value.OptionName))
                value.OptionName = $"Option {value.OptionNumber + 1}";

            var exists = Context.EstimateOptions.Any(i => i.EstimateOptionId == value.EstimateOptionId);
            if (exists)
            {
                Context.EstimateOptions.Update(value);
            }
            else
            {
                Context.EstimateOptions.Add(value);
            }

            await Context.SaveChangesAsync();

            return Ok(value);
        }
        catch (Exception ex)
        {
            return HandleError(ex, value);
        }
    }

    [HttpDelete("{id:guid}")]
    [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
    public async Task<IActionResult> Delete(Guid id)
    {
        try
        {
            await Context.DeleteEstimateOptionAsync(id);

            return Ok();
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }
}