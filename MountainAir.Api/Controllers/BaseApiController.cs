﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Security;
using Newtonsoft.Json;

namespace MountainAir.Api.Controllers;

[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
[Route("api/[Controller]")]
[ApiController]
[Produces("application/json")]
public abstract class BaseApiController : Controller
{
    protected ApplicationDbContext Context { get; }
    private ApplicationUser _appUser;

    protected BaseApiController()
        : this(new ApplicationDbContext())
    {
    }

    protected BaseApiController(ApplicationDbContext context)
    {
        this.Context = context;
    }

    protected abstract NLog.ILogger Logger { get;  }

    protected async Task<ApplicationUser> GetCurrentUser() => _appUser ??= await Context.Users.FirstAsync(u => u.UserName == User.Identity.Name);

    protected ApplicationUser CurrentUser
    {
        get { return _appUser ??= Context.Users.First(u => u.UserName == User.Identity.Name); }
    }

    protected IActionResult HandleError(Exception ex, object postedObject = null)
    {
        var e = ex.GetBaseException();

        if (postedObject != null)
        {
            string json = JsonConvert.SerializeObject(postedObject);
            Logger.Warn($"Error posting object: '{json}'");
        }

        Logger.Error(e, $"Error in operation: {ex.GetBaseException().Message}");

        return BadRequest($"Error processing request: {e.Message}");
    }
}