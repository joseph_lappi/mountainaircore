﻿using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Services;
using NLog;

namespace MountainAir.Api.Controllers;

public class WarrantyController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();
    private readonly IWarrantyService _warrantyService;

    public WarrantyController(
        IWarrantyService warrantyService, 
        ApplicationDbContext context)
        : base(context)
    {
        _warrantyService = warrantyService;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<Warranty>), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get(Guid estimateId)
    {
        try
        {
            var data = await _warrantyService.GetWarrantiesAsync(estimateId);

            return Ok(data);
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }
    
    [HttpPut]
    [HttpPost]
    [ProducesResponseType(typeof(Warranty), StatusCodes.Status200OK)]
    public async Task<IActionResult> CreateOrUpdate([FromBody] Warranty model)
    {
        try
        {
            var svc = new WarrantyService(Context);

            var exists = await svc.WarrantyExists(model.Id);
            
            var task = exists ? svc.UpdateWarrantyAsync(model, CurrentUser) : svc.AddWarrantyAsync(model, CurrentUser);

            var data = await task;

            return Ok(data);
        }
        catch (Exception ex)
        {
            return HandleError(ex, model);
        }
    }

    [HttpDelete, Route("{id:guid}")]
    public async Task<IActionResult> Delete(Guid id)
    {
        try
        {
            await _warrantyService.DeleteWarrantyAsync(id);

            return Ok();
        }
        catch (Exception ex)
        {
            Logger.Error(ex, $"Error deleting warranty: {id}");
            return Ok();
        }
    }
}