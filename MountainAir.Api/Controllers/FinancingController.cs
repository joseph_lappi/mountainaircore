﻿using EFCore.BulkExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using NLog;

namespace MountainAir.Api.Controllers;

public class FinancingController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();

    public FinancingController(
        ApplicationDbContext context)
        : base(context)
    {
    }
    
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<Financing>), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get()
    {
        try
        {
            var types = await Context.WarrantyTypes.AsNoTracking().ToListAsync();

            return Ok(types);
        }
        catch (Exception e)
        {
            return HandleError(e);
        }
    }
    
    [HttpGet, Route("{id:int}")]
    [ProducesResponseType(typeof(Financing), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get(int id)
    {
        try
        {
            var type = await Context.Financings
                .AsNoTracking()
                .FirstOrDefaultAsync(i => i.FinancingId == id);

            return Ok(type);
        }
        catch (Exception e)
        {
            return HandleError(e);
        }
    }
    
    [HttpPut, HttpPost]
    [ProducesResponseType(typeof(Financing), StatusCodes.Status200OK)]
    public async Task<IActionResult> AddOrUpdate([FromBody] Financing model)
    {
        try
        {
            var exists = Context.Financings.Any(i => i.FinancingId == model.FinancingId);
            if (exists)
            {
                Context.Financings.Update(model);
            }
            else
            {
                Context.Financings.Add(model);
            }
            await Context.SaveChangesAsync();
            return Ok(model);
        }
        catch (Exception ex)
        {
            return HandleError(ex, model);
        }
    }

    [HttpDelete("{id:int}")]
    [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
    public async Task<IActionResult> Delete(int id)
    {
        try
        {
            await Context.Financings.Where(i => i.FinancingId == id).BatchDeleteAsync();
            return Ok();
        }
        catch (Exception ex)
        {
            HandleError(ex);
        }

        return Ok();
    }
}