﻿using EFCore.BulkExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using NLog;

namespace MountainAir.Api.Controllers;

public class RoomsController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();

    public RoomsController(ApplicationDbContext context) : base(context)
    {

    }

    [ProducesResponseType(typeof(Room), StatusCodes.Status200OK)]
    [HttpGet("{id}")]
    public async Task<IActionResult> Get(Guid id)
    {
        try
        {
            var data = await Context.Rooms.FirstOrDefaultAsync(i => i.RoomId == id);
            if (data != null)
                return Ok(data);
            return NotFound();
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    [ProducesResponseType(typeof(Room), StatusCodes.Status200OK)]
    [HttpPut]
    [HttpPost]
    public async Task<IActionResult> Put([FromBody] Room value)
    {
        try
        {
            var exists = await Context.Rooms.AnyAsync(i => i.RoomId == value.RoomId);
            if (exists)
            {
                Context.Rooms.Update(value);
            }
            else
            {
                Context.Rooms.Add(value);
            }

            await Context.SaveChangesAsync();

            return Ok(value);
        }
        catch (Exception ex)
        {
            return HandleError(ex, value);
        }
    }

    [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
    [HttpDelete("{roomId:guid}")]
    public async Task<IActionResult> DeleteRoom(Guid roomId)
    {
        try
        {
            await using (var trans = await Context.Database.BeginTransactionAsync())
            {
                await Context.Doors.Where(d => d.RoomId == roomId).BatchDeleteAsync();
                await Context.Windows.Where(w => w.RoomId == roomId).BatchDeleteAsync();
                await Context.Rooms.Where(r => r.RoomId == roomId).BatchDeleteAsync();

                await trans.CommitAsync();
            }
            return Ok();
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }
}