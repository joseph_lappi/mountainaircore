﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.AzureStorage;
using MountainAir.Common.Data.Models;
using NLog;

namespace MountainAir.Api.Controllers;

[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
[Route("api/[controller]/{id:guid}")]
public class SignaturesController : Controller
{
    private readonly NLog.ILogger _logger = LogManager.GetCurrentClassLogger();
    private readonly ISignatureManager _signatureManager;

    public SignaturesController(ISignatureManager signatureManager)
    {
        this._signatureManager = signatureManager;
    }
        
    [Produces("application/json")]
    [ProducesResponseType(typeof(byte[]), StatusCodes.Status200OK)]
    [HttpGet]
    public async Task<IActionResult> Get(Guid id)
    {
        try
        {
            var results = await _signatureManager.GetAsync(id);

            return Json(new
            {
                SigData = Convert.ToBase64String(results)
            });
            //return File(results, "image/png", $"{id}.png");
        }
        catch (Exception ex)
        {
            _logger.Error(ex, $"Error in Get operation: {ex.GetBaseException().Message}");
            return BadRequest(ex.GetBaseException().Message);
        }
    }
        
    [Produces("application/json")]
    [ProducesResponseType(typeof(BlobViewModel), StatusCodes.Status200OK)]
    [HttpPost]
    public async Task<IActionResult> Post(Guid id, IFormFile file)
    {
        try
        {
            var result = await _signatureManager.AddAsync(id, file);

            return Ok(result);
        }
        catch (Exception ex)
        {
            _logger.Error(ex, $"Error in post operation: {ex.GetBaseException().Message}");
            return BadRequest(ex.GetBaseException().Message);
        }
    }

    [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
    [HttpDelete]
    public async Task<IActionResult> Delete(Guid id, [FromQuery] string ext = "png")
    {
        try
        {
            if (!await _signatureManager.FileExistsAsync(id, ext))
            {
                return Ok();
            }

            var result = await _signatureManager.DeleteAsync(id, ext);

            return Ok(result.Message);
        }
        catch (Exception ex)
        {
            _logger.Error(ex, $"Error in delete operation: {ex.GetBaseException().Message}");
            return Ok();
        }
    }
}