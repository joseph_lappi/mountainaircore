﻿using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.AzureStorage;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using NLog;

namespace MountainAir.Api.Controllers;

public class AttachmentsController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();
    private readonly IAzureBlobManager _azureStorageManager;

    public AttachmentsController(
        IAzureBlobManager azureStorageManager,
        ApplicationDbContext applicationDbContext) 
        : base(applicationDbContext)
    {
        _azureStorageManager = azureStorageManager;
    }
    
    [ProducesResponseType(typeof(FileStreamResult), StatusCodes.Status200OK)]
    [HttpGet, Route("{estimateId:guid}/{fileName}")]
    public async Task<IActionResult> Download(Guid estimateId, string fileName)
    {
        try
        {
            var response = await _azureStorageManager.GetEstimateFile(estimateId, fileName);
            
            return File(response.Value.Content, response.Value.ContentType, fileName);
        }
        catch (Exception ex)
        {
            Logger.Error(ex, $"Error downloading attachment for estimate {estimateId} - {fileName}");
        }

        return BadRequest();
    }

    [ProducesResponseType(typeof(IEnumerable<BlobViewModel>), StatusCodes.Status200OK)]
    [HttpGet, Route("{id}")]
    public async Task<IActionResult> Get(Guid id)
    {
        var results = await _azureStorageManager.GetEstimateFiles(id);
        var data = results.ToList();

        var items = data.Where(i => i.Name.EndsWith(".pdf", StringComparison.InvariantCultureIgnoreCase)).ToArray();

        if (items.Length > 0)
        {
            foreach (var item in items)
                data.Remove(item);
        }

        return Ok(data);
    }

    [ProducesResponseType(typeof(IEnumerable<BlobViewModel>), StatusCodes.Status200OK)]
    [HttpPost, Route("{estimateId:guid}")]
    public async Task<IActionResult> Post(Guid estimateId)
    {
        try
        {
            if (!this.Request.Form.Files.Any()) 
                return Ok();

            var retval = new List<BlobViewModel>();

            foreach (var formFile in this.Request.Form.Files)
            {
                var response = await _azureStorageManager.AddFile(estimateId, formFile);
                retval.Add(response);
            }

            return Ok(retval);
        }
        catch (Exception ex)
        {
            return BadRequest(ex);
        }
    }

    [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
    [HttpDelete, Route("{id}/{filename}")]
    public async Task<IActionResult> Delete(Guid id, string filename)
    {
        try
        {
            if (string.IsNullOrEmpty(filename))
                return BadRequest();

            if (!await _azureStorageManager.FileExists(id, filename))
            {
                return Ok();
            }

            await _azureStorageManager.Delete(id, filename);

            return Ok();
        }
        catch (Exception ex)
        {
            HandleError(ex);
            //string msg = ex.GetBaseException().Message;
            return Ok();
        }
    }
}