﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using NLog;

namespace MountainAir.Api.Controllers;

public class WindowsController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();

    public WindowsController(ApplicationDbContext context) : base(context)
    {

    }

    [ProducesResponseType(typeof(Window), StatusCodes.Status200OK)]
    [HttpPut]
    [HttpPost]
    public async Task<IActionResult> Put([FromBody]Window value)
    {
        try
        {
            var exists = Context.Windows.Any(w => w.WindowId == value.WindowId);
            if (exists)
            {
                Context.Windows.Update(value);
            }
            else
            {
                Context.Windows.Add(value);
            }

            await Context.SaveChangesAsync();

            return Ok(value);
        }
        catch (Exception ex)
        {
            return HandleError(ex, value);
        }
    }

    [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
    [HttpDelete, Route("{id:guid}")]
    public async Task<IActionResult> Delete(Guid id)
    {
        try
        {
            var data = await Context.Windows.FirstOrDefaultAsync(i => i.WindowId == id);
                
            if (data != null)
            {
                Context.Windows.Remove(data);
                await Context.SaveChangesAsync();
            }

            return Ok();
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

}