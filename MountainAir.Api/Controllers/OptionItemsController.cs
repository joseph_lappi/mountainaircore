﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using NLog;

namespace MountainAir.Api.Controllers;

public class OptionItemsController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();

    public OptionItemsController(ApplicationDbContext context) : base(context)
    {

    }

    [HttpGet("{id}")]
    [ProducesResponseType(typeof(IEnumerable<OptionItem>), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get(Guid id)
    {
        try
        {
            var data = await Context.OptionItems.Where(i => i.EstimateOptionId == id)
                .Include(i => i.OptionItemMaterials).ToListAsync();

            if (data != null)
                return Ok(data);
            return NotFound();
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    [HttpPut]
    [HttpPost]
    [ProducesResponseType(typeof(OptionItem), StatusCodes.Status200OK)]
    public async Task<IActionResult> Put([FromBody]OptionItem value)
    {
        try
        {
            var exists = Context.OptionItems.Any(i => i.OptionItemId == value.OptionItemId);
            if (exists)
            {
                Context.OptionItems.Update(value);
            }
            else
            {
                Context.OptionItems.Add(value);
            }
            await Context.SaveChangesAsync();
            return Ok(value);
        }
        catch (Exception ex)
        {
            return HandleError(ex, value);
        }
    }

    [HttpDelete("{id:guid}")]
    [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
    public async Task<IActionResult> Delete(Guid id)
    {
        try
        {
            await Context.DeleteOptionItemAsync(id);
            return Ok();
        }
        catch (Exception ex)
        {
            HandleError(ex);
        }

        return Ok();
    }
}