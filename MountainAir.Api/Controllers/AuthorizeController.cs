﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using MountainAir.Api.Extensions;
using MountainAir.Api.Security;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using MountainAir.Common.Services;
using NLog;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;

namespace MountainAir.Api.Controllers;

[AllowAnonymous]
public class AuthorizeController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IEmailSender _emailSender;
    private readonly TokenSettings _tokenSettings = new();
    private readonly ApplicationDbContext _appContext;

    public AuthorizeController(
        ApplicationDbContext applicationDbContext,
        IEmailSender emailSender,
        UserManager<ApplicationUser> userManager)
    {
        _appContext = applicationDbContext;
        _userManager = userManager;
        _emailSender = emailSender;
    }
        
    [HttpGet]
    public async Task<IActionResult> Login()
    {
        try
        {
            var request = HttpContext.Request;
            var authHeader = request.Headers["Authorization"];
            var authHeaderVal = AuthenticationHeaderValue.Parse(authHeader);

            // RFC 2617 sec 1.2, "scheme" name is case-insensitive
            if (authHeaderVal.Scheme.Equals("basic", StringComparison.OrdinalIgnoreCase) && authHeaderVal.Parameter != null)
            {
                var encoding = Encoding.GetEncoding("iso-8859-1");
                string credentials = encoding.GetString(Convert.FromBase64String(authHeaderVal.Parameter));

                int separator = credentials.IndexOf(':');
                string name = credentials.Substring(0, separator);
                string password = credentials.Substring(separator + 1);

                var user = await CheckPassword(name, password);

                if (user == null)
                {
                    await WriteLogAttempt(name, false);
                    return Unauthorized();
                }
                else
                {
                    await WriteLogAttempt(name, true);

                    var token = GenerateToken(user);
                    var refreshToken = GenerateRefreshToken();

                    user.RefreshToken = refreshToken;

                    await _userManager.UpdateAsync(user);

                    var roles = await _userManager.GetRolesAsync(user);

                    return Ok(new AuthenticateResponse(user, token.Token, token.TokenExpiration, refreshToken, roles.ToArray()));
                }
            }

            return Unauthorized();
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    public class RefreshRequest
    {
        public string Token { get; set; }

        public string RefreshToken { get; set; }
    }

    [HttpPost, Route("/api/Refresh")]
    public async Task<IActionResult> Refresh([FromBody] RefreshRequest value)
    {
        try
        {
            string token = value.Token;
            string refreshToken = value.RefreshToken;

            var principal = GetPrincipalFromExpiredToken(token);

            string userName = principal.Identity?.Name;

            Logger.Info($"Attempting to find user fro refresh token: {userName}");

            var user = await _appContext.Users.FirstOrDefaultAsync(i => i.UserName == userName && i.RefreshToken == refreshToken);

            if (user == null)
            {
	            user = await _appContext.Users.FirstOrDefaultAsync(i => i.UserName == userName);
	            if (user == null)
	            {
		            throw new SecurityTokenException($"Unable to find user for refresh token: {userName}");
	            }
            }

            var newJwtToken = GenerateToken(user);

            var newRefreshToken = GenerateRefreshToken();

            user.RefreshToken = newRefreshToken;

            await _userManager.UpdateAsync(user);

            return new ObjectResult(new TokenResponse
            {
                Token = newJwtToken.Token,
                TokenExpiration = newJwtToken.TokenExpiration,
                RefreshToken = newRefreshToken
            });
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    [HttpPost, Route("/api/ResetPassword")]
    public async Task<IActionResult> ResetPassword([FromBody] dynamic message)
    {
        string email = message.Email;

        string responseText = "Please check your email to reset your password.";

        var user = await _userManager.FindByEmailAsync(email);

        if (user == null)
        {
            // Don't reveal that the user does not exist or is not confirmed
            return BadRequest("Invalid Email");
        }

        var code = _userManager.GeneratePasswordResetTokenAsync(user).Result;

        var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Scheme);

        await _emailSender.SendPasswordResetEmailAsync(user.Email, callbackUrl);

        return Ok(responseText);
    }

    private (string Token, long TokenExpiration) GenerateToken(ApplicationUser user)
    {
        var handler = new JwtSecurityTokenHandler();

        var identity = new ClaimsIdentity(
            new GenericIdentity(user.UserName, "TokenAuth"),
            new[] { new Claim("ID", user.Id.ToString()) }
        );

        var expires = DateTime.Now + _tokenSettings.ExpiresSpan;

        var securityToken = handler.CreateToken(new SecurityTokenDescriptor
        {
            Issuer = TokenSettings.Issuer,
            Audience = TokenSettings.Audience,
            SigningCredentials = TokenSettings.SigningCredentials,
            Subject = identity,
            Expires = expires
        });

        var expTime = EpochTime.GetIntDate(securityToken.ValidTo);

        string token = handler.WriteToken(securityToken);

        return (token, expTime);
    }

    private string GenerateRefreshToken()
    {
        var randomNumber = new byte[32];
        using var rng = RandomNumberGenerator.Create();
        rng.GetBytes(randomNumber);
        return Convert.ToBase64String(randomNumber);
    }

    private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
    {
        var tokenValidationParameters = new TokenValidationParameters
        {
            ValidateAudience = false, //you might want to validate the audience and issuer depending on your use case
            ValidateIssuer = false,
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = TokenSettings.Key, 
            ValidateLifetime = false //here we are saying that we don't care about the token's expiration date
        };

        var tokenHandler = new JwtSecurityTokenHandler();
        var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out SecurityToken securityToken);
        var jwtSecurityToken = securityToken as JwtSecurityToken;
        if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
            throw new SecurityTokenException("Invalid token");

        return principal;
    }

    private string GetIpAddress()
    {
        // get source ip address for the current request
        if (Request.Headers.ContainsKey("X-Forwarded-For"))
            return Request.Headers["X-Forwarded-For"];
        else
            return HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
    }

    private async Task WriteLogAttempt(string username, bool success)
    {
        try
        {
            var attempt = new LogAttempt
            {
                LogDate = DateTime.Now,
                Success = success,
                UserName = username
            };
            Context.LogAttempts.Add(attempt);
            await Context.SaveChangesAsync();
        }
        catch (Exception ex)
        {
            Logger.Error(ex, "Error writing log attempt");
        }
    }

    private async Task<ApplicationUser> CheckPassword(string username, string password)
    {
        var user = await _userManager.FindByNameAsync(username);
        var isValid = await _userManager.CheckPasswordAsync(user, password);

        if (isValid)
        {
            if (user.Active)
                return user;
        }
        return null;
    }
}