﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Extensions;
using NLog;

namespace MountainAir.Api.Controllers;

public class EstimatesController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();

    public EstimatesController(ApplicationDbContext context) : base(context)
    {

    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<Estimate>), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get()
    {
        try
        {
            var currentUser = await GetCurrentUser();

            Estimate[] data = Context.Estimates.Where(e => e.AssignedTo == currentUser.Id && e.Active).ToArray();

            //if (await _userManager.IsInRoleAsync(currentUser, "CanViewAllProposals"))
            //{
            //    data = Context.Estimates.Where(e => e.Active).ToArray();
            //}
            //else
            //{
            //    data = Context.Estimates.Where(e => e.AssignedTo == currentUser.Id && e.Active).ToArray();
            //}

            return Ok(data);
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    [HttpGet, Route("{id:guid}")]
    [ProducesResponseType(typeof(Estimate), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get(Guid id)
    {
        try
        {
            Context.Database.SetCommandTimeout(120);

            var data = await Context.Estimates.Where(i => i.EstimateId == id)
                .Include(w => w.Warranties)
                .Include(r => r.Rooms)
                .ThenInclude(door => door.Doors)
                .Include(r => r.Rooms)
                .ThenInclude(window => window.Windows)
                .Include(o => o.EstimateOptions)
                .ThenInclude(items => items.OptionItems)
                .ThenInclude(mat => mat.OptionItemMaterials)
                .FirstOrDefaultAsync();

            if (data != null)
            {
                foreach (var est in data.EstimateOptions)
                {
                    if (string.IsNullOrEmpty(est.OptionName))
                        est.OptionName = $"Option {est.OptionNumber + 1}";
                }
                return Ok(data);
            }
            return NotFound();
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    [HttpPut]
    [HttpPost]
    [ProducesResponseType(typeof(Estimate), StatusCodes.Status200OK)]
    public async Task<IActionResult> CreateOrUpdateEstimate([FromBody]Estimate value)
    {
        try
        {
            var currentUser = await GetCurrentUser();
            var id = currentUser.Id;
            var estimate = await Context.Estimates.FirstOrDefaultAsync(e => e.EstimateId == value.EstimateId);
            if (estimate != null)
            {
                var createdBy = estimate.CreatedBy;
                var createdDate = estimate.CreatedDate;

                value.CopyProperties(estimate);

                estimate.CreatedBy = createdBy;
                estimate.CreatedDate = createdDate;
                estimate.UpdatedBy = id;
                estimate.UpdatedDate = DateTime.Now;

                Context.Estimates.Update(estimate);
                await Context.SaveChangesAsync();
                return Ok(estimate);
            }
            else
            {
                value.AssignedTo = value.CreatedBy;
                value.CreatedBy = value.CreatedBy;

                if (value.AssignedTo == Guid.Empty)
                    value.AssignedTo = value.CreatedBy;

                Context.Estimates.Add(value);
                await Context.SaveChangesAsync();

                return Ok(value);
            }

        }
        catch (Exception ex)
        {
            return HandleError(ex, value);
        }
    }

    //[HttpGet("IndexAttachments")]
    //private async Task<IActionResult> IndexAttachments()
    //{
    //    var mgr = CreateAttachmentManager();

    //    var containers = await mgr.GetAllContainers();

    //    var containerConfig = new BulkConfig
    //    {
    //        CalculateStats = true
    //    };

    //    var filesConfig = new BulkConfig
    //    {
    //        CalculateStats = true
    //    };

    //    var files = new List<AzureContainerFile>();

    //    foreach (var container in containers.ToList())
    //    {
    //        files.AddRange(container.Files);
    //    }

    //    await Context.BulkInsertOrUpdateAsync(containers.ToList(), containerConfig);

    //    await Context.BulkInsertOrUpdateAsync(files, filesConfig);

    //    var data = new
    //    {
    //        ContainersInserted = containerConfig.StatsInfo.StatsNumberInserted,
    //        ContainersUpdated = containerConfig.StatsInfo.StatsNumberUpdated,
    //        FilesInserted = filesConfig.StatsInfo.StatsNumberInserted,
    //        FilesUpdated = filesConfig.StatsInfo.StatsNumberUpdated
    //    };

    //    return Ok(data);
    //}
}