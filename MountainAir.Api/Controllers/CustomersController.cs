﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MountainAir.Api.Controllers;

public class CustomersController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();

    private const string TestUserRole = "TestUser";
    private readonly UserManager<ApplicationUser> _userManager;

    public CustomersController(UserManager<ApplicationUser> userManager, ApplicationDbContext context) : base(context)
    {
        _userManager = userManager;
    }

    private async Task<bool> IsTestUser()
    {
        var user = await GetCurrentUser();

        return await _userManager.IsInRoleAsync(user, TestUserRole);
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<Customer>), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get()
    {
        try
        {
            if(await IsTestUser())
            {
                var data = await Context.Customers.Where(c => c.TestData == true).Include(c => c.Addresses).ToListAsync();
                return Ok(data);
            }
            else
            {
                var data = await Context.Customers.Where(c => c.TestData != true).Include(c => c.Addresses).ToListAsync();
                return Ok(data);
            }

        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    [HttpGet, Route("{id}")]
    [ProducesResponseType(typeof(Customer), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get(Guid id)
    {
        try
        {
            var data = await Context.Customers.Include(c => c.Addresses).FirstOrDefaultAsync(i => i.CustomerId == id);

            if (data != null)
                return Ok(data);

            return NotFound();
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }
        
    [HttpPut]
    [HttpPost]
    [ProducesResponseType(typeof(Customer), StatusCodes.Status200OK)]
    public async Task<IActionResult> Put([FromBody]Customer value)
    {
        try
        {
            var item = await Context.Customers.FirstOrDefaultAsync(i => i.CustomerId == value.CustomerId);

            if (item != null)
            {
                item.CompanyName = value.CompanyName;
                item.Email = value.Email;
                item.FirstName = value.FirstName;
                item.LastName = value.LastName;
                item.PhoneNumber1 = value.PhoneNumber1;
                item.PhoneNumber2 = value.PhoneNumber2;

                if (await IsTestUser())
                {
                    item.TestData = true;
                }

                Context.Customers.Update(item);
                    
            }
            else
            {
                if (value.CustomerId == Guid.Empty)
                {
                    throw new ApplicationException($"Unable to add address with customerId: {value.CustomerId}; {value.LastName}, {value.FirstName}, {value.CompanyName}");
                }

                item = new Customer
                {
                    CustomerId = value.CustomerId,
                    CompanyName = value.CompanyName,
                    Email = value.Email,
                    FirstName = value.FirstName,
                    LastName = value.LastName,
                    PhoneNumber1 = value.PhoneNumber1,
                    PhoneNumber2 = value.PhoneNumber2
                };
                    
                if (await IsTestUser())
                {
                    item.TestData = true;
                }
                    
                Context.Customers.Add(item);
            }

            await Context.SaveChangesAsync();

            return Ok(item);
        }
        catch (Exception ex)
        {
            return HandleError(ex, value);
        }
    }
}