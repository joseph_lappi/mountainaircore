﻿using EFCore.BulkExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using NLog;

namespace MountainAir.Api.Controllers;

public class PaymentMethodController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();

    public PaymentMethodController(
        ApplicationDbContext context)
        : base(context)
    {
    }
    
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<PaymentMethod>), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get()
    {
        try
        {
            var types = await Context.PaymentMethods.AsNoTracking().ToListAsync();

            return Ok(types);
        }
        catch (Exception e)
        {
            return HandleError(e);
        }
    }
    
    [HttpGet, Route("{id:int}")]
    [ProducesResponseType(typeof(PaymentMethod), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get(int id)
    {
        try
        {
            var type = await Context.PaymentMethods
                .AsNoTracking()
                .FirstOrDefaultAsync(i => i.PaymentId == id);

            return Ok(type);
        }
        catch (Exception e)
        {
            return HandleError(e);
        }
    }
    
    [HttpPut, HttpPost]
    [ProducesResponseType(typeof(PaymentMethod), StatusCodes.Status200OK)]
    public async Task<IActionResult> AddOrUpdate([FromBody] PaymentMethod model)
    {
        try
        {
            var exists = Context.PaymentMethods.Any(i => i.PaymentId == model.PaymentId);
            if (exists)
            {
                Context.PaymentMethods.Update(model);
            }
            else
            {
                Context.PaymentMethods.Add(model);
            }
            await Context.SaveChangesAsync();
            return Ok(model);
        }
        catch (Exception ex)
        {
            return HandleError(ex, model);
        }
    }

    [HttpDelete("{id:int}")]
    [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
    public async Task<IActionResult> Delete(int id)
    {
        try
        {
            await Context.PaymentMethods.Where(i => i.PaymentId == id).BatchDeleteAsync();
            return Ok();
        }
        catch (Exception ex)
        {
            HandleError(ex);
        }

        return Ok();
    }
}