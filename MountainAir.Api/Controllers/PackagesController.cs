﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using NLog;

namespace MountainAir.Api.Controllers;

public class PackagesController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();

    public PackagesController(ApplicationDbContext context) : base(context)
    {

    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<Package>), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get()
    {
        try
        {
            var packages = await GetPackagesAsync();

            return Ok(packages);
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    private async Task<List<Package>> GetPackagesAsync()
    {
        var packages = await Context.Packages
            .Include(p => p.PackageItems.Where(pi => pi.CatalogItem.Active))
            .ThenInclude(pi => pi.PackageMaterials.Where(m => m.CatalogItem.Active))
            .Where(package => package.Active)
            .ToListAsync();

        return packages;
    }

    [HttpGet("{packageId}")]
    [ProducesResponseType(typeof(Package), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get(int packageId)
    {
        try
        {
            var package = await Context.Packages
                .Include(i => i.PackageItems)
                .ThenInclude(u => u.PackageMaterials)
                .Where(i => i.PackageId == packageId)
                .FirstOrDefaultAsync();

            if (package != null)
            {
                return Ok(package);
            }

            return NotFound();
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }
}