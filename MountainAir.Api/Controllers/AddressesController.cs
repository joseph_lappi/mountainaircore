﻿using EFCore.BulkExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using NLog;

namespace MountainAir.Api.Controllers;

public class AddressesController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();

    public AddressesController(ApplicationDbContext context) : base(context)
    {

    }

    /// <summary>
    /// Gets addresses for a customer.
    /// </summary>
    /// <param name="id">CustomerId</param>
    /// <returns></returns>
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<Address>), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get(Guid id)
    {
        try
        {
            var data = await Context.Addresses.Where(a => a.CustomerId == id).ToListAsync();

            return Ok(data);
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    [HttpPut]
    [HttpPost]
    [ProducesResponseType(typeof(Address), StatusCodes.Status200OK)]
    public async Task<IActionResult> Put([FromBody]Address value)
    {
        try
        {
            var currentRow = await Context.Addresses.FirstOrDefaultAsync(c => c.AddressId == value.AddressId);

            if (currentRow != null)
            {
                currentRow.AddressLine1 = value.AddressLine1;
                currentRow.AddressLine2 = value.AddressLine2;
                currentRow.City = value.City;
                currentRow.State = value.State;
                currentRow.Zip = value.Zip;

                Context.Addresses.Update(currentRow);
            }
            else
            {
                if (value.AddressId == Guid.Empty || value.CustomerId == Guid.Empty)
                {
                    throw new ApplicationException($"Unable to add address with addressId: {value.AddressId} or customerId: {value.CustomerId}");
                }

                currentRow = new Address
                {
                    AddressId = value.AddressId,
                    CustomerId = value.CustomerId,
                    AddressLine1 = value.AddressLine1,
                    AddressLine2 = value.AddressLine2,
                    City = value.City,
                    State = value.State,
                    Zip = value.Zip
                };

                Context.Addresses.Add(currentRow);
            }
                
            await Context.SaveChangesAsync();

            return Ok(currentRow);
        }
        catch (Exception ex)
        {
            return HandleError(ex, value);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    [HttpDelete, Route("{id:guid}")]
    [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
    public async Task<IActionResult> Delete(Guid id)
    {
        try
        {
            await Context.Addresses.Where(i => i.AddressId == id).BatchDeleteAsync();
            return Ok();
        }
        catch (Exception ex)
        {
            return HandleError(ex, id);
        }
    }
}