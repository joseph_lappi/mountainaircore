﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using NLog;
using System.Text;

namespace MountainAir.Api.Controllers;

/// <summary>
/// Used to administrate users through the API. 
/// </summary>
public class UserController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();
    
    private readonly UserManager<ApplicationUser> _userManager;

    public UserController(UserManager<ApplicationUser> userManager,
        ApplicationDbContext context) : base(context)
    {
        this._userManager = userManager;
    }

    [ProducesResponseType(typeof(IEnumerable<UserModel>), StatusCodes.Status200OK)]
    [HttpGet]
    public async Task<IActionResult> Get()
    {
        try
        {
            var retval = await _userManager.Users.Select(i => new UserModel 
            { 
                FirstName = i.FirstName, 
                LastName = i.LastName, 
                Active = i.Active, 
                Id = i.Id 
            }).ToListAsync();

            return Ok(retval);
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    [ProducesResponseType(typeof(UserModel), StatusCodes.Status200OK)]
    [HttpGet, Route("{id:guid}")]
    public async Task<IActionResult> Get(Guid id)
    {
        try
        {
            var data = await Context.Users.AsNoTracking().FirstOrDefaultAsync(i => i.Id == id);

            if (data != null)
                return Ok(new UserModel(data));

            return NotFound();
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    [ProducesResponseType(typeof(UserModel), StatusCodes.Status200OK)]
    [HttpPut]
    public async Task<IActionResult> Put([FromBody]UserModel value)
    {
        try
        {
            var user = await _userManager.FindByIdAsync(value.Id.ToString());

            if (user != null)
            {
                user.DefaultLaborRate = value.DefaultLaborRate;
                user.DefaultProfitMargin = value.DefaultProfitMargin;
                user.DefaultSalesTax = value.DefaultSalesTax;
                user.Email = value.Email;
                user.FirstName = value.FirstName;
                user.LastName = value.LastName;
                user.PhoneNumber = value.PhoneNumber;

                var result = await _userManager.UpdateAsync(user);

                if(!result.Succeeded)
                {
                    string message = CollectErrors(result);
                    throw new ApplicationException(message);
                }
                
                if (!string.IsNullOrEmpty(value.NewPassword) && !string.IsNullOrEmpty(value.OldPassword))
                {
                    var changePasswordResult = await _userManager.ChangePasswordAsync(user, value.OldPassword, value.NewPassword);

                    if (!changePasswordResult.Succeeded)
                    {
                        string message = CollectErrors(changePasswordResult);
                        throw new ApplicationException(message);
                    }
                }

                user = await _userManager.FindByIdAsync(user.Id.ToString());

                return Ok(new UserModel(user));
            }
            return NotFound();

        }
        catch (Exception ex)
        {
            return HandleError(ex, value);
        }
    }
    
    [ProducesResponseType(typeof(UserModel), StatusCodes.Status200OK)]
    [HttpPost]
    public async Task<IActionResult> Post([FromBody]UserModel value)
    {
        try
        {
            var user = new ApplicationUser
            {
                DefaultLaborRate = value.DefaultLaborRate,
                DefaultProfitMargin = value.DefaultProfitMargin,
                DefaultSalesTax = value.DefaultSalesTax,
                Email = value.Email,
                UserName = value.UserName,
                FirstName = value.FirstName,
                LastName = value.LastName,
                PhoneNumber = value.PhoneNumber
            };

            var result = await _userManager.CreateAsync(user, value.NewPassword);

            if(!result.Succeeded)
            {
                string message = CollectErrors(result);
                throw new ApplicationException(message);
            }

            user = await _userManager.FindByIdAsync(user.Id.ToString());

            return Ok(new UserModel(user));
        }
        catch (Exception ex)
        {
            return HandleError(ex, value);
        }
    }

    private string CollectErrors(IdentityResult result)
    {
        StringBuilder sb = new StringBuilder();
        foreach(var error in result.Errors)
        {
            sb.Append($"{error.Description};");
        }
        return sb.ToString();
    }
}