﻿using EFCore.BulkExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using NLog;

namespace MountainAir.Api.Controllers;

public class OptionItemMaterialController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();

    public OptionItemMaterialController(ApplicationDbContext context) : base(context)
    {

    }

    [HttpGet("{id}")]
    [ProducesResponseType(typeof(IEnumerable<OptionItemMaterial>), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get(Guid id)
    {
        try
        {
            var data = await Context.OptionItemMaterials.Where(i => i.OptionItemId == id).ToListAsync();
            if (data != null)
                return Ok(data);
            return NotFound();
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    [HttpPut]
    [HttpPost]
    [ProducesResponseType(typeof(OptionItemMaterial), StatusCodes.Status200OK)]
    public async Task<IActionResult> Put([FromBody]OptionItemMaterial value)
    {
        try
        {
            var exists = await Context.OptionItemMaterials.AnyAsync(i => i.OptionItemMaterialId == value.OptionItemMaterialId);
            if (exists)
            {
                Context.OptionItemMaterials.Update(value);
            }
            else
            {
                Context.OptionItemMaterials.Add(value);
            }
            await Context.SaveChangesAsync();
            return Ok(value);
        }
        catch (Exception ex)
        {
            return HandleError(ex, value);
        }
    }

    [HttpDelete("{id:guid}")]
    public async Task<IActionResult> Delete(Guid id)
    {
        try
        {
            await Context.OptionItemMaterials.Where(i => i.OptionItemMaterialId == id).BatchDeleteAsync();

            return Ok();
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }
}