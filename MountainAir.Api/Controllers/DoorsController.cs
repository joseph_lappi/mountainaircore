﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.Data.Models;
using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using NLog;

namespace MountainAir.Api.Controllers;

public class DoorsController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();
    public DoorsController(ApplicationDbContext context) : base(context)
    {

    }

    [HttpPut]
    [HttpPost]
    [ProducesResponseType(typeof(Door), StatusCodes.Status200OK)]
    public async Task<IActionResult> Put([FromBody]Door value)
    {
        try
        {
            var exists = Context.Doors.Any(d => d.DoorId == value.DoorId);
            if (exists)
            {
                Context.Doors.Update(value);
            }
            else
            {
                Context.Doors.Add(value);
            }

            await Context.SaveChangesAsync();

            return Ok(value);
        }
        catch (Exception ex)
        {
            return HandleError(ex, value);
        }
    }

    [HttpDelete("{id:guid}")]
    [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
    public async Task<IActionResult> Delete(Guid id)
    {
        try
        {
            var data = await Context.Doors.FirstOrDefaultAsync(d => d.DoorId == id);
            if (data != null)
            {
                Context.Doors.Remove(data);
                await Context.SaveChangesAsync();
            }
            return Ok();
        }
        catch (Exception ex)
        {
            return HandleError(ex, id);
        }
    }
}