﻿using EFCore.BulkExtensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using NLog;

namespace MountainAir.Api.Controllers;

public class WarrantyTypeController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();

    public WarrantyTypeController(
        ApplicationDbContext context)
        : base(context)
    {
    }
    
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<WarrantyType>), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get()
    {
        try
        {
            var types = await Context.WarrantyTypes.AsNoTracking().ToListAsync();

            return Ok(types);
        }
        catch (Exception e)
        {
            return HandleError(e);
        }
    }
    
    [HttpGet, Route("{id:guid}")]
    [ProducesResponseType(typeof(WarrantyType), StatusCodes.Status200OK)]
    public async Task<IActionResult> Get(Guid id)
    {
        try
        {
            var type = await Context.WarrantyTypes
                .AsNoTracking()
                .FirstOrDefaultAsync(i => i.Id == id);

            return Ok(type);
        }
        catch (Exception e)
        {
            return HandleError(e);
        }
    }
    
    [HttpPut, HttpPost]
    [ProducesResponseType(typeof(WarrantyType), StatusCodes.Status200OK)]
    public async Task<IActionResult> AddOrUpdate([FromBody] WarrantyType model)
    {
        try
        {
            var exists = Context.WarrantyTypes.Any(i => i.Id == model.Id);
            if (exists)
            {
                Context.WarrantyTypes.Update(model);
            }
            else
            {
                Context.WarrantyTypes.Add(model);
            }
            await Context.SaveChangesAsync();
            return Ok(model);
        }
        catch (Exception ex)
        {
            return HandleError(ex, model);
        }
    }

    [HttpDelete("{id:guid}")]
    [ProducesResponseType(typeof(void), StatusCodes.Status200OK)]
    public async Task<IActionResult> Delete(Guid id)
    {
        try
        {
            await Context.WarrantyTypes.Where(i => i.Id == id).BatchDeleteAsync();
            return Ok();
        }
        catch (Exception ex)
        {
            HandleError(ex);
        }

        return Ok();
    }
}