﻿using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.Data;
using MountainAir.Common.Models;
using MountainAir.Common.Services;
using MountainAir.Common.Utility;
using NLog;

namespace MountainAir.Api.Controllers;

public class EmailController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();
    private readonly IEmailSender _emailSender;

    public EmailController(
        IEmailSender emailSender, 
        ApplicationDbContext context)
        : base(context)
    {
        _emailSender = emailSender;
    }

    [HttpPost, Route("[Action]")]
    public async Task<IActionResult> SendEmail([FromBody] EmailMessage model)
    {
        try
        {
	        var user = CurrentUser; 

            var success = await _emailSender.SendEmailAsync(model, null, user.Email);

            if (success)
            {
                return Ok("Email sent successfully.");
            }
            
            return BadRequest("Message send failure.");
        }
        catch (Exception ex)
        {
            return HandleError(ex, model);
        }
    }
    
    [HttpPost, Route("[Action]")]
    public async Task<IActionResult> SendMultiPartEmail(
        [ModelBinder(BinderType = typeof(JsonModelBinder))] EmailMessage model,
        IList<IFormFile> files)
    {
        try
        {
	        var user = CurrentUser; 

            var success = await _emailSender.SendEmailAsync(model, files, user.Email);
            
            if (success)
            {
                return Ok("Email sent successfully.");
            }

            //string json = JsonConvert.SerializeObject(model);
            
            return BadRequest($"Message send failure.");
        }
        catch (Exception ex)
        {
            return HandleError(ex, model);
        }
    }
}