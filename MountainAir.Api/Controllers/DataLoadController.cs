﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Models;
using MountainAir.Common.Security;
using MountainAir.Common.Utility;
using NLog;

namespace MountainAir.Api.Controllers;

[Produces("text/plain")]
public class DataLoadController : BaseApiController
{
    protected override NLog.ILogger Logger => LogManager.GetCurrentClassLogger();
    private readonly UserManager<ApplicationUser> _userManager;

    public DataLoadController(UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        : base(context)
    {
        _userManager = userManager;
    }
    
    [HttpGet, Route("GetAll")]
    [ProducesResponseType(typeof(FullDownloadPackage), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetAll()
    {
        try
        {
            var model = await GetFullModelAsync();

            var result = ZipUtil.ZipObjectToBase64String(model);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    //[HttpGet, Route("GetAllData")]
    //[ProducesResponseType(typeof(FullDownloadPackage), StatusCodes.Status200OK)]
    //public async Task<IActionResult> GetAllData()
    //{
    //    try
    //    {
    //        var model = await GetFullModelAsync();

    //        return Ok(model);
    //    }
    //    catch (Exception ex)
    //    {
    //        return HandleError(ex);
    //    }
    //}
    
    //[HttpGet, Route("GetCatalogData")]
    //[ProducesResponseType(typeof(CatalogOnlyPackage), StatusCodes.Status200OK)]
    //public async Task<IActionResult> GetCatalogData()
    //{
    //    try
    //    {
    //        var model = await GetCatalogModelAsync();

    //        return Ok(model);
    //    }
    //    catch (Exception ex)
    //    {
    //        return HandleError(ex);
    //    }
    //}
    
    [HttpGet, Route("GetCatalog")]
    [ProducesResponseType(typeof(CatalogOnlyPackage), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetCatalog()
    {
        try
        {
            var model = await GetCatalogModelAsync();

            var result = ZipUtil.ZipObjectToBase64String(model);

            return Ok(result);
        }
        catch (Exception ex)
        {
            return HandleError(ex);
        }
    }

    private async Task<CatalogOnlyPackage> GetCatalogModelAsync()
    {
        var user = await GetCurrentUser();
        var roles = await _userManager.GetRolesAsync(user);

        var model = new CatalogOnlyPackage
        {
            WarrantyTypes = await Context.WarrantyTypes.AsNoTracking().ToListAsync(),
            CurrentUser = new UserModel(user, roles),
            CatalogItems = await Context.CatalogItems.AsNoTracking().Where(i => i.Active).ToListAsync(),
            Categories = await Context.Categories.AsNoTracking().ToListAsync(),
            Features = await Context.Features.AsNoTracking().ToListAsync(),
            Financings = await Context.Financings.AsNoTracking().ToListAsync(),
            Locations = await Context.Locations.AsNoTracking().ToListAsync(),
            Packages = await GetPackagesAsync(),
            PaymentMethods = await Context.PaymentMethods.AsNoTracking().ToListAsync()
        };
        return model;
    }

    private async Task<List<Package>> GetPackagesAsync()
    {
        var packages = await Context.Packages
            .Include(p => p.PackageItems.Where(pi => pi.CatalogItem.Active))
            .ThenInclude(pi => pi.PackageMaterials.Where(m => m.CatalogItem.Active))
            .Where(package => package.Active)
            .ToListAsync();

        return packages;
    }

    private async Task<FullDownloadPackage> GetFullModelAsync()
    {
        var currentUser = await GetCurrentUser();
        var roles = await _userManager.GetRolesAsync(currentUser);

        var model = new FullDownloadPackage
        {
            WarrantyTypes = await Context.WarrantyTypes.AsNoTracking().ToListAsync(),
            CurrentUser = new UserModel(currentUser, roles),
            CatalogItems = await Context.CatalogItems.AsNoTracking().Where(i => i.Active).ToListAsync(),
            Categories = await Context.Categories.AsNoTracking().ToListAsync(),
            Features = await Context.Features.AsNoTracking().ToListAsync(),
            Financings = await Context.Financings.AsNoTracking().ToListAsync(),
            Locations = await Context.Locations.AsNoTracking().ToListAsync(),
            Packages = await GetPackagesAsync(),
            PaymentMethods = await Context.PaymentMethods.AsNoTracking().ToListAsync(),
            Users = (await Context.Users.AsNoTracking().Where(i => i.Active == true).ToListAsync()).Select(i => new UserModel(i)).ToList(),
            Customers = await GetCustomers(),
            Estimates = await Context.Estimates.AsNoTracking().Where(i => i.AssignedTo == currentUser.Id && i.Active == true).ToListAsync()
        };
        return model;
    }

    private async Task<List<Customer>> GetCustomers()
    {
        var currentUser = await GetCurrentUser();

        var isTestUser = await _userManager.IsInRoleAsync(currentUser, "TestUser");

        if (isTestUser)
        {
            return await Context.Customers
                .AsNoTracking()
                .Where(c => c.TestData == true)
                .Include(c => c.Addresses)
                .ToListAsync();
        }

        var data = await Context.Customers
            .Include(c => c.Addresses)
            .AsNoTracking()
            .ToListAsync();
        return data;

    }
}