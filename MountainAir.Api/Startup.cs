﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using MountainAir.Api.Security;
using MountainAir.Common.AzureStorage;
using MountainAir.Common.Configuration;
using MountainAir.Common.Data;
using MountainAir.Common.JsonConverters;
using MountainAir.Common.Security;
using MountainAir.Common.Services;
using Newtonsoft.Json;
using NLog;
using NLog.Layouts;
using NLog.Targets;

namespace MountainAir.Api;

public class Startup
{
    private readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        AppSettings.Shared = new AppSettings(Configuration);

        var connString = AppSettings.Shared.DefaultConnectionString;

        services.AddDbContext<ApplicationDbContext>(options =>
            options.UseSqlServer(connString));

        services.AddControllers().AddNewtonsoftJson(options =>
        {
            options.SerializerSettings.DefaultValueHandling = DefaultValueHandling.Include;
            options.SerializerSettings.ContractResolver = new LowercaseContractResolver();

            options.SerializerSettings.Error = (sender, args) =>
            {
                _logger.Error(args.ErrorContext.Error.GetBaseException());
            };
        });

        CreateNLog(connString);

        services.AddIdentity<ApplicationUser, ApplicationRole>()
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();
        
        services.AddSingleton(sp =>
        {
            var cfg = sp.GetRequiredService<TokenSettings>();
            return cfg;
        });

        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    IssuerSigningKey = TokenSettings.Key,
                    ValidAudience = TokenSettings.Audience,
                    ValidIssuer = TokenSettings.Issuer,
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.Zero
                };
                options.RequireHttpsMetadata = false;
                options.SaveToken = true;
                options.Events = new JwtBearerEvents
                {
                    OnAuthenticationFailed = context =>
                    {
                        if (context.Exception.GetType() == typeof(SecurityTokenExpiredException))
                        {
                            context.Response.Headers.Add("Token-Expired", "true");
                        }
                        return Task.CompletedTask;
                    }
                };
            });

        services.AddMvc();

        // Add application services.
        services
            .AddSingleton<IHttpContextAccessor, HttpContextAccessor>()
            .AddTransient<IWarrantyService, WarrantyService>()
            .AddTransient<IEmailSender, SmtpEmailSender>()
            .AddTransient<IAzureBlobManager, AzureStorageManager>()
            .AddTransient<ISignatureManager, SignatureManager>();
            
        // Register the Swagger generator, defining one or more Swagger documents
        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen();

        //services.AddMemoryCache();

        //services.AddSession();
    }
    
    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
            //app.UseDatabaseErrorPage();
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Mountain Air API V1");
                c.RoutePrefix = string.Empty;
            });
        }
        else
        {
            //The default HSTS value is 30 days.You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }
        
        app.UseHttpsRedirection();

        app.UseAuthentication();
        
        //app.UseSession();
        
        app.UseRouting();
        
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllerRoute(name: "default", pattern: "api/{Controller=Home}/{action=Index}/{id?}");
        });
    }

    private static void CreateNLog(string connStr)
    {
        var sqlLog = new DatabaseTarget
        {
            ConnectionString = connStr,
            Name = "logSql",
            CommandText = "spLogEntryApi",
            CommandType = System.Data.CommandType.StoredProcedure
        };

        sqlLog.Parameters.Add(new DatabaseParameterInfo("@logged", Layout.FromString("${date}")));
        sqlLog.Parameters.Add(new DatabaseParameterInfo("@level", Layout.FromString("${level}")));
        sqlLog.Parameters.Add(new DatabaseParameterInfo("@message", Layout.FromString("${message}")));
        sqlLog.Parameters.Add(new DatabaseParameterInfo("@callSite", Layout.FromString("${callsite}")));
        sqlLog.Parameters.Add(new DatabaseParameterInfo("@exception", Layout.FromString("${exception:tostring}")));

        var logConsole = new NLog.Targets.ConsoleTarget
        {
            Name = "logconsole"
        };

        var config = new NLog.Config.LoggingConfiguration();

        config.LoggingRules.Add(new NLog.Config.LoggingRule("*", NLog.LogLevel.Info, logConsole));
        config.LoggingRules.Add(new NLog.Config.LoggingRule("*", NLog.LogLevel.Info, sqlLog));

        LogManager.Configuration = config;
        LogManager.ThrowExceptions = true;
        LogManager.ThrowConfigExceptions = true;
    }
}