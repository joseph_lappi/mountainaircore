﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Caching.Memory;
using MountainAir.Common.Security;

namespace MountainAir.Api.Security;

public class PermissionHandler : IAuthorizationHandler
{
    readonly UserManager<ApplicationUser> _userManager;
    readonly IMemoryCache _memoryCache;

    public PermissionHandler(UserManager<ApplicationUser> userManager, IMemoryCache memoryCache)
    {
        _userManager = userManager;
        _memoryCache = memoryCache;
    }

    public async Task HandleAsync(AuthorizationHandlerContext context)
    {
        var name = context.User.Identity.Name;

        ApplicationUser user = await _memoryCache.GetOrCreate($"AppUser_{name}", async entry => 
        {
            entry.AbsoluteExpiration = DateTime.Now.AddMinutes(5);

            if (Guid.TryParse(name, out Guid id))
            {
                return await _userManager.FindByIdAsync(id.ToString());
            }
            else
            {
                return await _userManager.FindByNameAsync(name);
            }
        });

        var pendingRequirements = context.PendingRequirements.ToList();

        foreach (var requirement in pendingRequirements)
        {
            if (requirement is ActiveUserPolicy)
            {
                if (user != null && user.Active)
                {
                    context.Succeed(requirement);
                }
            }
        }
    }
}