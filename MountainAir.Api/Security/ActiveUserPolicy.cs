﻿using System;
using Microsoft.AspNetCore.Authorization;

namespace MountainAir.Api.Security;

public class ActiveUserPolicy : IAuthorizationRequirement
{
    public const string PolicyName = "RequireActiveUser";
}