﻿using MountainAir.Common.Security;
using System;

namespace MountainAir.Api.Security;

public class AuthenticateResponse
{
    public AuthenticateResponse()
    {

    }

    public AuthenticateResponse(ApplicationUser user, string jwtToken, long tokenExpiration, string refreshToken, string[] roles)
    {
        if (user == null)
            return;

        UserId = user.Id;
        FirstName = user.FirstName;
        LastName = user.LastName;
        UserName = user.UserName;
        PhoneNumber = user.PhoneNumber;
        Email = user.Email;
        DefaultLaborRate = user.DefaultLaborRate;
        DefaultProfitMargin = user.DefaultProfitMargin;
        DefaultSalesTax = user.DefaultSalesTax;
        Roles = roles;
        Token = jwtToken;
        RefreshToken = refreshToken;
        TokenExpiration = tokenExpiration;
    }

    public long TokenExpiration { get; set; }

    public Guid UserId { get; set; }

    public string Email { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string PhoneNumber { get; set; }

    public string UserName { get; set; }

    public decimal? DefaultLaborRate { get; set; }

    public decimal? DefaultProfitMargin { get; set; }
        
    public decimal? DefaultSalesTax { get; set; }

    public string[] Roles { get; set; }

    public DateTime RequestAt => DateTime.Now;

    public string Token { get; set; }

    public string AuthToken => Token;
        
    public string RefreshToken { get; set; }
}