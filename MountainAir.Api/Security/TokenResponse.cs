﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MountainAir.Api.Security;

public class TokenResponse
{
    public string Token { get; set; }

    public long TokenExpiration { get; set; }

    public string RefreshToken { get; set; }
}