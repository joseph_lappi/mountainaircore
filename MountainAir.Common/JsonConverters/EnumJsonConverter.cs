﻿using Newtonsoft.Json;
using System;
using System.Globalization;
using System.Text;

namespace MountainAir.Common.JsonConverters;

/// <summary>
/// Formats the Enum values to the format that we follow across commerce.
/// </summary>
public class EnumJsonConverter : JsonConverter
{
    /// <summary>
    /// Boolean value to indicate whether PascalToJscriptCase() conversion should be disabled or not.
    /// </summary>
    private readonly bool isPascalToJscriptCaseConversionDisabled;

    public EnumJsonConverter()
    {
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="T:LS.MSFT.CSP.Models.JsonConverters.EnumJsonConverter" /> class.
    /// </summary>
    /// <param name="isPascalToJscriptCaseConversionDisabled">Boolean value to indicate if PascalToJscriptCase() conversion is disabled or not.</param>
    public EnumJsonConverter(bool isPascalToJscriptCaseConversionDisabled)
    {
        this.isPascalToJscriptCaseConversionDisabled = isPascalToJscriptCaseConversionDisabled;
    }

    /// <summary>Converts from JavaScript to Pascal case.</summary>
    /// <param name="jsonValue">The JSON value.</param>
    /// <returns>Pascal cased value</returns>
    public static string JScriptToPascalCase(string jsonValue)
    {
        if (string.IsNullOrEmpty(jsonValue))
            throw new ArgumentNullException(nameof(jsonValue));
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append(char.ToUpper(jsonValue[0], CultureInfo.InvariantCulture));
        for (int index = 1; index < jsonValue.Length; ++index)
            stringBuilder.Append(jsonValue[index] == '_' ? char.ToUpper(jsonValue[++index], CultureInfo.InvariantCulture) : jsonValue[index]);
        return stringBuilder.ToString();
    }

    /// <summary>
    /// Determines whether this instance can convert the specified object type.
    /// </summary>
    /// <param name="objectType">Type of the object.</param>
    /// <returns>
    /// <c>true</c> if this instance can convert the specified object type; otherwise, <c>false</c>.
    /// </returns>
    public override bool CanConvert(Type objectType)
    {
        if (objectType != (Type)null)
            return objectType.IsEnum;
        return false;
    }

    /// <summary>Reads the JSON representation of the object.</summary>
    /// <param name="reader">The <see cref="T:Newtonsoft.Json.JsonReader" /> to read from.</param>
    /// <param name="objectType">Type of the object.</param>
    /// <param name="existingValue">The existing value of object being read.</param>
    /// <param name="serializer">The calling serializer.</param>
    /// <returns>The object value.</returns>
    /// <exception cref="T:System.ArgumentNullException">
    /// reader
    /// or
    /// objectType
    /// </exception>
    /// <exception cref="T:Newtonsoft.Json.JsonSerializationException">
    /// </exception>
    public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
    {
        if (reader == null)
            throw new ArgumentNullException(nameof(reader));

        if (objectType == null)
            throw new ArgumentNullException(nameof(objectType));

        if (!objectType.IsEnum)
            throw new JsonSerializationException($"EnumJsonConverter cannot deserialize '{objectType.Name}'");

        if (reader.TokenType == JsonToken.String)
            return Enum.Parse(objectType, this.isPascalToJscriptCaseConversionDisabled ? reader.Value.ToString() : JScriptToPascalCase(reader.Value.ToString()));

        if (reader.TokenType == JsonToken.Integer)
            return Enum.ToObject(objectType, reader.Value);

        throw new JsonSerializationException($"EnumJsonConverter cannot deserialize '{reader.TokenType}' values");
    }

    /// <summary>Writes the JSON representation of the object.</summary>
    /// <param name="writer">The <see cref="T:Newtonsoft.Json.JsonWriter" /> to write to.</param>
    /// <param name="value">The value.</param>
    /// <param name="serializer">The calling serializer.</param>
    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        if (writer == null)
            throw new ArgumentNullException(nameof(writer));
        if (value == null)
            return;
        writer.WriteValue(this.isPascalToJscriptCaseConversionDisabled ? value.ToString() : PascalToJscriptCase(value.ToString()));
    }

    /// <summary>Converts from Pascal to JavaScript case.</summary>
    /// <param name="enumValue">The enum value.</param>
    /// <returns>string enum value</returns>
    private static string PascalToJscriptCase(string enumValue)
    {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.Append(char.ToLower(enumValue[0], CultureInfo.InvariantCulture));
        for (int index = 1; index < enumValue.Length; ++index)
        {
            if (char.IsUpper(enumValue[index]))
            {
                stringBuilder.Append("_");
                stringBuilder.Append(char.ToLower(enumValue[index], CultureInfo.InvariantCulture));
            }
            else
                stringBuilder.Append(enumValue[index]);
        }
        return stringBuilder.ToString();
    }
}