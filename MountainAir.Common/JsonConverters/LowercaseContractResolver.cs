﻿using Newtonsoft.Json.Serialization;

namespace MountainAir.Common.JsonConverters;

public class LowercaseContractResolver : DefaultContractResolver
{
    protected override string ResolvePropertyName(string propertyName)
    {
        return propertyName.ToLower();
    }
}