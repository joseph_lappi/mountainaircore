﻿//using FirebaseAdmin.Auth;
//using Microsoft.AspNetCore.Identity;
//using MountainAir.Common.Security;
//using NLog;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace MountainAir.Common.Firebase
//{
//    public class FirebaseUtil
//    {
//        private readonly UserManager<ApplicationUser> _userManager;
//        public ILogger Logger { get; set; } = LogManager.GetCurrentClassLogger();

//        public FirebaseUtil(UserManager<ApplicationUser> userManager)
//        {
//            _userManager = userManager;
//        }

//        /// <summary>
//        /// Formatting phone number to e.164 specification.
//        /// </summary>
//        /// <param name="phoneNumber"></param>
//        /// <returns></returns>
//        public string FormatPhoneNumber(string phoneNumber)
//        {
//            if (string.IsNullOrEmpty(phoneNumber))
//                return null;

//            string value = new string(phoneNumber.Where(c => char.IsDigit(c)).ToArray());

//            if (!value.StartsWith("1"))
//                value = "1" + value;

//            return "+" + value;
//        }

//        public async Task<UserRecord> GetFirebaseUser(ApplicationUser user)
//        {
//            var instance = FirebaseAuth.DefaultInstance;

//            return await instance.GetUserByEmailAsync(user.Email);
//        }

//        public async Task<UserRecord> CreateFirebaseUser(ApplicationUser user, string password)
//        {
//            var instance = FirebaseAuth.DefaultInstance;

//            var firebaseUser = await instance.CreateUserAsync(new UserRecordArgs
//            {
//                DisplayName = $"{user.FirstName} {user.LastName}",
//                Email = user.Email,
//                EmailVerified = true,
//                Password = password,
//                PhoneNumber = FormatPhoneNumber(user.PhoneNumber),
//                Disabled = !user.Active
//            });

//            return firebaseUser;
//        }

//        public async Task<UserRecord> GetFirebaseUserByEmailAsync(string email)
//        {
//            try
//            {
//                var instance = FirebaseAuth.DefaultInstance;

//                var firebaseUser = await instance.GetUserByEmailAsync(email);

//                return firebaseUser;
//            }
//            catch (Exception ex)
//            {
//                this.Logger.Error(ex.GetBaseException(), $"Error retreiving firebase user. {ex.Message}");
//            }
//            return null;
//        }

//        public async Task<UserRecord> UpdateFirebaseUser(string username, string password = null)
//        {
//            ApplicationUser user = await _userManager.FindByNameAsync(username);

//            if (user == null)
//                throw new ArgumentNullException("User");

//            return await UpdateFirebaseUser(user, password);
//        }

//        public async Task ChangeLocalPassword(ApplicationUser user, string password = null)
//        {
//            if (!string.IsNullOrEmpty(password))
//            {
//                if (await _userManager.HasPasswordAsync(user))
//                {
//                    await _userManager.RemovePasswordAsync(user);
//                }

//                await _userManager.AddPasswordAsync(user, password);
//            }
//        }

//        public async Task<UserRecord> UpdateFirebaseUser(ApplicationUser user, string password = null)
//        {
//            try
//            {
//                if (user == null)
//                    throw new ArgumentNullException("User");

//                var instance = FirebaseAuth.DefaultInstance;

//                var firebaseUser = await GetFirebaseUserByEmailAsync(user.Email);

//                if (string.IsNullOrEmpty(user.UID))
//                {
//                    user.UID = firebaseUser.Uid;

//                    await _userManager.UpdateAsync(user);
//                }

//                var args = new UserRecordArgs
//                {
//                    DisplayName = $"{user.FirstName} {user.LastName}",
//                    Email = user.Email,
//                    EmailVerified = true,
//                    Uid = user.UID,
//                    PhoneNumber = FormatPhoneNumber(user.PhoneNumber),
//                    Disabled = !user.Active
//                };

//                if (!string.IsNullOrEmpty(password))
//                    args.Password = password;

//                await instance.UpdateUserAsync(args);

//                return firebaseUser;
//            }
//            catch (Exception ex)
//            {
//                Logger.Error(ex, $"Error updating firebase user: {ex.Message}");
//            }

//            return null;
//        }
//    }
//}
