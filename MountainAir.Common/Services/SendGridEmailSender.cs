﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MountainAir.Common.Models;
using NLog;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace MountainAir.Common.Services;

public class SendGridEmailSender: IEmailSender
{
    private readonly IConfiguration _configuration;
    private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

    public SendGridEmailSender(IConfiguration configuration)
    {
        _configuration = configuration;
    }
    
    public async Task SendPasswordResetEmailAsync(string email, string callbackUrl)
    {
        var message = new EmailMessage
        {
            Email = email,
            Message = $"Please reset your password by clicking here: <a href='{callbackUrl}'>{callbackUrl}</a>",
            Subject = "Reset Password"
        };

        await SendEmailAsync(message);
    }

    public async Task SendEmailConfirmationAsync(string email, string link)
    {
        var message = new EmailMessage
        {
            Email = email,
            Message = $"Please confirm your account by clicking this link: <a href='{HtmlEncoder.Default.Encode(link)}'>link</a>",
            Subject = "Confirm your email"
        };

        await SendEmailAsync(message);
    }

    public async Task<bool> SendEmailAsync(EmailMessage message, IEnumerable<IFormFile> files = null, string sender = null)
    {
        try
        {
            var apiKey = _configuration.GetValue<string>("SendGridApiKey");
            var replyToAddress = _configuration.GetValue<string>("ReplyToAddress");
            var replyToName = sender ?? _configuration.GetValue<string>("ReplyToName");

            var client = new SendGridClient(apiKey);

            var msg = new SendGridMessage();

            msg.SetFrom(new EmailAddress(replyToAddress, replyToName));
            
            msg.AddTo(message.Email);

            if(!string.IsNullOrEmpty(message.Cc)) 
                msg.AddCc(message.Cc);
            
            if(!string.IsNullOrEmpty(message.Bcc)) 
                msg.AddBcc(message.Bcc);

            msg.SetSubject(message.Subject);

            msg.AddContent(MimeType.Html, message.Message);
            
            if (message.Attachments?.Any() ?? false)
            {
                foreach (var attachment in message.Attachments)
                {
                    msg.AddAttachment(attachment.FileName, attachment.Content, attachment.ContentType);
                }
            }

            if (files?.Any() ?? false)
            {
                foreach (var file in files)
                {
                    var stream = file.OpenReadStream();
                    await msg.AddAttachmentAsync(file.FileName, stream, file.ContentType);
                }
            }

            var response = await client.SendEmailAsync(msg);

            if (!response.IsSuccessStatusCode)
            {
                _logger.Warn($"Error sending message: {await response.Body.ReadAsStringAsync()}");
            }

            return response.IsSuccessStatusCode;
        }
        catch (Exception ex)
        {
            _logger.Error(ex, $"Error sending email: {ex.GetBaseException().Message}");
            throw;
        }
    }
}