﻿using System.Collections.Generic;
using Microsoft.Azure.NotificationHubs;

namespace MountainAir.Common.Services.Notifications;

public class NotificationSender : INotificationSender
{
    private List<Notification> notifications = new List<Notification>();

    public NotificationHubClient Hub { get; set; }

    private NotificationSender(NotificationSenderOptions options)
    {
        // Placeholders: replace with the connection string (with full access) for your notification hub and the hub name from the Azure Classics Portal
        Hub = NotificationHubClient.CreateClientFromConnectionString(options.ConnectionString, options.HubName);
    }

    public Notification CreateNotification(string message, string richType, string payload)
    {
        var notification = new Notification()
        {
            Id = notifications.Count,
            Message = message,
            RichType = richType,
            Payload = payload,
            Read = false
        };

        notifications.Add(notification);

        return notification;
    }

    //public Stream ReadImage(int id)
    //{
    //    var assembly = Assembly.GetExecutingAssembly();
    //    // Placeholder: image file name (for example, logo.png).
    //    return assembly.GetManifestResourceStream("AppBackend.img.{logo.png}");
    //}
}