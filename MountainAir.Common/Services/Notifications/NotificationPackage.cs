﻿using Newtonsoft.Json;

namespace MountainAir.Common.Services.Notifications;

public class NotificationPackage
{
    [JsonProperty("aps")]
    public Aps Aps { get; set; }

    [JsonProperty("richId")]
    public string RichId { get; set; }

    [JsonProperty("richMessage")]
    public string RichMessage { get; set; }

    [JsonProperty("richType")]
    public string RichType { get; set; }
}

public class Aps
{
    [JsonProperty("content-available")]
    public long ContentAvailable { get; set; }

    [JsonProperty("sound")]
    public string Sound { get; set; }
}