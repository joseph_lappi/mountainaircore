﻿namespace MountainAir.Common.Services.Notifications;

public class Notification
{
    public int Id { get; set; }

    // Initial notification message to display to users
    public string Message { get; set; }

    // Type of rich payload (developer-defined)
    public string RichType { get; set; }

    public string Payload { get; set; }

    public bool Read { get; set; }
}