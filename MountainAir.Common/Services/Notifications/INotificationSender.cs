﻿namespace MountainAir.Common.Services.Notifications;

public interface INotificationSender
{
    Notification CreateNotification(string message, string richType, string payload);
}