﻿namespace MountainAir.Common.Services.Notifications;

public class NotificationSenderOptions
{
    public string HubName { get; set; }

    public string ConnectionString { get; set; }
}