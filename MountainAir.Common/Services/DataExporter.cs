﻿using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MountainAir.Common.Data.Models;

namespace MountainAir.Common.Services;

public class DataExporter
{
    private readonly ApplicationDbContext _ctx;

    public DataExporter(ApplicationDbContext ctx)
    {
        this._ctx = ctx;
    }

    public async Task<Dictionary<Guid, CompanyRecord>> ExportData()
    {
        //var customers = await _ctx.Customers
        //    .Include(p => p.Addresses)
        //    .Include(p => p.Estimates)
        //    .ToListAsync();

        var estimates = await _ctx.Estimates
            .Where(e => e.Active && e.AddressId != null)
            .Include(e => e.EstimateOptions)
            .ThenInclude(o => o.OptionItems.Where(t => t.CatalogItemId != null))
            .ThenInclude(oi => oi.OptionItemMaterials)
            .Include(e => e.Created)
            .Include(e => e.Assigned)
            .Include(e => e.Address)
            .Include(e => e.BillingAddress)
            .Include(e => e.Customer)
            .Include(e => e.Rooms)
            .ThenInclude(r => r.Doors)
            .Include(e => e.Rooms)
            .ThenInclude(r => r.Windows)
            .Take(10)
            .ToListAsync();

        //var catalog = await _ctx.Categories
        //    .Include(c => c.CatalogItems)
        //        .ThenInclude(c => c.Category)
        //    .ToListAsync();

        //var employees = await _ctx.Users.ToListAsync();

        var companies = new List<CompanyRecord>();

        var company = new CompanyRecord(Guid.NewGuid(), "Mountain Air Conditioning and Heating", true)
        {
            Estimates = estimates.ToDictionary(e => e.EstimateId, e => new EstimateRecord(e)),
            //Customers = customers.ToDictionary(c => c.CustomerId, c => new CustomerRecord(c)),
            //Employees = employees.ToDictionary(p => p.Id, p => new EmployeeRecord(p)),
            //Catalog = catalog.ToDictionary(p => p.CategoryId, p => new CategoryRecord(p))
        };

        companies.Add(company);

        return companies.ToDictionary(p => p.Properties.Id, p => p);
    }

    public async Task<DownloadDictionary> GetData()
    {
        var categories = await _ctx.Categories.Where(p => !p.MaterialCategory).Include(p => p.CatalogItems).Take(10).ToListAsync();
        var catalogItems = categories.SelectMany(m => m.CatalogItems).ToList();

        var model = new DownloadDictionary
        {
            CatalogItems = catalogItems.ToDictionary(p => p.CatalogItemId, p => new CatalogItemRecord(p)),
            Categories = categories.ToDictionary(p => p.CategoryId, p => new CategoryRecord(p)),
            Features = await _ctx.Features.ToDictionaryAsync(p => p.FeatureId, p => p),
            Financings = await _ctx.Financings.ToDictionaryAsync(p => p.FinancingId, p => p),
            Packages = await _ctx.Packages.Where(p => p.Active).Include(p => p.PackageItems).ToDictionaryAsync(p => p.PackageId, p => new PackageRecord(p)),
            PackageItems = await _ctx.PackageItems.ToDictionaryAsync(p => p.PackageItemId, p => new PackageItemRecord(p)),
            PackageMaterial = await _ctx.PackageMaterials.ToDictionaryAsync(p => p.PackageMaterialId, p => new PackageItemMaterialRecord(p)),
            PaymentMethods = await _ctx.PaymentMethods.ToDictionaryAsync(p => p.PaymentId, p => p),
            Users = await _ctx.Users.Where(i => i.Active == true).ToDictionaryAsync(p => p.Id, p => new UserModel(p)),
            Customers = await _ctx.Customers.Where(c => c.TestData != true).Include(p => p.Addresses).ToDictionaryAsync(p => p.CustomerId, p => new CustomerRecord(p)),
            Estimates = await _ctx.Estimates.Where(i => i.Active == true).Include(p => p.EstimateOptions).ToDictionaryAsync(p => p.EstimateId, p => new EstimateRecord(p)),
            Addresses = await _ctx.Addresses.ToDictionaryAsync(p => p.AddressId, p => new AddressRecord(p)),
            Rooms = await _ctx.Rooms.Include(p => p.Doors).Include(p => p.Windows).ToDictionaryAsync(p => p.RoomId, p => new RoomRecord(p)),
            Doors = await _ctx.Doors.ToDictionaryAsync(p => p.DoorId, p => new DoorRecord(p)),
            Windows = await _ctx.Windows.ToDictionaryAsync(p => p.WindowId, p => new WindowRecord(p)),
            EstimateOptions = await _ctx.EstimateOptions.Include(p => p.OptionItems).ToDictionaryAsync(p => p.EstimateOptionId, p => new EstimateOptionRecord(p)),
            OptionItems = await _ctx.OptionItems.Include(p => p.Category).Include(p => p.OptionItemMaterials).ToDictionaryAsync(p => p.OptionItemId, p => new OptionItemRecord(p)),
            OptionItemMaterials = await _ctx.OptionItemMaterials.ToDictionaryAsync(p => p.OptionItemMaterialId, p => new OptionMaterialRecord(p))
        };

        return model;
    }

    public class DownloadDictionary
    {
        public Dictionary<int, CatalogItemRecord> CatalogItems { get; set; }
        public Dictionary<int, CategoryRecord> Categories { get; set; }
        public Dictionary<Guid, UserModel> Users { get; set; }
        public Dictionary<Guid, EstimateRecord> Estimates { get; set; }
        public Dictionary<int, PackageRecord> Packages { get; set; }
        public Dictionary<int, PackageItemRecord> PackageItems { get; set; }
        public Dictionary<int, PackageItemMaterialRecord> PackageMaterial { get; set; }
        public Dictionary<Guid, CustomerRecord> Customers { get; set; }
        public Dictionary<Guid, AddressRecord> Addresses { get; set; }
        public Dictionary<int, Feature> Features { get; set; }
        public Dictionary<int, Financing> Financings { get; set; }
        public Dictionary<int, PaymentMethod> PaymentMethods { get; set; }
        public Dictionary<Guid, RoomRecord> Rooms { get; set; }
        public Dictionary<Guid, DoorRecord> Doors { get; set; }
        public Dictionary<Guid, WindowRecord> Windows { get; set; }
        public Dictionary<Guid, EstimateOptionRecord> EstimateOptions { get; set; }
        public Dictionary<Guid, OptionItemRecord> OptionItems { get; set; }
        public Dictionary<Guid, OptionMaterialRecord> OptionItemMaterials { get; set; }
    }
        
    private async Task<Dictionary<Guid, CustomerRecord>> GetCustomers()
    {
        return await _ctx.Customers
            .Where(c => c.TestData != true)
            .Include(c => c.Addresses)
            .ToDictionaryAsync(p => p.CustomerId, p => new CustomerRecord(p));
    }
}