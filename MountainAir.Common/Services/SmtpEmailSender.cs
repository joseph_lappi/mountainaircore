﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MountainAir.Common.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.AzureStorage;

namespace MountainAir.Common.Services;

public class SmtpEmailSender : IEmailSender
{
    public static SmtpConfig LocalPapercutConfig = new() { EnableSsl = false, Host = "localhost", Port = 25 };
    private readonly SmtpConfig _configuration;
    private readonly ILogger _logger = LogManager.GetCurrentClassLogger();
    private readonly IAzureBlobManager _storageManager;

    public SmtpEmailSender(IConfiguration configuration, IAzureBlobManager storageManager)
    {
        _configuration = new SmtpConfig(configuration);
        _storageManager = storageManager;
    }
    
    public SmtpEmailSender(SmtpConfig configuration, IAzureBlobManager storageManager)
    {
        _configuration = configuration;
        _storageManager = storageManager;
    }

    public SmtpEmailSender()
    {
        _configuration = LocalPapercutConfig;
        //_storageManager = new AzureStorageManager("", "", AzureEnvironments.Dev);
    }

    public async Task<bool> SendEmailAsync(EmailMessage message, IEnumerable<IFormFile> files = null, string sender = null)
    {
        try
        {
	        _logger.Info($"Sending email for message Sender [{sender}]: {message.Subject}");

	        var attachmentCount = message.Attachments.Count;

            var fromAddr = new MailAddress(_configuration.ReplyToEmail, sender ?? _configuration.ReplyToName);

            var newMail = new MailMessage
            {
                From = fromAddr,
                Subject = message.Subject,
                SubjectEncoding = Encoding.UTF8,
                Body = message.Message,
                BodyEncoding = Encoding.UTF8,
                IsBodyHtml = true,
                Priority = MailPriority.Normal
            };

            var logId = await CreateEmailLog(message, sender, attachmentCount);

            newMail.To.Add(message.Email);

            //newMail.ReplyToList.Add(_configuration.ReplyToEmail);

            if(!string.IsNullOrEmpty(message.Cc))
                newMail.CC.Add(message.Cc);

            if(!string.IsNullOrEmpty(message.Bcc))
                newMail.Bcc.Add(message.Bcc);

            if (files?.Any() ?? false)
            {
                foreach (var file in files)
                {
	                await SaveAttachment(logId, file);
                    newMail.Attachments.Add(CreateAttachment(file));
                }
            }

            if (message.Attachments.Any())
            {
                foreach (var file in message.Attachments)
                {
	                await SaveAttachment(logId, file);
                    newMail.Attachments.Add(CreateAttachment(file));
                }
            }

            using var smtp = GetDisposableSmtpClient();

            await smtp.SendMailAsync(newMail);

            return true;
        }
        catch (Exception ex)
        {
            _logger.Error(ex, $"Error sending email: {ex.GetBaseException().Message}");
            throw;
        }
    }
    
    private async Task<Guid?> CreateEmailLog(EmailMessage message, string sender = null, int? attachmentCount = 0)
    {
	    try
	    {
		    await using var ctx = new ApplicationDbContext();

		    var log = new EmailLog()
		    {
                Id = Guid.NewGuid(),
                Message = message.Message,
                SendDate = DateTime.UtcNow,
                SendBcc = message.Bcc,
                SendCc = message.Cc,
                SendTo = message.Email,
                Subject = message.Subject,
                Sender = sender,
                AttachmentCount = attachmentCount
		    };

		    ctx.EmailLogs.Add(log);
            await ctx.SaveChangesAsync();
            return log.Id;
	    }
	    catch (Exception ex)
	    {
		    _logger.Error(ex, $"Error creating email log: {ex.GetBaseException().Message}");
	    }

	    return null;
    }
    
    private async Task SaveAttachment(Guid? messageId, EmailAttachment file)
    {
	    if (messageId == null || _storageManager == null)
		    return;

	    await _storageManager.AddAttachment(messageId.Value, file);
    }

    private async Task SaveAttachment(Guid? messageId, IFormFile file)
    {
	    if (messageId == null || _storageManager == null)
		    return;

	    await _storageManager.AddAttachment(messageId.Value, file);
    }

    private SmtpClient GetDisposableSmtpClient()
    {
        string uname = _configuration.Username;
        string pw = _configuration.Password;

        var smtpClient = new SmtpClient(_configuration.Host)
        {
            UseDefaultCredentials = false, 
            Port = _configuration.Port,
            EnableSsl = _configuration.EnableSsl,
            Credentials = new NetworkCredential(uname, pw)
        };

        //if (!smtpClient.UseDefaultCredentials)
        //{
        //    smtpClient.Credentials = new NetworkCredential(uname, pw);
        //}

        return smtpClient;
    }

    private Attachment CreateAttachment(EmailAttachment attachment)
    {
        var bytes = Convert.FromBase64String(attachment.Content);
        var ms = new MemoryStream(bytes);
        var ct = new ContentType(attachment.ContentType);
        var attach = new Attachment(ms, ct);
        attach.ContentDisposition.FileName = attachment.FileName;
        return attach;
    }

    private Attachment CreateAttachment(IFormFile attachment)
    {
        var ms = attachment.OpenReadStream();

        var ct = new ContentType(attachment.ContentType);
        var attach = new Attachment(ms, ct);
        attach.ContentDisposition.FileName = attachment.FileName;
        return attach;
    }

    public async Task SendPasswordResetEmailAsync(string email, string callbackUrl)
    {
        var message = new EmailMessage
        {
            Email = email,
            Message = $"Please reset your password by clicking here: <a href='{callbackUrl}'>{callbackUrl}</a>",
            Subject = "Reset Password"
        };

        await SendEmailAsync(message);
    }

    public async Task SendEmailConfirmationAsync(string email, string link)
    {
        var message = new EmailMessage
        {
            Email = email,
            Message = $"Please confirm your account by clicking this link: <a href='{HtmlEncoder.Default.Encode(link)}'>link</a>",
            Subject = "Confirm your email"
        };

        await SendEmailAsync(message);
    }
}

public class SmtpConfig
{
    public string ReplyToEmail { get; set; }
    public string Host { get; set; }
    public int Port { get; set; }
    public string Username { get; set; }
    public string Password { get; set; }
    public string ReplyToName { get; set; }
    public bool EnableSsl { get; set; } = true;

    public SmtpConfig()
    {

    }

    public SmtpConfig(IConfiguration config)
    {
        Host = config.GetValue<string>("SmtpHost");
        Port = config.GetValue<int>("SmtpPort");
        Username = config.GetValue<string>("SmtpUserName");
        Password = config.GetValue<string>("SmtpPassword");
        ReplyToEmail = config.GetValue<string>("SmtpReplyToEmail");
        ReplyToName = config.GetValue<string>("SmtpReplyToName");
    }
}