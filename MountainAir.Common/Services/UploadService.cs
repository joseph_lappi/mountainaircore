﻿using System;
using System.Collections.Generic;
using MountainAir.Common.DataAccess;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;
using MountainAir.Common.Data.Models;
using System.Reflection;

namespace MountainAir.Common.Services;

public class UploadService
{
    public List<CatalogItem> ConvertFileData(int categoryId, string fileData)
    {
        var retval = new List<CatalogItem>();
        var fixedCsvFileContent = Regex.Replace(fileData, @"(?!(([^""]*""){2})*[^""]*$)\n+", "<br>");
        var dt = DataTable.New.ReadFromString(fixedCsvFileContent);
        string[] columns = dt.ColumnNames.ToArray();

        foreach (Row row in dt.Rows)
        {
            var item = new CatalogItem()
            {
                CategoryId = categoryId
            };

            FillObject(row, item);

            retval.Add(item);
        }

        return retval;
    }

    public (List<CatalogItem> GoodRows, List<CatalogItem> BadRows) ConvertFileData(string fileData, List<Category> categories)
    {
        var goodRows = new List<CatalogItem>();
        var badRows = new List<CatalogItem>();

        var fixedCsvFileContent = Regex.Replace(fileData, @"(?!(([^""]*""){2})*[^""]*$)\n+", "<br>");
        var dt = DataTable.New.ReadFromString(fixedCsvFileContent);
        string[] columns = dt.ColumnNames.ToArray();

        foreach (Row row in dt.Rows)
        {
            var item = new CatalogItemImport();

            FillObject(row, item);

            var cat = categories.FirstOrDefault(i => i.CategoryName.Equals(item.CategoryName, StringComparison.OrdinalIgnoreCase));

            if (cat == null)
                badRows.Add(item);
            else
            {
                item.CategoryId = cat.CategoryId;
                goodRows.Add(item);
            }

        }

        return (goodRows, badRows);
    }

    private void FillObject<T>(Row row, T target)
        where T : class
    {
        var props = typeof(T).GetProperties().ToList();

        foreach (PropertyInfo prop in props)
        {
            var columnName = prop.Name;

            if (row.ContainsColumn(columnName))
            {
                var value = row[columnName];

                Type t = prop.PropertyType;

                if (value == null && t == typeof(string))
                    prop.SetValue(target, " ", null);
                else if (t == typeof(string))
                    prop.SetValue(target, value, null);
                else if ((t == typeof(int) || t == typeof(int?)) && int.TryParse(value, out int i))
                    prop.SetValue(target, i, null);
                else if ((t == typeof(decimal) || t == typeof(decimal?)) && decimal.TryParse(value, out decimal de))
                    prop.SetValue(target, de, null);
                else if ((t == typeof(long) || t == typeof(long?)) && long.TryParse(value, out long l))
                    prop.SetValue(target, l, null);
                else if ((t == typeof(DateTime) || t == typeof(DateTime?)) && DateTime.TryParse(value, out DateTime dt))
                    prop.SetValue(target, dt, null);
                else if ((t == typeof(bool) || t == typeof(bool?)) && bool.TryParse(value, out bool b))
                    prop.SetValue(target, b, null);
                else if ((t == typeof(Guid) || t == typeof(Guid?)) && Guid.TryParse(value, out Guid g))
                    prop.SetValue(target, g, null);
            }
        }
    }
}