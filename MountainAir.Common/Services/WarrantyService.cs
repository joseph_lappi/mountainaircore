﻿using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MountainAir.Common.Security;

namespace MountainAir.Common.Services;

public interface IWarrantyService
{
    Task<List<Warranty>> GetWarrantiesAsync(Guid estimateId);
    Task<Warranty> GetWarrantyAsync(Guid id);
    Task<Warranty> AddWarrantyAsync(Warranty warranty, ApplicationUser user);
    Task<Warranty> UpdateWarrantyAsync(Warranty warranty, ApplicationUser user);
    Task DeleteWarrantyAsync(Guid id);
    Task<bool> WarrantyExists(Guid id);
}

public class WarrantyService : IWarrantyService
{
    private readonly ApplicationDbContext _context;

    public WarrantyService(ApplicationDbContext context)
    {
        _context = context;
    }

    public Task<bool> WarrantyExists(Guid id)
    {
	    return _context.Warranties.AnyAsync(i => i.Id == id);
    }

    public async Task<List<Warranty>> GetWarrantiesAsync(Guid estimateId)
    {
        var data = await _context.Warranties
            .AsNoTracking()
            .Where(w => w.EstimateId == estimateId)
            .ToListAsync();
        return data;
    }

    //private async Task FillTerm(Warranty warranty)
    //{
    //    var type = await _context.WarrantyTypes.FirstAsync(i => i.Id == warranty.WarrantyTypeId);

    //    string term = null;
    //    DateTime? expirationDate = null;
        
    //    switch (warranty.TermType)
    //    {
    //        case TermTypes.Year:
    //            term = $"{warranty.TermLength} YR";
    //            warranty.ExpirationDate = DateTime.Now.AddYears(warranty.TermLength);
    //            warranty.Value = warranty.TermLength == 10 ? type.TenYearValue : type.FifteenYearValue;
    //            break;
    //        case TermTypes.LifeTime:
    //            term = "Lifetime";
    //            warranty.ExpirationDate = null;
    //            warranty.Value = type.LifetimeValue;
    //            warranty.TermLength = 0;
    //            break;
    //        default:
    //            throw new ArgumentOutOfRangeException(nameof(warranty.TermType), warranty.TermType, null);
    //    }

    //    warranty.ServiceFee = type.ServiceFee;
    //    warranty.TermText = term;
    //}
    
    //public async Task<Warranty> CreateWarrantyFromEstimateAsync(Guid estimateId, Guid warrantyTypeId, TermTypes termType, int termLength)
    //{
    //    var estimate = await _context.Estimates
    //        .AsNoTracking()
    //        .Include(c => c.Customer)
    //        .Include(c => c.Address)
    //        .FirstOrDefaultAsync(i => i.EstimateId == estimateId);

    //    if (estimate == null)
    //        throw new ArgumentNullException(nameof(estimate));
        
    //    var warranty = new Warranty()
    //    {
    //        AddDate = DateTime.Now,
    //        AddedBy = estimate.DesignerName,
    //        AddressLine1 = estimate.Address.AddressLine1,
    //        AddressLine2 = estimate.Address.AddressLine2,
    //        City = estimate.Address.City,
    //        State = estimate.Address.State,
    //        PostalCode = estimate.Address.Zip,
    //        CustomerName = estimate.Customer.DisplayName,
    //        CustomerSignedDate = estimate.ApprovalDate,
    //        EffectiveDate = estimate.InstallationDate.GetValueOrDefault(DateTime.Now),
    //        EstimateId = estimate.EstimateId,
    //        Id = Guid.NewGuid(),
    //        InstallDate = estimate.InstallationDate.GetValueOrDefault(DateTime.Now),
    //        TermLength = termLength,
    //        TermType = termType,
    //        WarrantyTypeId = warrantyTypeId
    //    };

    //    await FillTerm(warranty);

    //    _context.Warranties.Add(warranty);
    //    await _context.SaveChangesAsync();

    //    return warranty;
    //}

    public Task<Warranty> GetWarrantyAsync(Guid id)
    {
        return _context.Warranties.FirstOrDefaultAsync(i => i.Id == id);
    }
    
    public async Task<Warranty> AddWarrantyAsync(Warranty warranty, ApplicationUser user)
    {
        warranty.AddDate = DateTime.Now;
        warranty.AddedBy = user.DisplayName;
        
        _context.Warranties.Add(warranty);
        
        await _context.SaveChangesAsync();
        
        return warranty;
    }

    public async Task<Warranty> UpdateWarrantyAsync(Warranty warranty, ApplicationUser user)
    {
        warranty.UpdateDate = DateTime.Now;
        warranty.UpdateBy = user.DisplayName;

        var estimate = await _context.Estimates.FirstOrDefaultAsync(i => i.EstimateId == warranty.EstimateId);

        warranty.InstallDate ??= estimate.InstallationDate;

        warranty.CustomerSignedDate ??= estimate.ApprovalDate;

        warranty.EffectiveDate ??= estimate.ApprovalDate;

        _context.Warranties.Update(warranty);
        
        await _context.SaveChangesAsync();
        
        return warranty;
    }

    public async Task DeleteWarrantyAsync(Guid id)
    {
        var warranty = await _context.Warranties.FirstOrDefaultAsync(i => i.Id == id);

        if (warranty != null)
        {
            _context.Warranties.Remove(warranty);
            await _context.SaveChangesAsync();
        }
    }
}