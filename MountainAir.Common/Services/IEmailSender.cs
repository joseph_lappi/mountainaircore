﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MountainAir.Common.Models;
using SendGrid;

namespace MountainAir.Common.Services;

public interface IEmailSender
{
    Task<bool> SendEmailAsync(EmailMessage message, IEnumerable<IFormFile> files = null, string sender = null);
    Task SendEmailConfirmationAsync(string email, string link);
    Task SendPasswordResetEmailAsync(string email, string callbackUrl);
}