﻿//using MountainAir.Common.AzureStorage;
//using MountainAir.Common.Configuration;
//using NLog;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using Adobe.PDFServicesSDK.auth;
//using Adobe.PDFServicesSDK.io;
//using Adobe.PDFServicesSDK.options.createpdf;
//using Adobe.PDFServicesSDK.pdfops;
//using Microsoft.Extensions.Configuration;

//namespace MountainAir.Common.Services;

//public interface IAdobeDocumentService
//{
//    byte[] CreatePdfFromHtml(string html);
//    byte[] CreatePdfFromStream(Stream stream, string mediaType);
//    byte[] CreatePdfFromUrl(string url);
//    byte[] CreatePdfFromHtml(byte[] bytes);
//    byte[] CreatePdfFromBytes(byte[] bytes, string mediaType);
//}

//public class AdobeDocumentService : IAdobeDocumentService
//{
//    private class AdobeConfig
//    {
//        public AdobeConfig(IConfiguration configuration)
//        {
//            var section = configuration.GetSection("Adobe");

//            ClientId = section.GetValue<string>("ClientId");
//            ClientSecret = configuration.GetValue<string>("Adobe.ClientSecret");
//            PrivateKeyFileContents = configuration.GetValue<string>("Adobe.Base64_Encoded_Private_Key");
//            OrganizationId = section.GetValue<string>("OrganizationId");
//            AccountId = section.GetValue<string>("AccountId");
//        }

//        public string ClientId { get; set; }
//        public string ClientSecret { get; set; }
//        public string PrivateKeyFileContents { get; set; }
//        public string OrganizationId { get; set; }
//        public string AccountId { get; set; }
//    }

//    private readonly ILogger _logger = LogManager.GetCurrentClassLogger();
//    private readonly AdobeConfig _configuration;

//    public AdobeDocumentService(IConfiguration configuration)
//    {
//        _configuration = new AdobeConfig(configuration);
//    }

//    private Credentials CreateCredentials()
//    {
//        var cfg = _configuration;

//        var privateBytes = Convert.FromBase64String(cfg.PrivateKeyFileContents);
//        var privateKey = Encoding.UTF8.GetString(privateBytes);

//        Credentials credentials = new ServiceAccountCredentials.Builder()
//            .WithClientId(cfg.ClientId)
//            .WithClientSecret(cfg.ClientSecret)
//            .WithPrivateKey(privateKey)
//            .WithOrganizationId(cfg.OrganizationId)
//            .WithAccountId(cfg.AccountId)
//            .Build();
//        return credentials;
//    }
    
//    public byte[] CreatePdfFromHtml(string html)
//    {
//        var bytes = Encoding.UTF8.GetBytes(html); 
        
//        return CreatePdfFromHtml(bytes);
//    }
    
//    public byte[] CreatePdfFromHtml(byte[] bytes)
//    {        
//        var source = new MemoryStream(bytes);

//        var mediaType = MimeTypeUtility.GetMimeType(".html");
        
//        return CreatePdfFromStream(source, mediaType);
//    }
    
//    public byte[] CreatePdfFromBytes(byte[] bytes, string mediaType)
//    {        
//        var source = new MemoryStream(bytes);
        
//        return CreatePdfFromStream(source, mediaType);
//    }

//    public byte[] CreatePdfFromStream(Stream stream, string mediaType)
//    {
//        // Set operation input from a source file.
//        var source = FileRef.CreateFromStream(stream, mediaType);

//        return CreatePdf(source);
//    }
    
//    public byte[] CreatePdfFromUrl(string url)
//    {
//        // Set operation input from a source file.
//        var source = FileRef.CreateFromURI(new Uri(url));

//        return CreatePdf(source);
//    }

//    private byte[] CreatePdf(FileRef source)
//    {
//        try
//        {
//            if (source == null)
//            {
//                throw new ArgumentNullException(nameof(source));
//            }
//            /*
//             * Adobe does not allow use to reuse an execution context. This is a streamlined as I could make it for now.
//             */
            
//            // Initial setup, create credentials instance.
//            var credentials = CreateCredentials();

//            //Create an ExecutionContext using credentials and create a new operation instance.
//            var executionContext = Adobe.PDFServicesSDK.ExecutionContext.Create(credentials);

//            var pdfOperations = CreatePDFOperation.CreateNew();
            
//            pdfOperations.SetInput(source);

//            // Provide any custom configuration options for the operation.
//            SetCustomOptions(pdfOperations);

//            // Execute the operation.
//            var result = pdfOperations.Execute(executionContext);

//            var ms = new MemoryStream();
//            //Generating a file name
//            // Save the result to the specified location.
//            result.SaveAs(ms);

//            return ms.ToArray();
//        }
//        catch (Exception ex)
//        {
//            _logger.Error(ex, $"Exception encountered while executing operation: {ex.GetBaseException().Message}");
//            throw;
//        }
//    }
    
//    /// <summary>
//    /// Sets any custom options for the operation.
//    /// </summary>
//    /// <param name="htmlToPDFOperation">operation instance for which the options are provided.</param>
//    private static void SetCustomOptions(CreatePDFOperation pdfOperations)
//    {
//        // Define the page layout, in this case an 8 x 11.5 inch page (effectively portrait orientation).
//        var pageLayout = new Adobe.PDFServicesSDK.options.createpdf.PageLayout();
//        pageLayout.SetPageSize(8, 11.5);

//        // Set the desired HTML-to-PDF conversion options.
//        var htmlToPdfOptions = CreatePDFOptions.HtmlOptionsBuilder()
//            .IncludeHeaderFooter(true)
//            .WithPageLayout(pageLayout)
//            .Build();
//        pdfOperations.SetOptions(htmlToPdfOptions);
//    }
//}