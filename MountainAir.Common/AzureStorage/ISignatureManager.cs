﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using MountainAir.Common.Data.Models;

namespace MountainAir.Common.AzureStorage;

public interface ISignatureManager
{
    Task<BlobViewModel> AddAsync(Guid estimateId, IFormFile formFile, CancellationToken cancellationToken = default);
    Task<byte[]> GetAsync(Guid estimateId, string extension = "png", CancellationToken cancellationToken = default);
    Task<AzureActionResult> DeleteAsync(Guid estimateId, string extension = "png", CancellationToken cancellationToken = default);
    Task<bool> FileExistsAsync(Guid estimateId, string extension = "png", CancellationToken cancellationToken = default);
}