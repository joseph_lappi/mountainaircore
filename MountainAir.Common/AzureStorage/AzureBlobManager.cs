﻿//using Azure;
//using Azure.Storage.Blobs;
//using Azure.Storage.Blobs.Models;
//using Microsoft.AspNetCore.Http;
//using Microsoft.EntityFrameworkCore;
//using Microsoft.Extensions.Configuration;
//using MountainAir.Common.Data;
//using MountainAir.Common.Data.Models;
//using System;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using System.Threading;
//using System.Threading.Tasks;

//namespace MountainAir.Common.AzureStorage;

//public sealed class AzureBlobManager : IAzureBlobManager
//{
//    private static BlobServiceClient _storageAccount;
//    private readonly ApplicationDbContext _applicationDbContext;
    
//    public AzureBlobManager(ApplicationDbContext applicationDbContext, IConfiguration configuration)
//    {
//        _applicationDbContext = applicationDbContext;

//        var connectionString = configuration["AzureBlobStorage"];
//        _storageAccount = new BlobServiceClient(connectionString);
//    }
    
//    public AzureBlobManager(string dbConnectionString, string storageConnectionString)
//    {
//        _applicationDbContext = new ApplicationDbContext(dbConnectionString);
//        _storageAccount = new BlobServiceClient(storageConnectionString);
//    }

//    private static BlobContainerClient GetClient(string connectionString, AzureEnvironments environment)
//    {
//        _storageAccount = new BlobServiceClient(connectionString);
//        return _storageAccount.GetBlobContainerClient(environment.ToString().ToLower());
//    }
    
//    public async Task<List<AzureBlobContainer>> GetStoredContainers()
//    {
//        return await _applicationDbContext.AzureBlobContainers.Where(c => !c.Completed).ToListAsync();
//    }

//    public async Task<IEnumerable<AzureBlobContainer>> GetAllContainers(string prefix = null, CancellationToken cancellationToken = default)
//    {
//        var retval = new List<AzureBlobContainer>();

//        await foreach (var container in _storageAccount.GetBlobContainersAsync(BlobContainerTraits.None, prefix: prefix, cancellationToken: cancellationToken))
//        {
//            var containerRecord = new AzureBlobContainer
//            {
//                Id = container.Name,
//            };

//            retval.Add(containerRecord);
//        }

//        return retval;
//    }

//    public async Task<IEnumerable<BlobViewModel>> GetEstimateFiles(Guid estimateId, CancellationToken cancellationToken = default)
//    {
//        var blobs = new List<BlobViewModel>();

//        var blobContainer = _storageAccount.GetBlobContainerClient(estimateId.ToString());

//        if (await blobContainer.ExistsAsync(cancellationToken))
//        {
//            await foreach (var blob in blobContainer.GetBlobsAsync(cancellationToken: cancellationToken))
//            {
//                var blobClient = blobContainer.GetBlobClient(blob.Name);

//                blobs.Add(new BlobViewModel
//                {
//                    EstimateId = estimateId,
//                    Name = blob.Name,
//                    Url = blobClient.Uri.AbsoluteUri
//                });
//            }
//        }

//        return blobs;
//    }
    
//    public async Task<Response<BlobDownloadInfo>> GetEstimateFile(Guid estimateId, string fileName,
//        CancellationToken cancellationToken = default)
//    {
//        var blobContainer = _storageAccount.GetBlobContainerClient(estimateId.ToString());

//        if (await blobContainer.ExistsAsync(cancellationToken))
//        {
//            var blobClient = blobContainer.GetBlobClient(fileName);

//            var download = await blobClient.DownloadAsync(cancellationToken);

//            return download;
//        }

//        return null;
//    }

//    public async Task DownloadContainerContents(string rootDirectoryPath)
//    {
//        var containers = await GetAllContainers();

//        foreach (var container in containers)
//        {
//            await DownloadContainerContents(container.Id, rootDirectoryPath);
//        }
//    }

//    public async Task DownloadContainerContents(string containerName, string rootDirectoryPath)
//    {
//        try
//        {
//            Console.WriteLine($"Processing {containerName}");
//            var blobContainer = _storageAccount.GetBlobContainerClient(containerName);

//            if (await blobContainer.ExistsAsync())
//            {
//                if (!await _applicationDbContext.AzureBlobContainers.AnyAsync(i => i.Id == containerName))
//                {
//                    var containerRecord = new AzureBlobContainer()
//                    {
//                        Id = containerName,
//                        Completed = true
//                    };

//                    _applicationDbContext.AzureBlobContainers.Add(containerRecord);
//                    await _applicationDbContext.SaveChangesAsync();
//                }

//                string basePath = Path.Combine(rootDirectoryPath, containerName);

//                if (!Directory.Exists(basePath))
//                    Directory.CreateDirectory(basePath);
                
//                var currentList = await _applicationDbContext.AzureContainerFiles.Where(c => c.ContainerName == containerName).ToListAsync();

//                await foreach (var blob in blobContainer.GetBlobsAsync())
//                {
//                    string filePath = Path.Combine(basePath, blob.Name);

//                    if (!await _applicationDbContext.AzureContainerFiles.AnyAsync(i => i.ContainerName == containerName && i.Name == blob.Name))
//                    {
//                        var newItem = new AzureContainerFile()
//                        {
//                            ContainerName = containerName,
//                            Name = blob.Name,
//                            FilePath = filePath
//                        };

//                        _applicationDbContext.AzureContainerFiles.Add(newItem);
//                        await _applicationDbContext.SaveChangesAsync();
//                    }

//                    if (!File.Exists(filePath))
//                    {
//                        try
//                        {
//                            var blobClient = blobContainer.GetBlobClient(blob.Name);

//                            Console.WriteLine($"Processing {containerName} - {blob.Name}");

//                            var fileData = await blobClient.DownloadAsync();

//                            await using var stream = fileData.Value.Content;

//                            await using var fileStream = File.Create(filePath);

//                            await stream.CopyToAsync(fileStream);
//                        }
//                        catch (Exception ex)
//                        {
//                            Console.WriteLine(ex.GetBaseException().Message);
//                        }
//                    }
//                }
//            }
//        }
//        catch (Exception ex)
//        {
//            Console.WriteLine(ex.GetBaseException().Message);
//        }
//    }

//    public async Task<bool> Delete(Guid estimateId, string fileName, CancellationToken cancellationToken = default)
//    {
//        string path = $"{estimateId}/{fileName}";
        
//        var blobContainer = _storageAccount.GetBlobContainerClient(estimateId.ToString());

//        var response = await blobContainer.DeleteBlobIfExistsAsync(path, DeleteSnapshotsOption.IncludeSnapshots, cancellationToken: cancellationToken);

//        return response is { Value: true };
//    }

//    public async Task<BlobViewModel> AddFile(Guid estimateId, IFormFile formFile, CancellationToken cancellationToken = default)
//    {
//        if (formFile is not { Length: > 0 })
//            return null;
        
//        var blobContainer = _storageAccount.GetBlobContainerClient(estimateId.ToString());
        
//        if (!await blobContainer.ExistsAsync(cancellationToken))
//        {
//            await blobContainer.CreateAsync(PublicAccessType.Blob, cancellationToken: cancellationToken);
//        }

//        var blockBlob = blobContainer.GetBlobClient(formFile.FileName);

//        var options = new BlobUploadOptions
//        {
//            HttpHeaders = new BlobHttpHeaders
//            {
//                ContentType = formFile.ContentType
//            },
//            AccessTier = AccessTier.Cool
//        };

//        await using var stream = formFile.OpenReadStream();

//        await blockBlob.UploadAsync(stream, options, cancellationToken);

//        var retval = new BlobViewModel
//        {
//            EstimateId = estimateId,
//            Name = blockBlob.Name,
//            Url = blockBlob.Uri.AbsoluteUri
//        };

//        return retval;
//    }
        
//    //public async Task UploadFiles()
//    //{
//    //    try
//    //    {
//    //        var containers = await _applicationDbContext.AzureBlobContainers
//    //            .AsNoTracking()
//    //            .Where(c => c.Files.Any(f => !f.Processed))
//    //            .ToListAsync();
            
//    //        foreach (var container in containers)
//    //        {
//    //            try
//    //            {
//    //                var files = await _applicationDbContext.AzureContainerFiles.Where(f => f.ContainerName == container.Id && !f.Processed).ToListAsync();

//    //                foreach (var file in files)
//    //                {
//    //                    Console.WriteLine($"Processing {container.Id} - {file.Name}");

//    //                    try
//    //                    {
//    //                        string path = $"{container.Id}/{file.Name}";

//    //                        var blockBlob = _blobContainer.GetBlobClient(path);

//    //                        if (await blockBlob.ExistsAsync())
//    //                        {
//    //                            file.Processed = true;
//    //                            _applicationDbContext.AzureContainerFiles.Update(file);
//    //                            await _applicationDbContext.SaveChangesAsync();
//    //                            continue;
//    //                        }

//    //                        var options = new BlobUploadOptions
//    //                        {
//    //                            HttpHeaders = new BlobHttpHeaders
//    //                            {
//    //                                ContentType = MimeTypeUtility.GetMimeType(Path.GetExtension(file.Name))
//    //                            }
//    //                        };

//    //                        await using var fs = new FileStream(file.FilePath, FileMode.Open, FileAccess.Read);

//    //                        await blockBlob.UploadAsync(fs, options);
                                
//    //                        file.Processed = true;
//    //                        _applicationDbContext.AzureContainerFiles.Update(file);
//    //                        await _applicationDbContext.SaveChangesAsync();
//    //                    }
//    //                    catch (Exception ex)
//    //                    {
//    //                        Console.WriteLine(ex.GetBaseException().Message);
//    //                    }
//    //                }
//    //            }
//    //            catch (Exception ex)
//    //            {
//    //                Console.WriteLine(ex.GetBaseException().Message);
//    //            }
//    //        }
//    //    }
//    //    catch (Exception ex)
//    //    {
//    //        Console.WriteLine(ex.GetBaseException().Message);
//    //    }
//    //}
    
//    public async Task<bool> FileExists(Guid estimateId, string fileName, CancellationToken cancellationToken = default)
//    {
//        var path = $"{estimateId}/{fileName}";
        
//        var blobContainer = _storageAccount.GetBlobContainerClient(estimateId.ToString());

//        var container = blobContainer.GetBlobClient(path);

//        return await container.ExistsAsync(cancellationToken);
//    }
//}