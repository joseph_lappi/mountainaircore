﻿namespace MountainAir.Common.AzureStorage;

public enum AzureEnvironments
{
    Dev,
    Prod
}