﻿using Azure;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Models;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MountainAir.Common.AzureStorage;

public class AzureStorageManager : IAzureBlobManager
{
    private readonly ILogger _logger = LogManager.GetCurrentClassLogger();
    private static BlobServiceClient _storageAccount;
    private readonly ApplicationDbContext _applicationDbContext;
    private readonly BlobContainerClient _blobContainer;

    public AzureStorageManager(ApplicationDbContext applicationDbContext, IConfiguration configuration)
    {
        _applicationDbContext = applicationDbContext;

        var connectionString = configuration["AzureBlobStorage"];
        var environment = configuration["Environment"];

        _blobContainer = GetClient(connectionString, Enum.TryParse<AzureEnvironments>(environment, out var azureEnvironment) ? azureEnvironment : AzureEnvironments.Dev);
    }

    public AzureStorageManager(string dbConnectionstring, string storageConnectionString, AzureEnvironments environment)
    {
        _blobContainer = GetClient(storageConnectionString, environment);
        _applicationDbContext = new ApplicationDbContext(dbConnectionstring);
    }

    private static BlobContainerClient GetClient(string storageConnectionString, AzureEnvironments environment)
    {
        _storageAccount = new BlobServiceClient(storageConnectionString);
        return _storageAccount.GetBlobContainerClient(environment.ToString().ToLower());
    }

    public async Task<List<AzureBlobContainer>> GetStoredContainers()
    {
        return await _applicationDbContext.AzureBlobContainers.Where(c => !c.Completed).ToListAsync();
    }
    
    private static string GetAttachmentFilePath(Guid messageId, string fileName) => $"attachments/{messageId}/{fileName}";
    private static string GetFilePath(Guid estimateId, string fileName) => $"{estimateId}/{fileName}";

    public async Task<IEnumerable<AzureBlobContainer>> GetAllContainers(string prefix = null, CancellationToken cancellationToken = default)
    {
        var retval = new List<AzureBlobContainer>();

        await foreach (var container in _storageAccount.GetBlobContainersAsync(BlobContainerTraits.None, prefix: prefix, cancellationToken: cancellationToken))
        {
            var containerRecord = new AzureBlobContainer
            {
                Id = container.Name,
            };

            retval.Add(containerRecord);
        }

        return retval;
    }

    public async Task<IEnumerable<BlobViewModel>> GetEstimateFiles(Guid estimateId, CancellationToken cancellationToken = default)
    {
        var blobs = new List<BlobViewModel>();

        await foreach (var blob in _blobContainer.GetBlobsAsync(prefix: $"{estimateId}", cancellationToken: cancellationToken))
        {
            var blobClient = _blobContainer.GetBlobClient(blob.Name);

            blobs.Add(new BlobViewModel
            {
                EstimateId = estimateId,
                Name = blob.Name,
                Url = blobClient.Uri.AbsoluteUri
            });
        }

        return blobs;
    }

    public async Task<Response<BlobDownloadInfo>> GetEstimateFile(Guid estimateId, string fileName, CancellationToken cancellationToken = default)
    {
        string filePath = GetFilePath(estimateId, fileName);

        var blobClient = _blobContainer.GetBlobClient(filePath);

        var download = await blobClient.DownloadAsync(cancellationToken);

        return download;
    }

    public async Task DownloadContainerContents(string rootDirectoryPath)
    {
        var containers = await GetAllContainers();

        foreach (var container in containers)
        {
            await DownloadContainerContents(container.Id, rootDirectoryPath);
        }
    }

    public async Task DownloadContainerContents(string containerName, string rootDirectoryPath)
    {
        try
        {
            Console.WriteLine($"Processing {containerName}");
            var blobContainer = _storageAccount.GetBlobContainerClient(containerName);

            if (await blobContainer.ExistsAsync())
            {
                if (!await _applicationDbContext.AzureBlobContainers.AnyAsync(i => i.Id == containerName))
                {
                    var containerRecord = new AzureBlobContainer()
                    {
                        Id = containerName,
                        Completed = true
                    };

                    _applicationDbContext.AzureBlobContainers.Add(containerRecord);
                    await _applicationDbContext.SaveChangesAsync();
                }

                string basePath = Path.Combine(rootDirectoryPath, containerName);

                if (!Directory.Exists(basePath))
                    Directory.CreateDirectory(basePath);

                var currentList = await _applicationDbContext.AzureContainerFiles.Where(c => c.ContainerName == containerName).ToListAsync();

                await foreach (var blob in blobContainer.GetBlobsAsync())
                {
                    string filePath = Path.Combine(basePath, blob.Name);

                    if (!await _applicationDbContext.AzureContainerFiles.AnyAsync(i => i.ContainerName == containerName && i.Name == blob.Name))
                    {
                        var newItem = new AzureContainerFile()
                        {
                            ContainerName = containerName,
                            Name = blob.Name,
                            FilePath = filePath
                        };

                        _applicationDbContext.AzureContainerFiles.Add(newItem);
                        await _applicationDbContext.SaveChangesAsync();
                    }

                    if (!File.Exists(filePath))
                    {
                        try
                        {
                            var blobClient = blobContainer.GetBlobClient(blob.Name);

                            Console.WriteLine($"Processing {containerName} - {blob.Name}");

                            var fileData = await blobClient.DownloadAsync();

                            await using var stream = fileData.Value.Content;

                            await using var fileStream = File.Create(filePath);

                            await stream.CopyToAsync(fileStream);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.GetBaseException().Message);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.GetBaseException().Message);
        }
    }

    public async Task<bool> Delete(Guid estimateId, string fileName, CancellationToken cancellationToken = default)
    {
        string filePath = GetFilePath(estimateId, fileName);

        var response = await _blobContainer.DeleteBlobIfExistsAsync(filePath, DeleteSnapshotsOption.IncludeSnapshots, cancellationToken: cancellationToken);

        return response is { Value: true };
    }
    
    public async Task<bool> AddAttachment(Guid emailMessageId, IFormFile formFile, CancellationToken cancellationToken = default)
    {
        try
        {
            if (formFile is not { Length: > 0 })
                return false;

            string filePath = GetAttachmentFilePath(emailMessageId, formFile.FileName);

            var blockBlob = _blobContainer.GetBlobClient(filePath);

            var options = new BlobUploadOptions
            {
                HttpHeaders = new BlobHttpHeaders
                {
                    ContentType = formFile.ContentType
                },
                AccessTier = AccessTier.Cool
            };

            await using var stream = formFile.OpenReadStream();

            await blockBlob.UploadAsync(stream, options, cancellationToken);

            return true;
        }
        catch (Exception ex)
        {
            _logger.Error(ex, $"Error saving attachment: {ex.GetBaseException().Message}");
        }
        return false;
    }
    
    public async Task<bool> AddAttachment(Guid messageId, EmailAttachment formFile)
    {
	    try
	    {
		    if (formFile == null || formFile.Content.Length == 0)
			    return false;

		    string filePath = GetAttachmentFilePath(messageId, formFile.FileName);

		    var blockBlob = _blobContainer.GetBlobClient(filePath);

		    var options = new BlobUploadOptions
		    {
			    HttpHeaders = new BlobHttpHeaders
			    {
				    ContentType = formFile.ContentType
			    },
			    AccessTier = AccessTier.Cool
		    };

		    var stream = GetAttachmentContent(formFile);

		    await blockBlob.UploadAsync(stream, options);

		    return true;
	    }
	    catch (Exception ex)
	    {
		    _logger.Error(ex, $"Error saving attachment: {ex.GetBaseException().Message}");
	    }
	    return false;
    }

    private Stream GetAttachmentContent(EmailAttachment attachment)
    {
	    var bytes = Convert.FromBase64String(attachment.Content);
	    var ms = new MemoryStream(bytes);
	    return ms;
    }

    public async Task<BlobViewModel> AddFile(Guid estimateId, IFormFile formFile, CancellationToken cancellationToken = default)
    {
        if (formFile is not { Length: > 0 })
            return null;

        string filePath = GetFilePath(estimateId, formFile.FileName);

        var blockBlob = _blobContainer.GetBlobClient(filePath);

        var options = new BlobUploadOptions
        {
            HttpHeaders = new BlobHttpHeaders
            {
                ContentType = formFile.ContentType
            },
            AccessTier = AccessTier.Cool
        };

        await using var stream = formFile.OpenReadStream();

        await blockBlob.UploadAsync(stream, options, cancellationToken);

        var retval = new BlobViewModel
        {
            EstimateId = estimateId,
            Name = blockBlob.Name,
            Url = blockBlob.Uri.AbsoluteUri
        };

        return retval;
    }

    public async Task UploadFiles()
    {
        try
        {
            var files = await _applicationDbContext.AzureContainerFiles.Where(f => !f.Processed).ToListAsync();

            foreach (var file in files)
            {
                Console.WriteLine($"Processing {file.ContainerName} - {file.Name}");

                try
                {
                    string path = $"{file.ContainerName}/{file.Name}";

                    var blockBlob = _blobContainer.GetBlobClient(path);

                    if (await blockBlob.ExistsAsync())
                    {
                        file.Processed = true;
                        _applicationDbContext.AzureContainerFiles.Update(file);
                        await _applicationDbContext.SaveChangesAsync();
                        continue;
                    }

                    var options = new BlobUploadOptions
                    {
                        HttpHeaders = new BlobHttpHeaders
                        {
                            ContentType = MimeTypeUtility.GetMimeType(Path.GetExtension(file.Name))
                        }
                    };

                    await using var fs = new FileStream(file.FilePath, FileMode.Open, FileAccess.Read);

                    await blockBlob.UploadAsync(fs, options);

                    file.Processed = true;
                    _applicationDbContext.AzureContainerFiles.Update(file);
                    await _applicationDbContext.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.GetBaseException().Message);
                }
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.GetBaseException().Message);
        }
    }

    public async Task<bool> FileExists(Guid estimateId, string fileName, CancellationToken cancellationToken = default)
    {
        string filePath = GetFilePath(estimateId, fileName);

        var blobClient = _blobContainer.GetBlobClient(filePath);

        return await blobClient.ExistsAsync(cancellationToken);
    }
}