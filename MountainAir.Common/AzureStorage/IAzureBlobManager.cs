﻿using Microsoft.AspNetCore.Http;
using MountainAir.Common.Data.Models;
using System;
using System.Collections.Generic;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Azure;
using Azure.Storage.Blobs.Models;
using MountainAir.Common.Data;
using MountainAir.Common.Models;

namespace MountainAir.Common.AzureStorage;

public interface IAzureBlobManager
{
    Task<IEnumerable<AzureBlobContainer>> GetAllContainers(string prefix = null, CancellationToken cancellationToken = default);
    Task<IEnumerable<BlobViewModel>> GetEstimateFiles(Guid estimateId, CancellationToken cancellationToken = default);
    Task<Response<BlobDownloadInfo>> GetEstimateFile(Guid estimateId, string fileName,
        CancellationToken cancellationToken = default);
    Task<bool> Delete(Guid estimateId, string fileName, CancellationToken cancellationToken = default);
    Task<BlobViewModel> AddFile(Guid estimateId, IFormFile formFile, CancellationToken cancellationToken = default);
    Task<bool> FileExists(Guid estimateId, string fileName, CancellationToken cancellationToken = default);
    Task<bool> AddAttachment(Guid emailMessageId, IFormFile formFile, CancellationToken cancellationToken = default);
    Task<bool> AddAttachment(Guid messageId, EmailAttachment formFile);
}