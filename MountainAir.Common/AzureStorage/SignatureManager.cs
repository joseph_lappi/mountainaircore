﻿using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using MountainAir.Common.Data.Models;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace MountainAir.Common.AzureStorage;

public sealed class SignatureManager : ISignatureManager
{
    private readonly BlobContainerClient _blobContainer;

    public SignatureManager(string connectionString, AzureEnvironments environment)
    {
        _blobContainer = GetClient(connectionString, environment);
    }

    public SignatureManager(IConfiguration configuration)
    {
        var connectionString = configuration["AzureBlobStorage"];
        var environment = configuration["Environment"];
        
        _blobContainer = GetClient(connectionString, Enum.TryParse<AzureEnvironments>(environment, out var azureEnvironment) ? azureEnvironment : AzureEnvironments.Dev);
    }
    
    private static BlobContainerClient GetClient(string connectionString, AzureEnvironments environment)
    {
        var storageAccount = new BlobServiceClient(connectionString);
        return storageAccount.GetBlobContainerClient(environment.ToString().ToLower());
    }

    private static string GetFileName(Guid estimateId, string extension = "png") => $"vault/{estimateId}.{extension.Replace(".", string.Empty)}";

    public async Task<BlobViewModel> AddAsync(Guid estimateId, IFormFile formFile, CancellationToken cancellationToken = default)
    {
        if (formFile is not { Length: > 0 })
            return null;
        
        string fileExtension = Path.GetExtension(formFile.FileName);

        string fullName = GetFileName(estimateId, fileExtension);

        var blockBlob = _blobContainer.GetBlobClient(fullName);

        var options = new BlobUploadOptions
        {
            HttpHeaders = new BlobHttpHeaders
            {
                ContentType = formFile.ContentType
            }
        };

        await using var stream = formFile.OpenReadStream();

        var info = await blockBlob.UploadAsync(stream, options, cancellationToken);

        var retval = new BlobViewModel
        {
            EstimateId = estimateId,
            Name = fullName
        };

        return retval;
    }
    
    public async Task<byte[]> GetAsync(Guid estimateId, string extension = "png", CancellationToken cancellationToken = default)
    {
        string fullName = GetFileName(estimateId, extension);

        var blobClient = _blobContainer.GetBlobClient(fullName);

        var exists = await blobClient.ExistsAsync(cancellationToken); 

        if (!exists)
        {
            return Array.Empty<byte>();
        }

        var content = await blobClient.DownloadContentAsync(cancellationToken);

        return content.Value.Content.ToArray();
    }
    
    public async Task<AzureActionResult> DeleteAsync(Guid estimateId, string extension = "png", CancellationToken cancellationToken = default)
    {
        try
        {
            string fullName = GetFileName(estimateId, extension);

            var response = await _blobContainer.DeleteBlobIfExistsAsync(fullName, DeleteSnapshotsOption.IncludeSnapshots, cancellationToken: cancellationToken);

            string message = response ? fullName + " deleted" : fullName + " not deleted";

            return new AzureActionResult { Successful = response, Message = message };
        }
        catch (Exception ex)
        {
            return new AzureActionResult { Successful = false, Message = "error deleting fileName " + ex.GetBaseException().Message };
        }
    }
    
    public async Task<bool> FileExistsAsync(Guid estimateId, string extension = "png", CancellationToken cancellationToken = default)
    {
        string fullName = GetFileName(estimateId, extension);

        var blobClient = _blobContainer.GetBlobClient(fullName);
        
        return await blobClient.ExistsAsync(cancellationToken);
    }
}