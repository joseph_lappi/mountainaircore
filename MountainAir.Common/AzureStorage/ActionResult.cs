﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MountainAir.Common.AzureStorage;

public sealed class AzureActionResult
{
    public bool Successful { get; set; }
    public string Message { get; set; }
}