﻿using MountainAir.Common.JsonConverters;
using Newtonsoft.Json;
using System;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace MountainAir.Common.Utility;

public static class ZipUtil
{
    private static readonly JsonSerializerSettings Settings = new()
    {
        DefaultValueHandling = DefaultValueHandling.Ignore,
        NullValueHandling = NullValueHandling.Include,
        Formatting = Formatting.None,
        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
        ContractResolver = new LowercaseContractResolver()
    };

    public static void CopyTo(Stream src, Stream dest)
    {
        byte[] bytes = new byte[4096];

        int cnt;

        while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
        {
            dest.Write(bytes, 0, cnt);
        }
    }

    public static byte[] ZipObject(object model)
    {
        string str = JsonConvert.SerializeObject(model, Settings);

        return Zip(str);
    }

    public static string ZipObjectToBase64String(object model)
    {
        string str = JsonConvert.SerializeObject(model, Settings);

        var bytes = Zip(str);

        return Convert.ToBase64String(bytes);
    }

    public static byte[] Zip(string str)
    {
        var bytes = Encoding.UTF8.GetBytes(str);

        using var msi = new MemoryStream(bytes);
        using var mso = new MemoryStream();
        using (var gs = new GZipStream(mso, CompressionMode.Compress))
        {
            //msi.CopyTo(gs);
            CopyTo(msi, gs);
        }

        return mso.ToArray();
    }

    public static T UnzipObject<T>(byte[] bytes)
    {
        string str = Unzip(bytes);

        return JsonConvert.DeserializeObject<T>(str);
    }

    public static string Unzip(byte[] bytes)
    {
        using var msi = new MemoryStream(bytes);
        using var mso = new MemoryStream();
        using (var gs = new GZipStream(msi, CompressionMode.Decompress))
        {
            //gs.CopyTo(mso);
            CopyTo(gs, mso);
        }

        return Encoding.UTF8.GetString(mso.ToArray());
    }
}