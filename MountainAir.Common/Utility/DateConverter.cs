﻿using System;

namespace MountainAir.Common.Utility;

/// <summary>
/// 
/// </summary>
public static class DateConverter
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static string ConvertDate(DateTime? dt)
    {
        if (dt == null || !dt.HasValue)
            return null;

        return dt.Value.ToString("yyyy/MM/dd HH:mm:ss");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="str"></param>
    /// <returns></returns>
    public static DateTime? ConvertString(string str)
    {
        if (string.IsNullOrEmpty(str))
            return null;

        DateTime retval = DateTime.MinValue;

        if (!DateTime.TryParse(str, out retval))
            return null;

        return retval;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="unixTimeStamp"></param>
    /// <returns></returns>
    public static DateTime UnixTimeStampToDateTime(double unixTimeStamp)
    {
        // Unix timestamp is seconds past epoch
        System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
        dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
        return dtDateTime;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="dateTime"></param>
    /// <returns></returns>
    public static double DateTimeToUnixTimestamp(DateTime? dateTime)
    {
        if (dateTime != null && dateTime.HasValue)
            return (TimeZoneInfo.ConvertTimeToUtc(dateTime.Value) - new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds;
        else
            return 0;
    }
}