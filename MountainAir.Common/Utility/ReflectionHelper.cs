﻿using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Newtonsoft.Json.Serialization;

namespace MountainAir.Common.Utility;

public static class ReflectionHelper
{
    public static string GetKeyColumnName<T>()
    {
        var t = typeof(T);
        var props = t.GetProperties().Where(i => i.GetCustomAttribute<KeyAttribute>() != null).ToList();
        var prop = props.FirstOrDefault();
        if (prop != null)
            return GetSerializationName<T>(prop);
        return null;
    }

    private static string GetColumnName<T>(Expression<Func<T, object>> expression)
    {
        var body = expression.Body;

        if (body is MemberExpression)
        {
            return ((MemberExpression)body).Member.Name;
        }
        else if (body is UnaryExpression)
        {
            var b = (MemberExpression)((UnaryExpression)body).Operand;
            return b.Member.Name;
        }
        else
            throw new NotImplementedException($"Invalid type: {body.GetType().Name}");
    }

    public static T GetAttribute<T>(Type type, string propertyName)
        where T : Attribute
    {
        var propertyInfo = type.GetProperty(propertyName);

        if (propertyInfo == null)
            throw new ArgumentOutOfRangeException(nameof(propertyName), string.Format("{0} is not a property of {1}.", (object)propertyName, (object)type.Name));

        var attr = propertyInfo.GetCustomAttribute<T>();

        return attr;
    }

    public static string GetDisplayValue(Type type, string propertyName)
    {
        var attr = GetAttribute<DisplayAttribute>(type, propertyName);

        if (attr != null)
            return attr.Name;
        else
            return propertyName;
    }

    public static string GetDisplayValue<T>(string propertyName)
    {
        return GetDisplayValue(typeof(T), propertyName);
    }

    public static string GetDisplayValue<T>(PropertyInfo propertyInfo)
    {
        return GetDisplayValue<T>(propertyInfo.Name);
    }

    public static string GetDisplayValue<T>(Expression<Func<T, object>> expression)
    {
        string propertyName = GetColumnName<T>(expression);

        return GetDisplayValue<T>(propertyName);
    }

    public static string GetSerializationName<T>(PropertyInfo propertyInfo)
    {
        return GetSerializationName<T>(propertyInfo.Name);
    }

    public static string GetSerializationName<T>(string propertyName)
    {
        return GetJsonProperty<T>(propertyName);

        //var attr = GetAttribute<JsonPropertyAttribute>(typeof(T), propertyName);

        //if (attr != null)
        //    return attr.PropertyName;
        //else
        //    return propertyName;
    }

    public static string GetSerializationName(Type type, string propertyName)
    {
        //var attr = GetAttribute<JsonPropertyAttribute>(type, propertyName);

        //if (attr != null)
        //    return attr.PropertyName;
        //else
        //    return propertyName;
            
        return GetJsonProperty(type, propertyName);
    }

    public static string GetSerializationName<T>(Expression<Func<T, object>> expression)
    {
        string propertyName = GetColumnName<T>(expression);

        return GetSerializationName<T>(propertyName);
    }

    private static readonly IContractResolver DefaultResolver = new CamelCasePropertyNamesContractResolver(); //JsonSerializer.CreateDefault().ContractResolver;

    public static string GetJsonProperty(Type type, string jsonName, bool exact = false, IContractResolver resolver = null)
    {
        resolver = resolver ?? DefaultResolver;
            
        var contract = resolver.ResolveContract(type) as JsonObjectContract;
            
        if (contract == null)
            throw new ArgumentException($"{type.Name} is not serialized as a JSON object");
            
        var property = contract.Properties.GetProperty(jsonName, exact ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase);
            
        if (property == null)
            throw new ArgumentException($"Property {jsonName} was not found.");
            
        return property.PropertyName; //.ValueProvider.GetValue(obj); // Maybe check JsonProperty.Readable first, and throw an exception if not?
    }

    public static string GetJsonProperty<T>(string jsonName, bool exact = false, IContractResolver resolver = null)
    {
        resolver = resolver ?? DefaultResolver;
            
        var contract = resolver.ResolveContract(typeof(T)) as JsonObjectContract;
            
        if (contract == null)
            throw new ArgumentException($"{typeof(T).Name} is not serialized as a JSON object");
            
        var property = contract.Properties.GetProperty(jsonName, exact ? StringComparison.Ordinal : StringComparison.OrdinalIgnoreCase);
            
        if (property == null)
            throw new ArgumentException($"Property {jsonName} was not found.");
            
        return property.PropertyName; //.ValueProvider.GetValue(obj); // Maybe check JsonProperty.Readable first, and throw an exception if not?
    }
}