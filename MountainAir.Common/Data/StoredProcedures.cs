﻿using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace MountainAir.Common.Data;

public partial class ApplicationDbContext
{
    public async Task<List<MaterialReport>> SpGetMaterialList(Guid optionItemId)
    {
        var conn = this.Database.GetDbConnection();

        if (conn.State != ConnectionState.Open)
            await conn.OpenAsync();

        var cmd = conn.CreateCommand();
        cmd.CommandText = $"dbo.spGetMaterialList";
        cmd.CommandType = CommandType.StoredProcedure;

        var param = cmd.CreateParameter();
        param.DbType = DbType.Guid;
        param.ParameterName = "@OptionItemId";
        param.Value = optionItemId;
        cmd.Parameters.Add(param);

        await using var reader = await cmd.ExecuteReaderAsync();
        var items = reader.MapToList<MaterialReport>();

        return items;
    }
    
    public async Task<int> DeleteEstimateOptionAsync(Guid estimateOptionId)
    {
        return await Database.ExecuteSqlRawAsync("spDeleteEstimateOption @p0", new[] { $"{estimateOptionId}" });
    }

    public async Task<int> DeleteOptionItemAsync(Guid optionItemId)
    {
        return await Database.ExecuteSqlRawAsync("spDeleteOptionItem @p0", new[] { $"{optionItemId}" });
    }
}