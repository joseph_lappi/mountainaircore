﻿using MountainAir.Common.Extensions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Interfaces;

public interface IUserRecord
{
    string FirstName { get; set; }
    string LastName { get; set; }

    [Display(Name = "Display Name")]
    [NotMapped]
    string DisplayName { get; }

    [Display(Name = "Full Name")]
    [NotMapped]
    string FullName { get; }
}