﻿//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Data.Common;
//using System.Data.SqlClient;
//using System.Threading;
//using System.Threading.Tasks;
//using Microsoft.EntityFrameworkCore;
//using MountainAir.Common.Extensions;
//using Newtonsoft.Json;

//namespace MountainAir.Common.Data;

//public abstract class DataContext : DbContext
//{
//    private const int C_CommandTimeout = 300;
//    static JsonSerializerSettings settings = new JsonSerializerSettings
//    {
//        NullValueHandling = NullValueHandling.Ignore
//    };

//    protected DataContext(DbContextOptions options)
//        : base(options)
//    {
//    }

//    protected static DbContextOptions<T> CreateOptions<T>(string connectionString)
//        where T: DbContext
//    {
//        var optionsBuilder = new DbContextOptionsBuilder<T>()
//            .UseSqlServer(connectionString);

//        return optionsBuilder.Options;
//    }

//    protected string SetObject(object v)
//    {
//        if (v == null)
//            return null;
//        return JsonConvert.SerializeObject(v, settings);
//    }

//    protected T GetOrCreateObject<T>(string v)
//    {
//        if (string.IsNullOrEmpty(v))
//            return Activator.CreateInstance<T>();
//        return JsonConvert.DeserializeObject<T>(v);
//    }

//    protected List<T> ExecuteReader<T>(string sql)
//        where T : new()
//    {
//        DbCommand cmd = this.Database.GetDbConnection().CreateCommand();

//        cmd.CommandText = sql;
//        cmd.CommandType = CommandType.Text;
//        cmd.CommandTimeout = C_CommandTimeout;

//        if (cmd.Connection.State == ConnectionState.Closed)
//        {
//            cmd.Connection.Open();
//        }

//        using (var reader = cmd.ExecuteReader())
//        {
//            return reader.MapToList<T>();
//        }
//    }

//    protected async Task<List<T>> ExecuteReaderAsync<T>(string sql, CancellationToken cancellationToken = default)
//        where T : new()
//    {
//        DbCommand cmd = this.Database.GetDbConnection().CreateCommand();

//        cmd.CommandText = sql;
//        cmd.CommandType = CommandType.Text;
//        cmd.CommandTimeout = C_CommandTimeout;

//        if (cmd.Connection.State == ConnectionState.Closed)
//        {
//            cmd.Connection.Open();
//        }

//        using (var reader = await cmd.ExecuteReaderAsync(cancellationToken))
//        {
//            return reader.MapToList<T>();
//        }
//    }

//    protected async Task<List<T>> ExecuteProcedureAsync<T>(string procedureName, IEnumerable<SqlParameter> parameters, CancellationToken cancellationToken = default)
//        where T : new()
//    {
//        DbCommand cmd = this.Database.GetDbConnection().CreateCommand();

//        cmd.CommandText = procedureName;
//        cmd.CommandType = CommandType.StoredProcedure;
//        cmd.CommandTimeout = C_CommandTimeout;

//        if (parameters != null)
//        {
//            foreach (var parm in parameters)
//            {
//                cmd.Parameters.Add(parm);
//            }
//        }

//        if (cmd.Connection.State == ConnectionState.Closed)
//        {
//            cmd.Connection.Open();
//        }

//        using (var reader = await cmd.ExecuteReaderAsync(cancellationToken))
//        {
//            return reader.MapToList<T>();
//        }
//    }

//    protected int ExecuteNonQuery(string sql, IEnumerable<SqlParameter> parameters)
//    {
//        DbCommand cmd = this.Database.GetDbConnection().CreateCommand();

//        cmd.CommandText = sql;
//        cmd.CommandType = CommandType.Text;
//        cmd.CommandTimeout = C_CommandTimeout;

//        if (parameters != null)
//        {
//            foreach (var parm in parameters)
//            {
//                cmd.Parameters.Add(parm);
//            }
//        }

//        if (cmd.Connection.State == ConnectionState.Closed)
//        {
//            cmd.Connection.Open();
//        }

//        return cmd.ExecuteNonQuery();
//    }

//    protected Task<int> ExecuteNonQueryAsync(string sql, IEnumerable<SqlParameter> parameters, CancellationToken cancellationToken = default)
//    {
//        DbCommand cmd = this.Database.GetDbConnection().CreateCommand();

//        cmd.CommandText = sql;
//        cmd.CommandType = CommandType.Text;
//        cmd.CommandTimeout = C_CommandTimeout;

//        if (parameters != null)
//        {
//            foreach (var parm in parameters)
//            {
//                cmd.Parameters.Add(parm);
//            }
//        }

//        if (cmd.Connection.State == ConnectionState.Closed)
//        {
//            cmd.Connection.Open();
//        }

//        return cmd.ExecuteNonQueryAsync(cancellationToken);
//    }
//}