﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Configuration;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using System;

namespace MountainAir.Common.Data;

public partial class ApplicationDbContext : IdentityDbContext<ApplicationUser, ApplicationRole, Guid>
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
        : base(options)
    {
    }

    public ApplicationDbContext()
        : this(AppSettings.Shared.DefaultConnectionString)
    {

    }
        
    public ApplicationDbContext(string connectionString)
        : this(CreateOptions<ApplicationDbContext>(connectionString))
    {

    }

    protected static DbContextOptions<T> CreateOptions<T>(string connectionString)
        where T: DbContext
    {
        var optionsBuilder = new DbContextOptionsBuilder<T>()
            .UseSqlServer(connectionString);

        return optionsBuilder.Options;
    }

    public DbSet<Address> Addresses { get; set; }
    public DbSet<CatalogItem> CatalogItems { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Customer> Customers { get; set; }
    public DbSet<Door> Doors { get; set; }
    public DbSet<Estimate> Estimates { get; set; }
    public DbSet<Window> Windows { get; set; }
    public DbSet<Feature> Features { get; set; }
    public DbSet<Financing> Financings { get; set; }
    public DbSet<LogAttempt> LogAttempts { get; set; }
    public DbSet<Photo> Photos { get; set; }
    public DbSet<CatalogItemTxn> CatalogItemTxns { get; set; }
    public DbSet<EstimateOption> EstimateOptions { get; set; }
    public DbSet<OptionItemMaterial> OptionItemMaterials { get; set; }
    public DbSet<OptionItem> OptionItems { get; set; }
    public DbSet<Room> Rooms { get; set; }
    public DbSet<PaymentMethod> PaymentMethods { get; set; }
    public DbSet<Package> Packages { get; set; }
    public DbSet<PackageItem> PackageItems { get; set; }
    public DbSet<PackageMaterial> PackageMaterials { get; set; }
    public DbSet<AzureBlobContainer> AzureBlobContainers { get; set; }
    public DbSet<AzureContainerFile> AzureContainerFiles { get; set; }

    public DbSet<vCatalogItem> CatalogItemList { get; set; }
    public DbSet<vEstimateList> EstimateList { get; set; }
    public DbSet<vEstimateEquipmentReport> EstimateEquipmentReport { get; set; }
    public DbSet<Warranty> Warranties { get; set; }
    public DbSet<WarrantyType> WarrantyTypes { get; set; }
    public DbSet<EmailLog> EmailLogs { get; set; }
    public DbSet<Location> Locations { get; set; }

    public DbSet<Campaign> Campaigns { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
            
        modelBuilder.ApplyConfiguration(new CatalogItem.Configuration());
        modelBuilder.ApplyConfiguration(new AzureContainerFile.Configuration());
        modelBuilder.ApplyConfiguration(new vEstimateEquipmentReport.Configuration());
            
        modelBuilder.Entity<vCatalogItem>().HasNoKey();
        modelBuilder.Entity<vEstimateList>().HasNoKey();

        modelBuilder.Entity<Package>()
            .HasIndex(c => c.PackageName)
            .IsUnique(true);

        modelBuilder.Entity<Package>()
            .HasMany(p => p.PackageItems)
            .WithOne(pi => pi.Package)
            .IsRequired();

        modelBuilder.Entity<PackageItem>()
            .HasMany(pi => pi.PackageMaterials)
            .WithOne(pim => pim.PackageItem)
            .IsRequired();
    }
}