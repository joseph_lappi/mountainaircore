﻿//using Microsoft.EntityFrameworkCore;
//using MountainAir.Common.Configuration;
//using MountainAir.Common.Data.Models;
//using System;
//using System.Threading.Tasks;

//namespace MountainAir.Common.Data;

//public partial class MountainAirContext : DataContext
//{
//    //public static readonly LoggerFactory MyLoggerFactory = new LoggerFactory(new[]
//    //{
//    //    new DebugLoggerProvider()
//    //});

//    public MountainAirContext()
//        : this(AppSettings.Shared.DefaultConnectionString)
//    {

//    }
        
//    public MountainAirContext(string connectionString)
//        : base(CreateOptions<MountainAirContext>(connectionString))
//    {

//    }

//    public MountainAirContext(DbContextOptions<MountainAirContext> options) 
//        : base(options)
//    {
//    }
        
//    public DbSet<Employee> Users { get; set; }
//    public DbSet<Address> Addresses { get; set; }
//    public DbSet<CatalogItem> CatalogItems { get; set; }
//    public DbSet<Category> Categories { get; set; }
//    public DbSet<Customer> Customers { get; set; }
//    public DbSet<Door> Doors { get; set; }
//    public DbSet<Estimate> Estimates { get; set; }
//    public DbSet<Window> Windows { get; set; }
//    public DbSet<Feature> Features { get; set; }
//    public DbSet<Financing> Financings { get; set; }
//    public DbSet<LogAttempt> LogAttempts { get; set; }
//    public DbSet<Photo> Photos { get; set; }
//    public DbSet<CatalogItemTxn> CatalogItemTxns { get; set; }
//    public DbSet<EstimateOption> EstimateOptions { get; set; }
//    public DbSet<OptionItemMaterial> OptionItemMaterials { get; set; }
//    public DbSet<OptionItem> OptionItems { get; set; }
//    public DbSet<Room> Rooms { get; set; }
//    public DbSet<PaymentMethod> PaymentMethods { get; set; }
//    public DbSet<Package> Packages { get; set; }
//    public DbSet<PackageItem> PackageItems { get; set; }
//    public DbSet<PackageMaterial> PackageMaterials { get; set; }
//    public DbSet<AzureBlobContainer> AzureBlobContainers { get; set; }
//    public DbSet<AzureContainerFile> AzureContainerFiles { get; set; }

//    public DbSet<vCatalogItem> CatalogItemList { get; set; }
//    public DbSet<vEstimateList> EstimateList { get; set; }
//    public DbSet<vEstimateEquipmentReport> EstimateEquipmentReport { get; set; }
//    public DbSet<Warranty> Warranties { get; set; }

//    protected override void OnModelCreating(ModelBuilder modelBuilder)
//    {
//        base.OnModelCreating(modelBuilder);
            
//        modelBuilder.ApplyConfiguration(new AzureContainerFile.Configuration());
//        modelBuilder.ApplyConfiguration(new vEstimateEquipmentReport.Configuration());
            
//        modelBuilder.Entity<vCatalogItem>().HasNoKey();
//        modelBuilder.Entity<vEstimateList>().HasNoKey();
            
//        modelBuilder.Entity<AzureContainerFile>()
//            .HasKey(c => new { c.Name, c.ContainerId });

//        modelBuilder.Entity<Package>()
//            .HasIndex(c => c.PackageName)
//            .IsUnique(true);

//        modelBuilder.Entity<Package>()
//            .HasMany(p => p.PackageItems)
//            .WithOne(pi => pi.Package)
//            .IsRequired();

//        modelBuilder.Entity<PackageItem>()
//            .HasMany(pi => pi.PackageMaterials)
//            .WithOne(pim => pim.PackageItem)
//            .IsRequired();
//    }

//    public async Task<int> DeleteEstimateOptionAsync(Guid estimateOptionId)
//    {
//        return await Database.ExecuteSqlRawAsync("spDeleteEstimateOption @p0", new[] { $"{estimateOptionId}" });
//    }

//    public async Task<int> DeleteOptionItemAsync(Guid optionItemId)
//    {
//        return await Database.ExecuteSqlRawAsync("spDeleteOptionItem @p0", new[] { $"{optionItemId}" });
//    }
//}