﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("PaymentMethods")]
public class PaymentMethod
{
    [Key]
    public int PaymentId { get; set; }

    [Required]
    public string Method { get; set; }
}