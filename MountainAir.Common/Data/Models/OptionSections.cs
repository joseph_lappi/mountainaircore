﻿// josephlappi - 10/20/2018joseph.lappi@gmail.com
// 10/20/2018 - 4:10 PM
using System.ComponentModel.DataAnnotations;

namespace MountainAir.Common.Data.Models;

public enum OptionSections
{
    [Display(Name = "Heating")]
    Heating = 0,
    [Display(Name = "Cooling")]
    Cooling = 1,
    [Display(Name = "No Markup")]
    NoMarkup = 2,
    [Display(Name = "Incentives")]
    Incentives = 3
}