﻿// josephlappi - 10/20/2018joseph.lappi@gmail.com
// 10/20/2018 - 4:10 PM
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("Packages")]
public class Package
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int PackageId { get; set; }

    [Required]
    [StringLength(100)]
    [Display(Name = "Package Name")]
    public string PackageName { get; set; }

    public DateTime AddDate { get; set; }

    public Guid AddBy { get; set; }

    [Display(Name = "Expriation Date")]
    public DateTime? ExpirationDate { get; set; }

    public Guid? UpdatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public bool Active { get; set; }
        
    [Display(Name = "AHRI Rating")]
    public string AhriRating { get; set; }

    [Display(Name = "Profit Margin")]
    public decimal? ProfitMargin { get; set; }

    public List<PackageItem> PackageItems { get; set; } = new List<PackageItem>();
}