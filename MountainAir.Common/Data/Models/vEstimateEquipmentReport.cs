﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MountainAir.Common.Security;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table(ViewName)]
public class vEstimateEquipmentReport
{
    public const string ViewName = "vEstimateEquipmentReport"; 

    public class Configuration : IEntityTypeConfiguration<vEstimateEquipmentReport>
    {
        public void Configure(EntityTypeBuilder<vEstimateEquipmentReport> builder)
        {
            builder.HasNoKey();
        }
    }

    public Guid EstimateId { get; set; }

    public string EstimateNumber { get; set; }

    public DateTime CreatedDate { get; set; }

    public Guid CreatedById { get; set; }

    [ForeignKey(nameof(CreatedById))]
    public ApplicationUser CreatedBy { get; set; }

    public Guid AssignedToId { get; set; }

    [ForeignKey(nameof(AssignedToId))]
    public ApplicationUser AssignedTo { get; set; }
        
    public Guid? UpdatedById { get; set; }

    [ForeignKey(nameof(UpdatedById))]
    public ApplicationUser UpdatedBy { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public Guid CustomerId { get; set; }

    [ForeignKey(nameof(CustomerId))]
    public Customer Customer { get; set; }

    public bool Active { get; set; }

    public bool ReadyToProcess { get; set; }

    public decimal? DownPayment { get; set; }

    public string CatalogNum { get; set; }

    public string ModelNum { get; set; }

    public string Description { get; set; }

    public string CustomerDescription { get; set; }

    public decimal ItemPrice { get; set; }

    public decimal? ItemLabor { get; set; }

    public int Quantity { get; set; }

    public decimal? TotalLabor { get; set; }

    public decimal? TotalPrice { get; set; }

    public decimal? MaterialLabor { get; set; }

    public decimal? MaterialTotal { get; set; }
}