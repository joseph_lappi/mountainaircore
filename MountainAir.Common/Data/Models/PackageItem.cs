﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MountainAir.Common.Data.Models;

[Table("PackageItems")]
public class PackageItem
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int PackageItemId { get; set; }

    public int PackageId { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    [ForeignKey(nameof(PackageId))]
    public Package Package { get; set; }

    [Display(Name = "Catalog Item")]
    public int CatalogItemId { get; set; }

    [ForeignKey(nameof(CatalogItemId))]
    public CatalogItem CatalogItem { get; set; }

    public int Quantity { get; set; }

    [JsonIgnore, System.Text.Json.Serialization.JsonIgnore]
    public decimal? Price { get; set; }
        
    //[JsonIgnore]
    //[System.Text.Json.Serialization.JsonIgnore]
    public decimal? AdditionalPrice { get; set; }

    ////Added this way to minimize changes to the app. 
    [JsonProperty("price")]
    [System.Text.Json.Serialization.JsonPropertyName("price")]
    public decimal CalculatedPrice 
    {
        get
        {
            if(this.Price.HasValue)
            {
                decimal price = this.Price.GetValueOrDefault(0);

                if(AdditionalPrice.HasValue)
                {
                    price += AdditionalPrice.GetValueOrDefault(0);
                }

                return price;
            }
            return 0;
        }
    }

    public int? Labor { get; set; }

    public int Section { get; set; }

    [NotMapped]
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public OptionSections OptionSection => (OptionSections)Section;

    public List<PackageMaterial> PackageMaterials { get; set; } = new List<PackageMaterial>();
}