﻿using System.ComponentModel.DataAnnotations;

namespace MountainAir.Common.Data.Models;

public class MaterialReport
{
    [Key]
    public int Id { get; set; }

    public int CatalogItemId { get; set; }

    public string CategoryName { get; set; }

    public string ModelNum { get; set; }

    public decimal Price { get; set; }

    public int? Labor { get; set; }

    public int Quantity { get; set; }

    public int CategoryId { get; set; }

    public int? SortOrder { get; set; }

    public string CatalogNum { get; set; }

    public string KeyCode { get; set; }

    public string Descr { get; set; }
}