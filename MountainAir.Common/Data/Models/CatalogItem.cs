﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;

namespace MountainAir.Common.Data.Models;

[Table("CatalogItems")]
public class CatalogItem
{
    public class Configuration : IEntityTypeConfiguration<CatalogItem>
    { 
        public void Configure(EntityTypeBuilder<CatalogItem> builder)
        {
            builder.ToTable(tb =>
            {
                tb.HasTrigger("TU_CatalogItems");
                tb.HasTrigger("TD_CatalogItems");
            });
        }
    }

    [Key]
    public int CatalogItemId { get; set; }

    [Display(Name = "Catalog Number")]
    public string CatalogNum { get; set; }

    [Display(Name = "Model Number")]
    public string ModelNum { get; set; }

    [Display(Name = "Description")]
    public string Descr { get; set; }
        
    [Column(TypeName = "decimal(18,2)")]
    public decimal Price { get; set; }

    [Display(Name = "Category")]
    public int CategoryId { get; set; }

    public int? Labor { get; set; }

    [Display(Name = "Customer Description")]
    public string CustomerDesc { get; set; }

    public bool Active { get; set; } = true;

    [Display(Name = "KeyCode")]
    public string KeyCode { get; set; }

    [Display(Name = "Monthly Price")]
    [Column(TypeName = "decimal(18,2)")]
    public decimal? MonthlyPrice { get; set; }

    public string CategoryName => Category?.CategoryName ?? string.Empty;

    [ForeignKey(nameof(CategoryId))]
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public Category Category { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public List<CatalogItemTxn> Histories { get; set; }
}