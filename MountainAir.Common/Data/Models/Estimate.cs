﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using MountainAir.Common.Security;
using System.Net;

namespace MountainAir.Common.Data.Models;

[Table("Estimates")]
public class Estimate
{   
    [Key]
    public Guid EstimateId { get; set; }

    public Guid CustomerId { get; set; }

    [Display(Name = "First Name")]
    public string CustomerFirstName { get; set; }
    
    [Display(Name = "Last Name")]
    public string CustomerLastName { get; set; }
    
    [Display(Name = "Company")]
    public string CompanyName { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    [ForeignKey(nameof(CustomerId))]
    public Customer Customer { get; set; }

    public Guid? AddressId { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    [ForeignKey(nameof(AddressId))]
    [Display(Name = "Address")]
    public Address Address { get; set; }

    public Guid? BillingAddressId { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    [ForeignKey(nameof(BillingAddressId))]
    [Display(Name = "Billing Address")]
    public Address BillingAddress { get; set; }

    [StringLength(50)]
    public string EstimateNumber { get; set; }

    [Display(Name = "Assigned To")]
    public Guid AssignedTo { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    [ForeignKey(nameof(AssignedTo))]
    [Display(Name = "Assigned To")]
    public ApplicationUser Assigned { get; set; }

    [Display(Name = "Created Date")]
    public DateTime CreatedDate { get; set; }

    public Guid CreatedBy { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    [ForeignKey(nameof(CreatedBy))]
    public ApplicationUser Created { get; set; }

    [StringLength(2000)]
    public string EstimateNotes { get; set; }

    public Guid? UpdatedBy { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    [ForeignKey(nameof(UpdatedBy))]
    public ApplicationUser Updated { get; set; }

    [Display(Name = "Last Updated Date")]
    public DateTime? UpdatedDate { get; set; }

    [Display(Name = "Installation Date")]
    public DateTime? InstallationDate { get; set; }

    [Display(Name = "Approval Date")]
    public DateTime? ApprovalDate { get; set; }
        
    public decimal? SystemTotal { get; set; }

    [Display(Name = "Profit Margin Rate")]
    public decimal? ProfitMarginRate { get; set; }

    [Display(Name = "Sales Tax Rate")]
    public decimal? SalesTaxRate { get; set; }

    [StringLength(500)]
    public string CustomerApproval { get; set; }

    [StringLength(200)]
    [Display(Name = "Designer Name")]
    public string DesignerName { get; set; }

    [StringLength(50)]
    [Display(Name = "Designer Phone")]
    public string DesignerPhone { get; set; }

    public decimal? BtuGain { get; set; }
        
    public decimal? BtuLoss { get; set; }
        
    public int? SummerTempDiff { get; set; }
        
    public int? WinterTempDiff { get; set; }
        
    public decimal? WindowRValue { get; set; }
        
    public decimal? DoorRValue { get; set; }
        
    public decimal? CeilingHeight { get; set; }
        
    public decimal? AppliancesMultiplier { get; set; }
        
    public decimal? CeilingRValue { get; set; }
        
    public decimal? LaborRate { get; set; }
        
    public decimal? InfiltrationGWMultiplier { get; set; }
        
    public decimal? PeopleMultiplier { get; set; }
        
    public decimal? FloorRValue { get; set; }
        
    public decimal FinancingRate { get; set; }
        
    public int FinancingTerm { get; set; }
        
    public decimal? WallRValue { get; set; }
        
    public decimal? CoolingCFM { get; set; }
        
    public decimal? HeatingCFM { get; set; }
        
    public decimal? HeatingRegisters { get; set; }
        
    public decimal? CoolingRegisters { get; set; }
        
    public decimal? CoolingCFMDivisor { get; set; }
        
    public decimal? HeatingCFMDivisor { get; set; }
        
    public decimal? AirTonage { get; set; }
        
    public Guid? SelectedOptionId { get; set; }
        
    public decimal FinancingFactor { get; set; }
        
    public bool Active { get; set; }

    public string InstallAddressLine1 { get; set; }

    public string InstallAddressLine2 { get; set; }

    public string InstallCity { get; set; }

    public string InstallState { get; set; }

    public string InstallZip { get; set; }
    
    public string BillingAddressLine1 { get; set; }

    public string BillingAddressLine2 { get; set; }

    public string BillingCity { get; set; }

    public string BillingState { get; set; }

    public string BillingZip { get; set; }
    
    public string InstallAddressDisplayValue => $"{InstallAddressLine1}, {InstallAddressLine2}, {InstallCity}, {InstallState}, {InstallZip}";
    
    public string BillingAddressDisplayValue => $"{BillingAddressLine1}, {BillingAddressLine2}, {BillingCity}, {BillingState}, {BillingZip}";

    [StringLength(100)]
    [Display(Name = "Payment Method")]
    public string PaymentMethod { get; set; }
    public bool? ReadyToProcess { get; set; }
    [Display(Name = "Down Payment")]
    public decimal? DownPayment { get; set; }
    [Display(Name = "Financing")]
    public string FinancingString => $"{FinancingRate}% for {FinancingTerm} months";

    public List<EstimateOption> EstimateOptions { get; set; } = new();
    
    public List<Room> Rooms { get; set; } = new();
    
    public List<Warranty> Warranties { get; set; } = new();

    [NotMapped]
    public virtual List<BlobViewModel> Attachments { get; set; } = new List<BlobViewModel>();

    [NotMapped]
    public EstimateOption SelectedOption => EstimateOptions?.FirstOrDefault(i => i.EstimateOptionId == SelectedOptionId);
}