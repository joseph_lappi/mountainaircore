﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("Photos")]
public class Photo
{
    [Key]
    public Guid PhotoId { get; set; }

    public Guid EstimateId { get; set; }

    public DateTime CreatedDate { get; set; }

    [StringLength(10)]
    [Required]
    public string FileExtension { get; set; }

    [StringLength(2000)]
    [Required]
    public string PhotoUrl { get; set; }
}