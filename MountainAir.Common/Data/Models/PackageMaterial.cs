﻿// josephlappi - 10/20/2018joseph.lappi@gmail.com
// 10/20/2018 - 4:10 PM
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MountainAir.Common.Data.Models;

[Table("PackageMaterial")]
public class PackageMaterial
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int PackageMaterialId { get; set; }

    public int PackageItemId { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    [ForeignKey(nameof(PackageItemId))]
    public PackageItem PackageItem { get; set; }

    public int CatalogItemId { get; set; }

    [ForeignKey(nameof(CatalogItemId))]
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public CatalogItem CatalogItem { get; set; }

    public int Quantity { get; set; }

    public decimal? Price { get; set; }

    public int? Labor { get; set; }
}