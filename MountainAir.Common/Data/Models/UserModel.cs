﻿using MountainAir.Common.Data.Interfaces;
using MountainAir.Common.Extensions;
using MountainAir.Common.Security;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MountainAir.Common.Data.Models;

public class UserModel : IUserRecord
{
    public UserModel()
    {

    }

    public UserModel(ApplicationUser user, IEnumerable<string> roles)
        : this(user)
    {
        this.Roles = roles;
    }

    public UserModel(ApplicationUser user)
    {
        if (user == null)
            return;

        this.Active = user.Active;
        this.Email = user.Email;
        this.FirstName = user.FirstName;
        this.LastName = user.LastName;
        this.DefaultLaborRate = user.DefaultLaborRate;
        this.DefaultProfitMargin = user.DefaultProfitMargin;
        this.DefaultSalesTax = user.DefaultSalesTax;
        this.PhoneNumber = user.PhoneNumber;
        this.Id = user.Id;
        this.UserName = user.UserName;
    }

    public bool Active { get; set; }

    public decimal? DefaultLaborRate { get; set; }

    public decimal? DefaultProfitMargin { get; set; }

    public decimal? DefaultSalesTax { get; set; }

    public string Email { get; set; }

    public string FirstName { get; set; }

    public Guid Id { get; set; }

    public string NewPassword { get; set; }
    
    public string OldPassword { get; set; }

    public string LastName { get; set; }

    public string PhoneNumber { get; set; }

    public string UserName { get; set; }

    [Display(Name = "Display Name")]
    public string DisplayName => this.FormatDisplayName();

    [Display(Name = "Full Name")]
    public string FullName => this.FormatFullName();

    public IEnumerable<string> Roles { get; set; }
}