﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("WarrantyTypes")]
public class WarrantyType
{
    [Key]
    public Guid Id { get; set; }

    public string DisplayText { get; set; }

    public string TermText { get; set; }

    public int Value { get; set; }

    public int ServiceFee { get; set; }

    public int? TermLength { get; set; }
}