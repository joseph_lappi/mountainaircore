﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("Features")]
public class Feature
{
    [Key]
    public int FeatureId { get; set; }

    [Display(Name = "Feature Text")]
    public string FeatureText { get; set; }

    public decimal? Cost { get; set; }

    public int? Labor { get; set; }

    [Display(Name = "Sort Order")]
    public int? SortOrder { get; set; }
}