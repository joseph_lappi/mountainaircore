﻿using MountainAir.Common.Data.Interfaces;
using MountainAir.Common.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("Customers")]
public class Customer : IUserRecord
{
    [Key]
    [Display(Name = "Customer")]
    public Guid CustomerId { get; set; }

    [Display(Name = "Last Name")]
    public string LastName { get; set; }

    [Display(Name = "First Name")]
    public string FirstName { get; set; }

    [Display(Name = "Company")]
    public string CompanyName { get; set; }

    [Display(Name = "Phone Number 1")]
    public string PhoneNumber1 { get; set; }

    [Display(Name = "Phone Number 2")]
    public string PhoneNumber2 { get; set; }
        
    public string Email { get; set; }

    public bool? TestData { get; set; }

    public List<Address> Addresses { get; set; } = new();

    [Newtonsoft.Json.JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public List<Estimate> Estimates { get; set; } = new();

    [Display(Name = "Display Name")]
    [NotMapped]
    public string DisplayName => this.FormatDisplayName();

    [Display(Name = "Full Name")]
    [NotMapped]
    public string FullName => this.FormatFullName();
}