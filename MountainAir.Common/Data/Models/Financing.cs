﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("Financing")]
public class Financing
{
    [Key]
    public int FinancingId { get; set; }
    
    [Column(TypeName = "decimal(18,2)")]
    public decimal Rate { get; set; }

    public int Term { get; set; }

    [Display(Name = "Payment Factor")]
    [Column(TypeName = "decimal(18,6)")]
    public decimal PaymentFactor { get; set; }
}