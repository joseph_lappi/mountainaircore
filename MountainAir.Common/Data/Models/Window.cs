﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MountainAir.Common.Data.Models;

[Table("Windows")]
public class Window
{
    [Key]
    public Guid WindowId { get; set; }

    public Guid RoomId { get; set; }

    public decimal? BTULoss { get; set; }

    public decimal? BTUGain { get; set; }

    public bool? Fixed { get; set; }

    public decimal? Width { get; set; }

    public decimal? Height { get; set; }

    public int? NumberOfWindows { get; set; }
        
    public string Direction { get; set; }

    public string WindowType { get; set; }

    public decimal? Infiltration { get; set; }

    public decimal? FixedPerimeter { get; set; }

    public decimal? SlidingPerimeter { get; set; }

    [JsonIgnore]
    [ForeignKey(nameof(RoomId))]
    [System.Text.Json.Serialization.JsonIgnore]
    public Room Room { get; set; }
}