﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("EmailLogs")]
public class EmailLog
{
	[Key]
	public Guid Id { get; set; }

	public DateTimeOffset? SendDate { get; set; }

	public string SendTo { get; set; }

	public string SendCc { get; set; }

	public string SendBcc { get; set; }

	public string Subject { get; set; }

	public string Message { get; set; }

	public string Sender { get; set; }

	public int? AttachmentCount { get; set; }
}