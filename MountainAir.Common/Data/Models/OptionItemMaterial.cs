﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("OptionItemMaterial")]
public class OptionItemMaterial
{
    [Key]
    public Guid OptionItemMaterialId { get; set; }

    public Guid OptionItemId { get; set; }

    public int CategoryId { get; set; }

    public int CatalogItemId { get; set; }

    public int? Labor { get; set; }

    [StringLength(200)]
    public string CatalogNum { get; set; }

    public decimal? Price { get; set; }

    public int Quantity { get; set; }

    [StringLength(250)]
    public string CustomerDesc { get; set; }

    [StringLength(200)]
    public string ModelNum { get; set; }

    [StringLength(200)]
    public string Descr { get; set; }

    [StringLength(50)]
    public string KeyCode { get; set; }
        
    [ForeignKey(nameof(OptionItemId))]
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public virtual OptionItem OptionItem { get; set; }
    
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    [ForeignKey(nameof(CategoryId))]
    public Category Category { get; set; }

    public string CategoryName => Category?.CategoryName;
    
    public string Location { get; set; }
}