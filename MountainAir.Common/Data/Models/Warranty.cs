﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("Warranties")]
public class Warranty
{
	[Key]
	public Guid Id { get; set; }

	public Guid EstimateId { get; set; }

	public Guid WarrantyTypeId { get; set; }

	[ForeignKey(nameof(WarrantyTypeId))]
	public WarrantyType WarrantyType { get; set; }

	[JsonIgnore]
	[ForeignKey(nameof(EstimateId))]
	public Estimate Estimate { get; set; }

	[NotMapped]
	public string CustomerName => $"{Estimate?.CustomerFirstName} {Estimate?.CustomerLastName}";

	[NotMapped]
	public string AddressLine1 => Estimate?.InstallAddressLine1;

	[NotMapped]
	public string AddressLine2 => Estimate?.InstallAddressLine2;

	[NotMapped]
	public string City => Estimate?.InstallCity;

	[NotMapped]
	public string State => Estimate?.InstallState;

	[NotMapped]
	public string Zip => Estimate?.InstallZip;

	public DateTime? EffectiveDate { get; set; }

	public DateTime? InstallDate { get; set; }

    public DateTime? ExpirationDate { get; set; }

	public DateTime? CustomerSignedDate { get; set; }

	public DateTime AddDate { get; set; }

	public string AddedBy { get; set; }

	public DateTime? UpdateDate { get; set; }

	public string UpdateBy { get; set; }

	public int Value { get; set; }

	public string DisplayText { get; set; }

	public string TermText { get; set; }

	public int ServiceFee { get; set; }
}