﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("CatalogItemTxn")]
public class CatalogItemTxn
{
    [Key]
    public int Id { get; set; }
        
    [Required]
    public string LogType { get; set; }
        
    public int CatalogItemId { get; set; }
        
    public string CatalogNum { get; set; }
        
    public string ModelNum { get; set; }
        
    public string Descr { get; set; }
        
    [Column(TypeName = "decimal(18,2)")]
    public decimal Price { get; set; }
        
    public int CategoryId { get; set; }
        
    public int? Labor { get; set; }
        
    public string CustomerDesc { get; set; }
        
    public bool Active { get; set; }
        
    public DateTime TimeStamp { get; set; }
        
    public string KeyCode { get; set; }

    [Display(Name = "MonthlyPrice")]
    [Column(TypeName = "decimal(18,2)")]
    public decimal? MonthlyPrice { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    [ForeignKey("CatalogItemId")]
    public virtual CatalogItem CatalogItem { get; set; }
}