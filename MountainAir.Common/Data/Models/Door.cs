﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MountainAir.Common.Data.Models;

[Table("Doors")]
public class Door
{
    [Key]
    public Guid DoorId { get; set; }
        
    public Guid RoomId { get; set; }
        
    [Column(TypeName = "decimal(18,2)")]
    public decimal? Width { get; set; }
        
    [Column(TypeName = "decimal(18,2)")]
    public decimal? Height { get; set; }
        
    [Column(TypeName = "decimal(18,4)")]
    public decimal? BTUGain { get; set; }
        
    public int? NumberOfDoors { get; set; }
        
    [Column(TypeName = "decimal(18,4)")]
    public decimal? BTULoss { get; set; }
        
    public int? RValue { get; set; }
        
    [Column(TypeName = "decimal(18,4)")]
    public decimal? Infiltration { get; set; }
        
    [Column(TypeName = "decimal(18,4)")]
    public decimal? Perimeter { get; set; }

    [JsonIgnore]
    [ForeignKey(nameof(RoomId))]
    [System.Text.Json.Serialization.JsonIgnore]
    public Room Room { get; set; }
}