﻿using MountainAir.Common.Data.Interfaces;
using MountainAir.Common.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("vEstimateList")]
public class vEstimateList : IUserRecord
{
    public Guid EstimateId { get; set; }

    [Display(Name = "Estimate Number")]
    public string EstimateNumber { get; set; }

    [Display(Name = "Created On")]
    public DateTime CreatedDate { get; set; }

    [Display(Name = "Created By")]
    public Guid CreatedBy { get; set; }

    [Display(Name = "Updated On")]
    public DateTime? UpdatedDate { get; set; }

    [Display(Name = "Updated By")]
    public Guid? UpdatedBy { get; set; }

    [Display(Name = "Assigned To")]
    public Guid AssignedTo { get; set; }

    [Display(Name = "Customer")]
    public Guid CustomerId { get; set; }

    [Display(Name = "First Name")]
    public string CustomerFirstName { get; set; }

    [Display(Name = "Last Name")]
    public string CustomerLastName { get; set; }

    [Display(Name = "Company")]
    public string CompanyName { get; set; }

    [Display(Name = "Assigned First Name")]
    public string AssignedFirstName { get; set; }

    [Display(Name = "Assigned Last Name")]
    public string AssignedLastName { get; set; }

    [Display(Name = "Created Last Name")]
    public string CreatedByLastName { get; set; }

    [Display(Name = "Created First Name")]
    public string CreatedByFirstName { get; set; }

    [ReadOnly(true)]
    public bool Active { get; set; }

    [Display(Name = "Ready to Process")]
    public bool? ReadyToProcess { get; set; }

    [Display(Name = "Down Payment")]
    public decimal? DownPayment { get; set; }

    [Display(Name = "Test Data")]
    public bool? TestData { get; set; }

    [Display(Name = "Created By")]
    [NotMapped]
    [JsonProperty("createdByFullName")]
    public string CreatedByFullName { get => this.FormatFullName(CreatedByFirstName, CreatedByLastName); }

    [Display(Name = "Customer")]
    [NotMapped]
    [JsonProperty("customerFullName")]
    public string CustomerFullName { get => this.FormatFullName(CustomerFirstName, CustomerLastName); }

    [Display(Name = "Assigned To")]
    [NotMapped]
    [JsonProperty("assignedFullName")]
    public string AssignedFullName { get => this.FormatFullName(AssignedFirstName, AssignedLastName); }

    public Guid? SelectedOptionId { get; set; }

    [NotMapped]
    public List<BlobViewModel> Attachments { get; set; } = new List<BlobViewModel>();

    [NotMapped]
    [JsonProperty("validationNotes")]
    [Display(Name = "Validation Notes")]
    public string ValidationNotes { get; set; }

    [NotMapped]
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public string FirstName { get; set; }

    [NotMapped]
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public string LastName { get; set; }

    public string DisplayName { get; }

    public string FullName { get; }
}