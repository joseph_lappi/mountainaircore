﻿using System;

namespace MountainAir.Common.Data.Models;

public class BlobViewModel
{
    public Guid EstimateId { get; set; }

    public string Name { get; set; }

    public string Url { get; set; }

    public string FormattedName => Name?.Replace(EstimateId.ToString(), "").Replace("/", "");
}