﻿namespace MountainAir.Common.Data.Models;

public class CatalogItemImport : CatalogItem
{
    public string CategoryName { get; set; }
}