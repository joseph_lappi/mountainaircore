﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace MountainAir.Common.Data.Models;

[Table("AzureBlobContainers")]
public class AzureBlobContainer
{
    [Key]
    public string Id { get; set; }

    public string Url { get; set; }

    public List<AzureContainerFile> Files { get; set; } = new List<AzureContainerFile>();

    public bool Completed { get; set; }
}

[Table("AzureContainerFiles")]
public class AzureContainerFile
{
    public class Configuration : IEntityTypeConfiguration<AzureContainerFile>
    {
        public void Configure(EntityTypeBuilder<AzureContainerFile> builder)
        {
            builder.HasKey(k => new
            {
                k.Name,
                k.ContainerName
            });
        }
    }

    public string Name { get; set; }
        
    public string ContainerName { get; set; }

    [JsonIgnore]
    [ForeignKey(nameof(ContainerName))]
    public AzureBlobContainer Container { get; set; }

    public string Url { get; set; }
        
    public string FilePath { get; set; }

    public bool Processed { get; set; }
}