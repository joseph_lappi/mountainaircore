﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace MountainAir.Common.Data.Models;

[Table("EstimateOptions")]
public class EstimateOption
{
    [Key]
    public Guid EstimateOptionId { get; set; }
        
    public Guid EstimateId { get; set; }
        
    public int OptionNumber { get; set; }

    [Required]
    public string OptionName { get; set; }
        
    public decimal? SalesTax { get; set; }
        
    public decimal? ProfitAmount { get; set; }
        
    public decimal? LaborTotal { get; set; }
        
    public decimal? Total { get; set; }
        
    public decimal? HeatingLabor { get; set; }
        
    public decimal? HeatingCost { get; set; }
        
    public decimal? CoolingLabor { get; set; }
        
    public decimal? CoolingCost { get; set; }
        
    public decimal? IncentiveLabor { get; set; }
        
    public decimal? IncentiveCost { get; set; }
        
    public decimal? NoMarkupLabor { get; set; }
        
    public decimal? NoMarkupCost { get; set; }
        
    public decimal? SubTotal { get; set; }

    public decimal? FinancingFactor { get; set; }

    public decimal? FinancingRate { get; set; }

    public int? FinancingTerm { get; set; }

    public decimal? MonthlyPayment { get; set; }

    public string AhriRating { get; set; }

    public bool? AhriDirty { get; set; }

    [NotMapped] 
    public string AhriDescription => $"AHRI Rating: {(string.IsNullOrWhiteSpace(AhriRating) ? "N/A" : AhriRating)} {(AhriDirty ?? false ? " - (Modified)" : "")}";

    [JsonIgnore]
    [ForeignKey(nameof(EstimateId))]
    [System.Text.Json.Serialization.JsonIgnore]
    public Estimate Estimate { get; set; }

    public List<OptionItem> OptionItems { get; set; }
}