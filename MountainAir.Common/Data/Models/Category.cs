﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("Categories")]
public class Category
{
    [Key]
    public int CategoryId { get; set; }

    [Display(Name = "Category")]
    [StringLength(100)]
    [Required]
    public string CategoryName { get; set; }

    [Display(Name = "Heating")]
    public bool VisibleHeating { get; set; }

    [Display(Name = "Cooling")]
    public bool VisibleCooling { get; set; }

    [Display(Name = "Incentive")]
    public bool VisibleIncentive { get; set; }

    [Display(Name = "No-Markup")]
    public bool VisibleNoMarkup { get; set; }

    [Display(Name = "Left Side")]
    public bool LeftSideEstimate { get; set; }

    [Display(Name = "Right Side")]
    public bool RightSideEstimate { get; set; }

    [Display(Name = "Material")]
    public bool MaterialCategory { get; set; }

    [Display(Name = "Sort Order")]
    public int? SortOrder { get; set; }

    [JsonIgnore, System.Text.Json.Serialization.JsonIgnore]
    public List<CatalogItem> CatalogItems { get; set; }

    [NotMapped, JsonIgnore, System.Text.Json.Serialization.JsonIgnore]
    public string Group => MaterialCategory ? "Material" : "Equipment";
}