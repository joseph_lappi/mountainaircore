﻿using System;

namespace MountainAir.Common.Data.Models;

public class UploadRequest
{
    public Guid FileId { get; set; }

    public string FileData { get; set; }
}