﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("vCatalogItems")]
public class vCatalogItem
{
    public int CatalogItemId { get; set; }

    public int CategoryId { get; set; }

    public string CategoryName { get; set; }

    public bool LeftSideEstimate { get; set; }

    public bool RightSideEstimate { get; set; }

    public bool MaterialCategory { get; set; }

    public int? SortOrder { get; set; }

    public bool VisibleCooling { get; set; }

    public bool VisibleHeating { get; set; }

    public bool VisibleIncentive { get; set; }

    public bool VisibleNoMarkup { get; set; }
        
    public string CatalogNum { get; set; }

    public string Descr { get; set; }

    public string ModelNum { get; set; }

    public decimal Price { get; set; }

    public bool Active { get; set; }

    public string CustomerDesc { get; set; }

    public int? Labor { get; set; }
}