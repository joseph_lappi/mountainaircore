﻿using MountainAir.Common.Extensions;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("Addresses")]
public class Address
{
    [Key]
    public Guid AddressId { get; set; }

    public Guid CustomerId { get; set; }

    [Display(Name = "Address Line 1")]
    public string AddressLine1 { get; set; }

    [Display(Name = "Address Line 2")]
    public string AddressLine2 { get; set; }

    public string City { get; set; }

    public string State { get; set; }

    public string Zip { get; set; }

    [ForeignKey(nameof(CustomerId))]
    [JsonIgnore, System.Text.Json.Serialization.JsonIgnore]
    public Customer Customer { get; set; }

    [NotMapped]
    [JsonIgnore, System.Text.Json.Serialization.JsonIgnore]
    public string DisplayValue => this.FormatAddress();
}