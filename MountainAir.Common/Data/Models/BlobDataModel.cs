﻿using System;

namespace MountainAir.Common.Data.Models;

public class BlobDataModel
{
    public BlobDataModel() { }

    public BlobDataModel(BlobViewModel t)
    {
        this.Name = t.Name;
        this.Url = t.Url;
    }

    public string Name { get; set; }

    public DateTime Created { get; set; }

    public DateTime Modified { get; set; }

    public long Size { get; set; }

    public string Url { get; set; }
}