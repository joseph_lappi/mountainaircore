﻿using MountainAir.Common.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("OptionItems")]
public class OptionItem
{
    [Key]
    public Guid OptionItemId { get; set; }

    public Guid EstimateOptionId { get; set; }

    public int CategoryId { get; set; }

    [ForeignKey(nameof(CategoryId))]
    public Category Category { get; set; }

    public int? CatalogItemId { get; set; }

    public int Section { get; set; }

    public int? SystemNumber { get; set; }

    [StringLength(50)]
    public string CatalogNum { get; set; }

    [StringLength(100)]
    public string ModelNum { get; set; }

    [StringLength(50)]
    public string KeyCode { get; set; }

    [StringLength(200)]
    public string Descr { get; set; }

    public decimal ItemPrice { get; set; }

    public decimal? ItemLabor { get; set; }

    [StringLength(1000)]
    public string CustomerDesc { get; set; }

    public int? Quantity { get; set; }

    public decimal? SubTotalPrice { get; set; }

    public decimal? TotalPrice { get; set; }

    public decimal? MaterialTotal { get; set; }

    public decimal? MaterialLabor { get; set; }

    public decimal? TotalLabor { get; set; }

    public decimal? ProfitMargin { get; set; }

    public decimal? MonthlyPrice { get; set; }

    public string Location { get; set; }

    [NotMapped]
    public decimal? TaxAmount { get; set; }

    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    [ForeignKey("EstimateOptionId")]
    public EstimateOption EstimateOption { get; set; }
        
    public virtual List<OptionItemMaterial> OptionItemMaterials { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [NotMapped]
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public List<MaterialReport> MaterialList { get; } = new();

    [NotMapped]
    [System.Text.Json.Serialization.JsonIgnore]
    [JsonIgnore]
    public List<List<MaterialReport>> Lists => MaterialList.Split(2);

    /// <summary>
    /// 
    /// </summary>
    [NotMapped]
    [JsonIgnore]
    [System.Text.Json.Serialization.JsonIgnore]
    public List<OptionItemMaterial> SpecialMaterial { get; set; }

    [NotMapped]
    public bool IsCredit => Category != null && (Category.CategoryName.Contains("INSTANT", StringComparison.OrdinalIgnoreCase) || Category.CategoryName.Contains("DEFERRED", StringComparison.OrdinalIgnoreCase));

    [NotMapped]
    public bool InstantCredit => Category != null && Category.CategoryName.Contains("INSTANT", StringComparison.OrdinalIgnoreCase);

    [NotMapped]
    public bool DeferredCredit => Category != null && Category.CategoryName.Contains("DEFERRED", StringComparison.OrdinalIgnoreCase);
}