﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Data.Models;

[Table("LogAttempts")]
public class LogAttempt
{
    [Key]
    public int Id { get; set; }

    public DateTime LogDate { get; set; }

    [StringLength(100)]
    [Required]
    public string UserName { get; set; }

    public bool Success { get; set; }

    public string ErrorMessage { get; set; }
}