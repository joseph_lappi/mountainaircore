﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Text;

namespace MountainAir.Common.Security;

public class TokenSettings
{
    private readonly TimeSpan _expiresSpan = TimeSpan.FromDays(1);

    private const string Secret = "q7GJ*Qi2j1ksxoPBxO#ZvTdvKfX9M1h4*Z5dWaoX6pv*Q#3N^nMlb#nmm!cKIiYB^@Q&XQ@ejYTXX$MyLctRTQ2*y@R**&FMq4t";

    public const int RefreshTokenTTL = 2;
        
    public const string Issuer = "MountainAirApi";

    public const string Audience = "MountainAirApi";

    public TimeSpan ExpiresSpan => _expiresSpan;

    public string TokenType => "Bearer";

    public static SymmetricSecurityKey Key => GetSignInKey(); 

    public static SigningCredentials SigningCredentials => new(Key, SecurityAlgorithms.HmacSha256Signature);

    private static SymmetricSecurityKey GetSignInKey()
    {
        var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Secret));

        return signingKey;
    }
}