﻿using Microsoft.AspNetCore.Identity;
using System;

namespace MountainAir.Common.Security;

public class ApplicationRole : IdentityRole<Guid>
{
}