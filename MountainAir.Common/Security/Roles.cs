﻿namespace MountainAir.Common.Security;

public enum Roles
{
    Administrators,
    CanCreateCatalog,
    CanDeleteCatalog,
    CanCatalog,
    CanCustomers,
    CanProposals,
    CanViewCatalog,
    CanEditCatalog,
    CanViewCustomers,
    CanViewProposals,
    CanEditCustomers,
    CanEditProposals,
    CanEditNews,
    CanViewNews,
    CanViewAllProposals,
    TestUser
}