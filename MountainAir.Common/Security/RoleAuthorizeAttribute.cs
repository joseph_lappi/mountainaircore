﻿using System.Text;
using Microsoft.AspNetCore.Authorization;

namespace MountainAir.Common.Security;

public class RoleAuthorizeAttribute : AuthorizeAttribute
{
    public RoleAuthorizeAttribute(params Roles[] roles)
    {
        base.Roles = GetRoleString(roles);
    }

    internal static string GetRoleString(params Roles[] roles)
    {
        StringBuilder sb = new StringBuilder();

        foreach (var r in roles)
        {
            if (sb.Length > 0)
                sb.Append(",");

            sb.Append(r.ToString());
        }

        return sb.ToString();
    }
}