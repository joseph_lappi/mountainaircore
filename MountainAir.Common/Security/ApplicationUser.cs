﻿using Microsoft.AspNetCore.Identity;
using MountainAir.Common.Data.Interfaces;
using MountainAir.Common.Extensions;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MountainAir.Common.Security;

public class ApplicationUser : IdentityUser<Guid>, IUserRecord
{
    /// <summary>
    /// 
    /// </summary>
    public string LastName { get; set; }

    [Display(Name = "Display Name")]
    [NotMapped]
    public string DisplayName => this.FormatDisplayName();

    [Display(Name = "Full Name")]
    [NotMapped]
    public string FullName => this.FormatFullName();

    /// <summary>
    /// 
    /// </summary>
    public string FirstName { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public bool Active { get; set; }
        
    /// <summary>
    /// 
    /// </summary>
    public DateTime AddDate { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [Column(TypeName = "decimal(18,2)")]
    public decimal? DefaultSalesTax { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [Column(TypeName = "decimal(18,2)")]
    public decimal? DefaultLaborRate { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [Column(TypeName = "decimal(18,2)")]
    public decimal? DefaultProfitMargin { get; set; }
       
    public string RefreshToken { get; set; }
}