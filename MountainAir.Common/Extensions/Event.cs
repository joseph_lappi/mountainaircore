﻿using System;
using System.Linq;

namespace MountainAir.Common.Extensions;

public static class Event
{
    public static void Fire(this EventHandler eh, object sender)
    {
        if (eh == null)
            return;
        eh(sender, EventArgs.Empty);
    }

    public static void Fire<T>(this EventHandler<T> eh, object sender)
        where T : EventArgs
    {
        if (eh == null)
            return;
        eh(sender, sender as T);
    }

    /// <summary>
    /// Finds all events that end with "Changed" and registers an event handler with each event.
    /// </summary>
    /// <param name="o"></param>
    /// <param name="handler"></param>
    public static void BindAllPropertyChangedEvents<T>(this T o, EventHandler handler)
    {
        if (o == null)
            return;

        var events = o.GetType().GetEvents().Where(
            e => e.Name.EndsWith("Changed") && e.EventHandlerType == typeof(EventHandler));
        foreach (var e in events)
        {
            e.AddEventHandler(o, handler);
        }
    }
}