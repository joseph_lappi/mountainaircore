﻿using Microsoft.Extensions.Configuration;
using MountainAir.Common.Utility;
using System;
using System.Collections.Generic;
using System.Text;

namespace MountainAir.Common.Extensions;

public static class IConfigurationExtensions
{
    public static T Deserialize<T>(this IConfiguration config, string sectionKey)
        where T : new()
    {
        var section = config.GetSection(sectionKey);

        return DeserializeFromConfiguration<T>(section);
    }

    private static T DeserializeFromConfiguration<T>(IConfigurationSection section)
        where T : new()
    {
        Type t = typeof(T);

        var g = Activator.CreateInstance<T>();

        var properties = t.GetProperties();

        foreach (var prop in properties)
        {
            string serializationName = ReflectionHelper.GetSerializationName(t, prop.Name);

            string value = section.GetValue<string>(serializationName);

            prop.SetValue(g, value);
        }

        return g;
    }
}