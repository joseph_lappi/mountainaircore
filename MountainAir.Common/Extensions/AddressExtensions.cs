﻿using MountainAir.Common.Data.Models;

namespace MountainAir.Common.Extensions;

public static class AddressExtensions
{
    public static string FormatAddress(this Address address)
    {
        return $"{address.AddressLine1}, {address.AddressLine2}, {address.City}, {address.State}, {address.Zip}";
    }
}