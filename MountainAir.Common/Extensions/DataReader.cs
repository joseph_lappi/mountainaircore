﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MountainAir.Common.Extensions;

public static class DataReader
{
    public static List<T> MapToList<T>(this DbDataReader dr)
        where T: new()
    {
        var entities = new List<T>();
        if(dr != null && dr.HasRows)
        {
            var entity = typeof(T);
            var propDict = new Dictionary<string, PropertyInfo>();
            var props = entity.GetProperties(BindingFlags.Instance | BindingFlags.Public);
            propDict = props.ToDictionary(p => p.Name.ToUpper(), p => p);

            while(dr.Read())
            {
                T newObject = new T();
                for(int i = 0; i < dr.FieldCount; i++)
                {
                    string key = dr.GetName(i).ToUpper();

                    if (propDict.ContainsKey(key))
                    {
                        var info = propDict[key];

                        if(info != null && info.CanWrite)
                        {
                            var val = dr.GetValue(i);
                            info.SetValue(newObject, (val == DBNull.Value ? null : val), null);
                        }
                    }
                }
                entities.Add(newObject);
            }
        }
            
        return entities;
    }
}