﻿using Microsoft.AspNetCore.Identity;
using MountainAir.Common.Security;
using System.Threading.Tasks;

namespace MountainAir.Common.Extensions;

public static class Security
{
    public static Task<bool> IsInRoleAsync(this UserManager<ApplicationUser> userManager, ApplicationUser user, Roles role)
    {
        return userManager.IsInRoleAsync(user, role.ToString());
    }
}