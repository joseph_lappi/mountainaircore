﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;

namespace MountainAir.Common.Extensions;

public static class Reflection
{
    /// <summary>
    /// 
    /// </summary>
    /// <param name="source"></param>
    /// <param name="target"></param>
    public static void CopyProperties<T>(this T source, T target)
    {
        var plist = from prop in typeof(T).GetProperties()
            where prop.CanRead && 
                  prop.CanWrite
            select prop;

        foreach (var prop in plist)
        {
            if (prop.CustomAttributes.All(c => c.AttributeType != typeof(KeyAttribute)) && prop.CustomAttributes.All(c => c.AttributeType != typeof(ForeignKeyAttribute)))
            {
                prop.SetValue(target, prop.GetValue(source, null), null);
            }
        }
    }

    public static void CopyProperties(this object source, object target, params string[] excludedFields)
    {
        if (source == null || target == null)
            return;

        var sourceProps = from prop in source.GetType().GetProperties() where prop.CanRead && prop.CanWrite select prop;
        var targetProps = from prop in target.GetType().GetProperties() where prop.CanRead && prop.CanWrite select prop;

        foreach (PropertyInfo sourceProp in sourceProps)
        {
            if (excludedFields != null && excludedFields.Contains(sourceProp.Name))
                continue;

            var targetProp = targetProps.FirstOrDefault(i => i.Name == sourceProp.Name);

            if (targetProp != null)
            {
                var value = sourceProp.GetValue(source, null)?.ToString();

                Type t = targetProp.PropertyType;

                if (value == null && t == typeof(string))
                    targetProp.SetValue(target, null, null);
                else if (t == typeof(string))
                    targetProp.SetValue(target, value, null);
                else if ((t == typeof(int) || t == typeof(int?)) && int.TryParse(value, out int i))
                    targetProp.SetValue(target, i, null);
                else if ((t == typeof(decimal) || t == typeof(decimal?)) && decimal.TryParse(value, out decimal de))
                    targetProp.SetValue(target, de, null);
                else if ((t == typeof(long) || t == typeof(long?)) && long.TryParse(value, out long l))
                    targetProp.SetValue(target, l, null);
                else if ((t == typeof(DateTime) || t == typeof(DateTime?)) && DateTime.TryParse(value, out DateTime dt))
                    targetProp.SetValue(target, dt, null);
                else if ((t == typeof(bool) || t == typeof(bool?)) && bool.TryParse(value, out bool b))
                    targetProp.SetValue(target, b, null);
                else if ((t == typeof(Guid) || t == typeof(Guid?)) && Guid.TryParse(value, out Guid g))
                    targetProp.SetValue(target, g, null);
                else if (t.BaseType == typeof(Enum))
                    targetProp.SetValue(target, Enum.Parse(t, value));
            }
        }
    }
}