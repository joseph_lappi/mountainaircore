﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MountainAir.Common.Extensions;

public static class StreamExtensions
{
    public static IEnumerable<T> StreamOrEmpty<T>(this IEnumerable<T> stm)
    {
        return (stm != null) ? stm : new T[0];
    }
}