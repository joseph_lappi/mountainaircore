﻿using MountainAir.Common.Data.Interfaces;
using System.Text;

namespace MountainAir.Common.Extensions;

public static class CustomerExtensions
{
    public static string FormatDisplayName(this IUserRecord customer)
    {
        return $"{customer.FirstName} {customer.LastName}".Trim();
    }
        
    public static string FormatFullName(this IUserRecord customer)
    {
        return FormatFullName(customer, customer.FirstName, customer.LastName);

        //string lastName = customer.LastName;
        //string firstName = customer.FirstName;

        //StringBuilder sb = new StringBuilder();

        //if (!string.IsNullOrEmpty(lastName))
        //    sb.Append(lastName);

        //if (sb.Length > 0)
        //    sb.Append(", ");

        //if (!string.IsNullOrEmpty(firstName))
        //    sb.Append(firstName);

        //return sb.ToString();
    }

    public static string FormatFullName(this IUserRecord customer, string firstName, string lastName)
    {
        StringBuilder sb = new StringBuilder();

        if (!string.IsNullOrEmpty(lastName))
            sb.Append(lastName);

        if (sb.Length > 0)
            sb.Append(", ");

        if (!string.IsNullOrEmpty(firstName))
            sb.Append(firstName);

        return sb.ToString();
    }
}