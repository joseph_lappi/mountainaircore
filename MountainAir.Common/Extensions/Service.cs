﻿using System;

namespace MountainAir.Common.Extensions;

public static class Service
{
    /// <summary>
    /// Strongly typed version of IServiceProvider.GetService to get rid of all the casts and typeofs.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="sp"></param>
    /// <returns></returns>
    public static T GetService<T>(this IServiceProvider sp)
    {
        return (T)sp.GetService(typeof(T));
    }

    /// <summary>
    /// A bit stronger than GetService, this requires the service to be present, or an exception is thrown.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="sp"></param>
    /// <returns>the service instance.</returns>
    public static TService RequireService<TService>(this IServiceProvider sp)
    {
        var serviceType = typeof(TService);
        var service = (TService)sp.GetService(serviceType);
        if (service == null)
            throw new InvalidOperationException($"Unable to load service: '{serviceType.FullName}'.");
        return service;
    }
}