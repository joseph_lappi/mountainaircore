﻿using System.Collections.Generic;
using System.Linq;

namespace MountainAir.Common.Extensions;

public static class Collection
{
    public static List<List<T>> Split<T>(this IEnumerable<T> source, int columnCount)
    {
        int count = source.Count() / columnCount;

        if (source.Count() % 2 != 0)
            count++;

        var retval = source.Select((x, i) => new { Index = i, Value = x }).GroupBy(x => x.Index / count).Select(x => x.Select(v => v.Value).ToList()).ToList();

        return retval;
    }
}