﻿using Microsoft.Extensions.Configuration;
using MountainAir.Common.Extensions;

namespace MountainAir.Common.Configuration;

public class AppSettings
{
    public AppSettings()
    {
    }

    public AppSettings(IConfiguration config)
    {
        //AuthConfig = config.Deserialize<GoogleAuthConfig>("GoogleAuth");

        DefaultConnectionString = config.GetConnectionString("DefaultConnection");
    }

    public string DefaultConnectionString { get; set; }
        
    public static AppSettings Shared { get; set; }
}