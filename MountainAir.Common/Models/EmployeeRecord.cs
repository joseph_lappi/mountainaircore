﻿using System;
using MountainAir.Common.Extensions;
using MountainAir.Common.Security;

namespace MountainAir.Common.Models;

public class EmployeeRecord
{
    public EmployeeRecord(ApplicationUser u)
    {
        if (u == null)
            return;

        u.CopyProperties(this);
            
        FullName = u.FullName;
        DisplayName = u.DisplayName;
    }
        
    public Guid Id { get; set; }

    public string LastName { get; set; }

    public string DisplayName { get; set; }

    public string FullName { get; set; }

    public string FirstName { get; set; }

    public decimal? DefaultSalesTax { get; set; }

    public decimal? DefaultLaborRate { get; set; }

    public decimal? DefaultProfitMargin { get; set; }
}