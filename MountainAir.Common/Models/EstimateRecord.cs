﻿using System;
using System.Collections.Generic;
using System.Linq;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Extensions;

namespace MountainAir.Common.Models;

public class EstimateRecord
{
    public EstimateRecord(Estimate e)
    {
        e.CopyProperties(this);

        Id = e.EstimateId;

        EstimateOptions = e.EstimateOptions.Select(p => p.EstimateOptionId);
        Customer = new CustomerRecord(e.Customer);
        Address = new AddressRecord(e.Address);
        BillingAddress = new AddressRecord(e.BillingAddress);
        Assigned = new EmployeeRecord(e.Assigned);
        Created = new EmployeeRecord(e.Created);
        Updated = new EmployeeRecord(e.Updated);
    }

    public Guid Id { get; set; }

    public IEnumerable<Guid> EstimateOptions { get; set; }

    public CustomerRecord Customer { get; set; }

    public AddressRecord Address { get; set; }

    public AddressRecord BillingAddress { get; set; }

    public EmployeeRecord Assigned { get; set; }

    public EmployeeRecord Created { get; set; }

    public EmployeeRecord Updated { get; set; }

    public string EstimateNumber { get; set; }

    public DateTime CreatedDate { get; set; }

    public string EstimateNotes { get; set; }

    public DateTime? UpdatedDate { get; set; }

    public DateTime? InstallationDate { get; set; }

    public DateTime? ApprovalDate { get; set; }
        
    public decimal? SystemTotal { get; set; }

    public decimal? ProfitMarginRate { get; set; }

    public decimal? SalesTaxRate { get; set; }

    public string CustomerApproval { get; set; }

    public string DesignerName { get; set; }

    public string DesignerPhone { get; set; }

    public decimal? BtuGain { get; set; }
        
    public decimal? BtuLoss { get; set; }
        
    public int? SummerTempDiff { get; set; }
        
    public int? WinterTempDiff { get; set; }
        
    public decimal? WindowRValue { get; set; }
        
    public decimal? DoorRValue { get; set; }
        
    public decimal? CeilingHeight { get; set; }
        
    public decimal? AppliancesMultiplier { get; set; }
        
    public decimal? CeilingRValue { get; set; }
        
    public decimal? LaborRate { get; set; }
        
    public decimal? InfiltrationGWMultiplier { get; set; }
        
    public decimal? PeopleMultiplier { get; set; }
        
    public decimal? FloorRValue { get; set; }
        
    public decimal FinancingRate { get; set; }
        
    public int FinancingTerm { get; set; }
        
    public decimal? WallRValue { get; set; }
        
    public decimal? CoolingCFM { get; set; }
        
    public decimal? HeatingCFM { get; set; }
        
    public decimal? HeatingRegisters { get; set; }
        
    public decimal? CoolingRegisters { get; set; }
        
    public decimal? CoolingCFMDivisor { get; set; }
        
    public decimal? HeatingCFMDivisor { get; set; }
        
    public decimal? AirTonage { get; set; }
        
    public Guid? SelectedOptionId { get; set; }
        
    public decimal FinancingFactor { get; set; }
        
    public bool Active { get; set; }
        
    public string PaymentMethod { get; set; }
        
    public bool? ReadyToProcess { get; set; }

    public decimal? DownPayment { get; set; }
        
    public string FinancingString => $"{FinancingRate}% for {FinancingTerm} months";
}