﻿namespace MountainAir.Common.Models;

public class EmailAttachment
{
    public string FileName { get; set; }
    public string Content { get; set; }
    public string ContentType { get; set; }
}