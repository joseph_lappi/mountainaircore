﻿using System;
using MountainAir.Common.Data.Models;

namespace MountainAir.Common.Models;

public class AddressRecord
{
    public AddressRecord(Address a)
    {
        if (a == null)
            return;

        Id = a.AddressId;
        AddressLine1 = a.AddressLine1;
        AddressLine2 = a.AddressLine2;
        City = a.City;
        State = a.State;
        Zip = a.Zip;
    }

    public Guid Id { get; set; }

    public string Zip { get; set; }

    public string State { get; set; }

    public string City { get; set; }

    public string AddressLine2 { get; set; }

    public string AddressLine1 { get; set; }
}