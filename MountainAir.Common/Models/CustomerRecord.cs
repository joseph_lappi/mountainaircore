﻿using System;
using System.Collections.Generic;
using System.Linq;
using MountainAir.Common.Data.Models;

namespace MountainAir.Common.Models;

public class CustomerRecord
{
    public CustomerRecord(Customer c)
    {
        Id = c.CustomerId;
        LastName = c.LastName;
        FirstName = c.FirstName;
        CompanyName = c.CompanyName;
        PhoneNumber1 = c.PhoneNumber1;
        PhoneNumber2 = c.PhoneNumber2;
        Email = c.Email;
        DisplayName = c.DisplayName;
        FullName = c.FullName;
        TestData = c.TestData.GetValueOrDefault(false);

        Addresses = c.Addresses.Select(p => p.AddressId);
    }

    public IEnumerable<Guid> Addresses { get; set; }

    public Guid Id { get; set; }

    public bool TestData { get; set; }

    public string FullName { get; set; }

    public string DisplayName { get; set; }

    public string Email { get; set; }

    public string PhoneNumber2 { get; set; }

    public string PhoneNumber1 { get; set; }

    public string CompanyName { get; set; }

    public string FirstName { get; set; }

    public string LastName { get; set; }
}