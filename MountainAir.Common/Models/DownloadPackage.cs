﻿using MountainAir.Common.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace MountainAir.Common.Models;

public class FullDownloadPackage : CatalogOnlyPackage
{
    public List<UserModel> Users { get; set; }

    public List<Customer> Customers { get; set; }

    public List<Estimate> Estimates { get; set; }
}

public class CatalogOnlyPackage
{
    public List<WarrantyType> WarrantyTypes { get; set; }

    public UserModel CurrentUser { get; set; }

    public List<PaymentMethod> PaymentMethods { get; set; }

    public List<Financing> Financings { get; set; }

    public List<Category> Categories { get; set; }

    public List<CatalogItem> CatalogItems { get; set; }

    public List<Feature> Features { get; set; }

    public List<Package> Packages { get; set; }

    public List<Location> Locations { get; set; }
}