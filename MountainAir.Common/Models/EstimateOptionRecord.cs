﻿using MountainAir.Common.Data.Models;
using MountainAir.Common.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MountainAir.Common.Models;

public class EstimateOptionRecord
{
    public EstimateOptionRecord(EstimateOption e)
    {
        e.CopyProperties(this);

        Id = e.EstimateOptionId;

        OptionItems = e.OptionItems.Select(p => p.OptionItemId);
    }

    public IEnumerable<Guid> OptionItems { get; set; }

    public Guid Id { get; set; }

    public int OptionNumber { get; set; }

    public string OptionName { get; set; }

    public decimal? SalesTax { get; set; }

    public decimal? ProfitAmount { get; set; }

    public decimal? LaborTotal { get; set; }

    public decimal? Total { get; set; }

    public decimal? HeatingLabor { get; set; }

    public decimal? HeatingCost { get; set; }

    public decimal? CoolingLabor { get; set; }

    public decimal? CoolingCost { get; set; }

    public decimal? IncentiveLabor { get; set; }

    public decimal? IncentiveCost { get; set; }

    public decimal? NoMarkupLabor { get; set; }

    public decimal? NoMarkupCost { get; set; }

    public decimal? SubTotal { get; set; }

    public decimal? FinancingFactor { get; set; }

    public decimal? FinancingRate { get; set; }

    public int? FinancingTerm { get; set; }

    public decimal? MonthlyPayment { get; set; }
}

public class OptionItemRecord
{
    public OptionItemRecord(OptionItem o)
    {
        o.CopyProperties(this);

        Id = o.EstimateOptionId;

        InstantCredit = o.Category.CategoryName.Contains("INSTANT", StringComparison.OrdinalIgnoreCase);

        DeferredCredit = o.Category.CategoryName.Contains("DEFERRED", StringComparison.OrdinalIgnoreCase);

        OptionItemMaterials = o.OptionItemMaterials.Select(p => p.OptionItemMaterialId);
    }

    public IEnumerable<Guid> OptionItemMaterials { get; set; }

    public Guid Id { get; set; }

    public int CategoryId { get; set; }

    public int? CatalogItemId { get; set; }

    public int Section { get; set; }

    public int? SystemNumber { get; set; }

    public string CatalogNum { get; set; }

    public string ModelNum { get; set; }

    public string KeyCode { get; set; }

    public string Descr { get; set; }

    public decimal ItemPrice { get; set; }

    public decimal? ItemLabor { get; set; }

    public string CustomerDesc { get; set; }

    public int? Quantity { get; set; }

    public decimal? SubTotalPrice { get; set; }

    public decimal? TotalPrice { get; set; }

    public decimal? MaterialTotal { get; set; }

    public decimal? MaterialLabor { get; set; }

    public decimal? TotalLabor { get; set; }

    public decimal? ProfitMargin { get; set; }

    public decimal? MonthlyPrice { get; set; }

    public bool IsCredit => InstantCredit || DeferredCredit;

    public bool InstantCredit { get; set; }

    public bool DeferredCredit { get; set; }
}

public class OptionMaterialRecord
{
    public OptionMaterialRecord(OptionItemMaterial m)
    {
        m.CopyProperties(this);

        Id = m.OptionItemMaterialId;
    }

    public Guid Id { get; set; }

    public int CategoryId { get; set; }

    public int CatalogItemId { get; set; }

    public int? Labor { get; set; }

    public string CatalogNum { get; set; }

    public decimal? Price { get; set; }

    public int Quantity { get; set; }

    public string CustomerDesc { get; set; }

    public string ModelNum { get; set; }

    public string Descr { get; set; }

    public string KeyCode { get; set; }

    public string Category { get; set; }
}