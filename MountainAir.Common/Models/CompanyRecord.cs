﻿using System;
using System.Collections.Generic;

namespace MountainAir.Common.Models;

public class CompanyRecord
{
    public CompanyRecord(Guid id, string companyName, bool active)
    {
        Properties = new CompanyRecordProperties()
        {
            Active = active,
            Id = id,
            Name = companyName
        };
    }

    public CompanyRecordProperties Properties { get; set; }

    public Dictionary<Guid, EstimateRecord> Estimates { get; set; }

    public Dictionary<Guid, CustomerRecord> Customers { get; set; }

    public Dictionary<Guid, EmployeeRecord> Employees { get; set; }

    public Dictionary<int, CategoryRecord> Catalog { get; set; }
}

public class CompanyRecordProperties
{
    public Guid Id { get; set; }

    public string Name { get; set; }

    public bool Active { get; set; }
}