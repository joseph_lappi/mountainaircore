﻿using System;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Extensions;

namespace MountainAir.Common.Models;

public class WindowRecord
{
    public WindowRecord(Window w)
    {
        w.CopyProperties(this);

        Id = w.WindowId;
    }
        
    public Guid Id { get; set; }

    public decimal? BTULoss { get; set; }

    public decimal? BTUGain { get; set; }

    public bool? Fixed { get; set; }

    public decimal? Width { get; set; }

    public decimal? Height { get; set; }

    public int? NumberOfWindows { get; set; }
        
    public string Direction { get; set; }

    public string WindowType { get; set; }

    public decimal? Infiltration { get; set; }

    public decimal? FixedPerimeter { get; set; }

    public decimal? SlidingPerimeter { get; set; }
}