﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Extensions;
using Newtonsoft.Json;

namespace MountainAir.Common.Models;

public class PackageRecord
{
    public PackageRecord(Package p)
    {
        p.CopyProperties(this);

        Id = p.PackageId;

        PackageItems = p.PackageItems.Select(i => i.PackageItemId);
    }
        
    public int Id { get; set; }

    public IEnumerable<int> PackageItems { get; set; }        
        
    public string PackageName { get; set; }

    public bool Active { get; set; }
        
    public decimal? ProfitMargin { get; set; } 
}
    
public class PackageItemRecord
{
    public PackageItemRecord(PackageItem p)
    {
        p.CopyProperties(this);

        Id = p.PackageItemId;
        PackageMaterials = p.PackageMaterials.Select(i => i.PackageMaterialId);
    }

    public int Id { get; set; }

    public IEnumerable<int> PackageMaterials { get; set; }   

    public int PackageId { get; set; }

    public int CatalogItemId { get; set; }

    public int Quantity { get; set; }

    [JsonIgnore]
    public decimal? Price { get; set; }

    public decimal? AdditionalPrice { get; set; }

    //Added this way to minimize changes to the app. 
    [JsonProperty("Price")]
    public decimal CalculatedPrice 
    {
        get
        {
            if(this.Price.HasValue)
            {
                decimal price = this.Price.GetValueOrDefault(0);

                if(AdditionalPrice.HasValue)
                {
                    price += AdditionalPrice.GetValueOrDefault(0);
                }

                return price;
            }
            return 0;
        }
    }

    public int? Labor { get; set; }

    public int Section { get; set; }
}
    
public class PackageItemMaterialRecord
{
    public PackageItemMaterialRecord(PackageMaterial p)
    {
        p.CopyProperties(this);

        this.Id = p.PackageMaterialId;
    }

    public int Id { get; set; }

    public int PackageItemId { get; set; }

    public int CatalogItemId { get; set; }

    public int Quantity { get; set; }

    public decimal? Price { get; set; }

    public int? Labor { get; set; }
}