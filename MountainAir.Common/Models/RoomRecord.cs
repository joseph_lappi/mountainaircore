﻿using System;
using System.Collections.Generic;
using System.Linq;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Extensions;

namespace MountainAir.Common.Models;

public class RoomRecord
{
    public RoomRecord(Room r)
    {
        r.CopyProperties(this);

        Id = r.RoomId;

        Doors = r.Doors.Select(p => p.DoorId);
        Windows = r.Windows.Select(p => p.WindowId);
    }

    public IEnumerable<Guid> Doors { get; set; }

    public IEnumerable<Guid> Windows { get; set; }

    public Guid Id { get; set; }

    public decimal? AppliancesBtu { get; set; }

    public decimal? AppliancesMultiplier { get; set; }

    public bool? CalculatingBasement { get; set; }

    public decimal? CeilingHeight { get; set; }

    public decimal? ColdCeilingBtu { get; set; }

    public decimal? ColdFloorBtu { get; set; }

    public decimal? DoorBTULoss { get; set; }

    public decimal? DoorBTUGain { get; set; }

    public decimal? ExposedWall { get; set; }

    public decimal? InfiltationGWPercent { get; set; }

    public decimal? InfiltrationGWBtu { get; set; }

    public decimal? NetExpWallBtu { get; set; }

    public decimal? NetExpWallPercent { get; set; }

    public decimal? NetWallBtu { get; set; }

    public decimal? NetWallPercent { get; set; }

    public decimal? PeopleBtu { get; set; }

    public decimal? PeopleMultiplier { get; set; }

    public decimal? RoomLength { get; set; }

    public string RoomLocation { get; set; }

    public string RoomName { get; set; }

    public decimal? RoomWidth { get; set; }

    public decimal? TotalSensible { get; set; }

    public decimal? WarmCeilingBtu { get; set; }

    public decimal? WarmCeilingPercent { get; set; }

    public decimal? WarmFloorBtu { get; set; }

    public decimal? WarmFloorPercent { get; set; }

    public int? PeopleCount { get; set; }

    public int? AppliancesCount { get; set; }

    public decimal? DoorArea { get; set; }

    public decimal? RoomArea { get; set; }

    public decimal? SubtotalSensible { get; set; }

    public string TotalSensibleControl { get; set; }

    public bool? VaultedCeiling { get; set; }

    public decimal? WindowSlidingArea { get; set; }

    public decimal? WindowBTULoss { get; set; }

    public decimal? WindowFixedArea { get; set; }

    public decimal? WindowBTUGain { get; set; }

    public decimal? RoomTotal { get; set; }

    public decimal? FloorRValue { get; set; }

    public decimal? InfiltrationGWMultiplier { get; set; }

    public decimal? CeilingRValue { get; set; }

    public decimal? WallRValue { get; set; }

    public decimal? DoorInfiltration { get; set; }

    public decimal? WindowInfiltration { get; set; }

    public bool? ApplyDuctLossMultiplier { get; set; }

    public decimal? CoolingCFM { get; set; }

    public decimal? HeatingCFM { get; set; }

    public int? CoolingRegisters { get; set; }

    public int? HeatingRegisters { get; set; }

    public int? AdditionalRegisters { get; set; }

    public int? ColdFloorPercent { get; set; }

    public int? SortOrder { get; set; }
}