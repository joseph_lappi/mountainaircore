﻿using System.Collections.Generic;
using System.Linq;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Extensions;

namespace MountainAir.Common.Models;

public class CatalogItemRecord
{
    public CatalogItemRecord(CatalogItem c)
    {
        c.CopyProperties(this);

        Id = c.CatalogItemId;
    }

    public int Id { get; set; }

    public int CategoryId { get; set; }

    public string CatalogNum { get; set; }

    public string ModelNum { get; set; }

    public string Descr { get; set; }

    public decimal Price { get; set; }

    public int? Labor { get; set; }
        
    public string CustomerDesc { get; set; }

    public bool Active { get; set; } = true;

    public string KeyCode { get; set; }

    public decimal? MonthlyPrice { get; set; }
}

public class CategoryRecord
{
    public CategoryRecord(Category c)
    {
        c.CopyProperties(this);

        Id = c.CategoryId;

        CatalogItems = c.CatalogItems.Select(p => p.CatalogItemId);
    }

    public IEnumerable<int> CatalogItems { get; set; }

    public int Id { get; set; }

    public string CategoryName { get; set; }

    public bool VisibleHeating { get; set; }

    public bool VisibleCooling { get; set; }

    public bool VisibleIncentive { get; set; }

    public bool VisibleNoMarkup { get; set; }

    public bool LeftSideEstimate { get; set; }

    public bool RightSideEstimate { get; set; }

    public bool MaterialCategory { get; set; }

    public int? SortOrder { get; set; }
}