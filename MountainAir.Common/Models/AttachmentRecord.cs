﻿using MountainAir.Common.Data.Models;

namespace MountainAir.Common.Models;

public class AttachmentRecord
{
    public AttachmentRecord(BlobViewModel b)
    {
        Name = b.Name;
        Url = b.Url;
    }

    public string Url { get; set; }

    public string Name { get; set; }
}