﻿using System;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Extensions;

namespace MountainAir.Common.Models;

public class DoorRecord
{
    public DoorRecord(Door d)
    {
        d.CopyProperties(this);

        Id = d.DoorId;
    }
        
    public Guid Id { get; set; }

    public decimal? Width { get; set; }
        
    public decimal? Height { get; set; }
        
    public decimal? BTUGain { get; set; }
        
    public int? NumberOfDoors { get; set; }
        
    public decimal? BTULoss { get; set; }
        
    public int? RValue { get; set; }
        
    public decimal? Infiltration { get; set; }
        
    public decimal? Perimeter { get; set; }
}