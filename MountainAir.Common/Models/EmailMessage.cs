﻿using System.Collections.Generic;

namespace MountainAir.Common.Models;

public class EmailMessage
{
    public string Email { get; set; }
        
    public string Cc { get; set; }
        
    public string Bcc { get; set; }

    public string Subject { get; set; }

    public string Message { get; set; }
        
    public List<EmailAttachment> Attachments { get; set; } = new();
}