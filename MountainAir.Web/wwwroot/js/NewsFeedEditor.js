﻿(function ($) {
    function NewsFeedEditor() {
        var $this = this;

        function initialize() {
            $('#FullText').summernote({
                focus: true,
                height: 150,
                codemirror: {
                    theme: 'united'
                }
            });
        }

        $this.init = function () {
            initialize();
        }
    }
    $(function () {
        var self = new NewsFeedEditor();
        self.init();
    });
}(jQuery));