﻿// requires vendor/daterangepicker/daterangepicker.css to be included first
// requires vendor/daterangepicker/daterangepicker.js to be included first
// requires Vue.js main script to be included first
// requires jquery to be included first
// requires momentJS to be included first

// accepts and returns dateRange objects with JavaScript dates
// (uses Moment under the covers)
// example:
//---
// JS:
/*
new Vue({
	el: '#vue-root',
	data: {
		myDateRange: { startDate: Date.UTC(2020, 04, 01), endDate: Date.UTC(2020, 04, 30) },
		// optional
		overrideRanges: {
			'This Month': [moment.utc().startOf('month'), moment.utc().endOf('month')],
			'Last Month': [moment.utc().subtract(1, 'month').startOf('month'), moment.utc().subtract(1, 'month').endOf('month')]
		},
		// optional (see https://momentjs.com/docs/#/parsing/object/)
		maxSpan: { days: 45 }
	}
});
*/
//---
// HTML: (ranges is optional - defaults are provided if you don't set it.)
// <date-range-picker v-model="myDateRange" :ranges="overrideRanges" :max-span="maxSpan"></date-range-picker>

Vue.component('date-range-picker', {
	template: `<input type="text" />`,
	props: ['value', 'ranges', 'maxSpan'],

	mounted: function () {
		var self = this;

		const utcToday = moment().startOf('day').utc(true);
		const utcEndOfToday = utcToday.endOf('day');

		// Clone the "moment"s above since moments are mutable.
		const defaultRanges = {
			'Today': [utcToday.clone(), utcEndOfToday.clone()],
			'Yesterday': [utcToday.clone().subtract(1, 'days'), utcEndOfToday.clone().subtract(1, 'days')],
			'Last 7 Days': [utcToday.clone().subtract(6, 'days'), utcEndOfToday.clone()],
			'Last 30 Days': [utcToday.clone().subtract(29, 'days'), utcEndOfToday.clone()],
			'This Month': [utcToday.clone().startOf('month'), utcToday.clone().endOf('month').endOf('day')],
			'Last Month': [utcToday.clone().startOf('month').subtract(1, 'month'), utcToday.clone().subtract(1, 'month').endOf('month').endOf('day')]
		}

		const configRanges = this.ranges || defaultRanges;

		// Options
		const options = {
			startDate: moment.utc(this.value.startDate),
			endDate: moment.utc(this.value.endDate),
			ranges: configRanges
		};

		if(this.maxSpan) {
			options.maxSpan = this.maxSpan;
		}

		$(this.$el).daterangepicker(
			// Options
			options,
			// Changed event handler 
			function (start, end, label) {
				// Start is the beginning of the day in Local time zone
				// End if the end of the day "11:59:59 PM" in the Local time zone
				// Ensure beginning of day UTC for start and end of day UTC for end (same dates but UTC timezone instead of local)
				const utcStart = start.utc(true);
				const utcEnd = end.utc(true);

				const newDateRange = { startDate: utcStart.toDate(), endDate: utcEnd.toDate() };
				self.$emit('input', newDateRange);
			}
		);
	}
});