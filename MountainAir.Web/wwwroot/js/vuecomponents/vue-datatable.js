﻿Vue.component('datatable', {
    props: {
        // We pass the config to the component so we have full control over configuration and event handling.
        config: { type: Object, required: true },

        // We bind the table data so we can change it from the calling code and the component will handle redrawing, etc.
        tableData: { type: Array, required: false, default: () => [] }
    },

    data: function () {
        return {};
    },

    mounted() {
        // The key to making the Vue object available to the datatable
        // is to run the Setup on $nextTick after mounting.
        // This allows the Vue to finish its construction/initiliaztion too.
        // Then we connect the datatable api object to the vue instance in
        // the DataTable's "initComplete" event (passed in via the "config" prop)
        this.$nextTick(this.setupDataTable);
    },

    methods: {
        refreshTableData() {
            $(this.$refs.table).DataTable().clear().rows.add(this.tableData).draw(false);
        },

        setupDataTable() {
            const internalConfig = Object.assign({}, this.config);
            const externalInitComplete = this.config.initComplete && typeof (this.config.initComplete) === 'function'
                ? this.config.initComplete
                : null;

            internalConfig.initComplete = function (settings, json) {
                if (externalInitComplete && typeof (externalInitComplete) === 'function') {
                    externalInitComplete(settings, json, this);
                }
            }

            $(this.$refs.table).DataTable(internalConfig);

            if (this.tableData && this.tableData.length > 0) {
                this.refreshTableData();
            }
        }
    },

    watch: {
        tableData(newData, oldData) {
            this.$nextTick(this.refreshTableData);
        }
    },
    template: `<table ref="table" class="table table-striped table-sm"></table>`
});