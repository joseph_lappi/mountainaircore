﻿(function ($) {
    function FAQEditor() {
        var $this = this;

        function initialize() {
            $('#Answer').summernote({
                focus: true,
                height: 150,
                codemirror: {
                    theme: 'united'
                }
            });
        }

        $this.init = function () {
            initialize();
        }
    }
    $(function () {
        var self = new FAQEditor();
        self.init();
    })
}(jQuery))