﻿var table;
var editor;

$(document).ready(function () {
    editor = new $.fn.dataTable.Editor({
        ajax: "/api/UserAdmin/UpdateUser",
        //ajax: function (method, url, data, success, error) {
        //    $.ajax({
        //        type: "POST",
        //        url: "/api/UserAdmin/UpdateUser",
        //        data: data,
        //        dataType: "json",
        //        success: function (json) {
        //            console.log('Success', json);
        //            success(json);
        //            return json;
        //        },
        //        error: function (xhr, error, thrown) {
        //            console.log(thrown);
        //        }
        //    });
        //},
        table: "#userTable",
        idSrc: "id",
        fields: [
            { label: "First Name", name: "firstName" },
            { label: "Last Name", name: "lastName" },
            { label: "Username", name: "userName" },
            {
                label: "Email", name: "email",
                attr: {
                    type: "email"
                }
            },
            {
                label: "Phone Number", name: "phoneNumber",
                attr: {
                    type: "phone"
                }
            },
            {
                label: "New Password",
                name: "newPasswordValue",
                type: "password"
            },
            {
                label: "Labor Rate", name: "defaultLaborRate",
                attr: {
                    type: "number"
                }
            },
            {
                label: "Profit Margin", name: "defaultProfitMargin",
                attr: {
                    type: "number"
                }
            },
            {
                label: "Sales Tax", name: "defaultSalesTax",
                attr: {
                    type: "number"
                }
            },
            {
                label: "Active",
                name: "active",
                type: "radio",
                options: [
                    { label: 'Yes', value: true },
                    { label: 'No', value: false }
                ]
            }
        ]
    });

    //$('#userTable').on('dblclick', 'tbody tr', function (e) {
    //    editor.bubble(this, {
    //        buttons: { label: '&gt;', fn: function () { this.submit(); } }
    //    });
    //});

    bindTable();
});

function bindTable() {
    table = $('#userTable').DataTable({
        colReorder: true,
        dom: '<<<"row"<"col"B><"col"f>>><t><"row"<"col"l><"col"i><"col"p>>>',
        proccessing: true,
        ajax: "/api/UserAdmin/GetUsers",
        select: true,
        columns: [
            { title: "Last Name", data: "lastName" },
            { title: "First Name", data: "firstName" },
            { title: "User Name", data: "userName" },
            { title: "Email", data: "email" },
            { title: "Phone", data: "phoneNumber" },
            { title: "Labor Rate", data: "defaultLaborRate" },
            { title: "Profit Margin", data: "defaultProfitMargin" },
            { title: "Sales Tax", data: "defaultSalesTax" },
            {
                title: "Active",
                data: "active",
                render: ma.web.datatables.renders.booleanRender
            }
        ],
        buttons: [
            {
                extend: "create",
                editor: editor
            },
            {
                extend: "edit",
                editor: editor
            }
        ]
    });
}