﻿var editor; // use a global for the submit and return data rendering in the examples
var table;
const boolRender = ma.web.datatables.renders.booleanRender;
const boolOptions = ma.web.datatables.selectoptions.booleanOptions;

$(document).ready(function () {
    editor = new $.fn.dataTable.Editor({
        ajax: function (method, url, data, success, error) {
            $.ajax({
                type: 'POST',
                url: '/api/Categories/UpdateData',
                data: data,
                dataType: 'json',
                success: function (json) {
                    success(json);
                },
                error: function (xhr, error, thrown) {
                    //error(xhr, error, thrown);
                    console.log(thrown);
                }
            });
        },
        table: '#tblCategories',
        idSrc: 'categoryId',
        fields: [
            {
                label: 'Category Name', 
                name: 'categoryName'
            },
            { 
                label: 'Sort Order', 
                name: 'sortOrder', 
                attr: { type: 'number' }
            },
            {
                label: 'Heating',
                name: 'visibleHeating',
                type: 'select',
                options: boolOptions
            },
            {
                label: 'Cooling',
                name: 'visibleCooling',
                type: 'select',
                options: boolOptions
            },
            {
                label: 'Incentive',
                name: 'visibleIncentive',
                type: 'select',
                options: boolOptions
            },
            {
                label: 'No Markup',
                name: 'visibleNoMarkup',
                type: 'select',
                options: boolOptions
            },
            {
                label: 'Left Side Estimate',
                name: 'leftSideEstimate',
                type: 'select',
                options: boolOptions
            },
            {
                label: 'Right Side Estimate',
                name: 'rightSideEstimate',
                type: 'select',
                options: boolOptions
            },
            {
                label: 'Material',
                name: 'materialCategory',
                type: 'select',
                options: boolOptions
            }
        ]
    });

    //editor.on('open', function () {
    //    var catId = $("#ddlCategories").val();
    //    this.set('categoryId', catId);
    //});

    loadData();
});

//$('#tblCategories').on('dblclick', 'tbody td:not(:first-child)', function (e) {
//    editor.inline(this, {
//        buttons: { label: '&gt;', fn: function () { this.submit(); } }
//    });
//});

// Add event listener for opening and closing details
$('#tblCategories tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
    }
    else {
        // Open this row
        row.child(format(row.data())).show();
        tr.addClass('shown');
    }
});

$('#ddlCategories').on('change', function () {
    loadData();
});

function loadData() {
    var url = '/api/Categories/GetData';

    if (table !== undefined) {
        table.ajax.url(url).load();
        return;
    }

    table = $('#tblCategories').DataTable({
        dom: '<<<"row"<"col"B><"col"f>>><t><"row"<"col"l><"col"i><"col"p>>>',
        proccessing: true,
        colReorder: true,
        ajax: {
            url: url,
            type: 'GET',
            //dataSrc: function (json) {
            //    console.log(json.data);
            //    return json.data;
            //}
        },
        columns: [
            { title: 'CategoryName', data: 'categoryName' },
            { title: 'Sort Order', data: 'sortOrder' },
            {
                title: 'Heating',
                data: 'visibleHeating',
                render: boolRender
            },
            {
                title: 'Cooling',
                data: 'visibleCooling',
                render: boolRender
            },
            {
                title: 'Incentive',
                data: 'visibleIncentive',
                render: boolRender
            },
            {
                title: 'No Markup',
                data: 'visibleNoMarkup',
                render: boolRender
            },
            {
                title: 'Left Side',
                data: 'leftSideEstimate',
                render: boolRender
            },
            {
                title: 'Right Side',
                data: 'rightSideEstimate',
                render: boolRender
            },
            {
                title: 'Material',
                data: 'materialCategory',
                render: boolRender
            }
        ],
        select: {
            style: 'os',
            selector: 'tr'
        },
        buttons: [
            {
                extend: 'create',
                editor: editor
            },
            {
                extend: 'edit',
                editor: editor
            },
            //{
            //    extend: "remove",
            //    editor: editor
            //},
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ]
    });
}
