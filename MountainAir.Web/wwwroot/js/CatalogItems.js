﻿var editor; // use a global for the submit and return data rendering in the examples
var table;
const currencyRender = $.fn.dataTable.render.number(',', '.', 2, '$'); 

$(document).ready(function () {
    editor = new $.fn.dataTable.Editor({
        ajax: function (method, url, data, success, error) {
            $.ajax({
                type: "POST",
                url: "/api/CatalogItems/UpdateData",
                data: data,
                dataType: "json",
                success: function (json) {
                    success(json);
                },
                error: function (xhr, error, thrown) {
                    //error(xhr, error, thrown);
                    console.log(thrown);
                }
            });
        },
        table: "#tblCatalogItems",
        idSrc: "catalogItemId",
        fields: [
            { label: "Category", name: "categoryId",
                type: "select",
                options: categoryOptions
            },
            { label: "Catalog Number", name: "catalogNum" },
            { label: "Model Number", name: "modelNum" },
            { label: "Description", name: "descr" },
            { label: "Customer Description", name: "customerDesc" },
            { label: "Price", name: "price", attr: { type: "number" } },
            { label: "Labor", name: "labor", attr: { type: "number" } },
            {
                label: "Active",
                name: "active",
                type: "select",
                options: [
                    { label: 'Yes', value: true },
                    { label: 'No', value: false }
                ]
            }
        ]
    });
    
    loadData();
});

//$('#tblCatalogItems').on('dblclick', 'tbody td:not(:first-child)', function (e) {
//    editor.inline(this, {
//        buttons: { label: '&gt;', fn: function () { this.submit(); } }
//    });
//});

// Add event listener for opening and closing details
$('#tblCatalogItems tbody').on('click', 'td.details-control', function () {
    var tr = $(this).closest('tr');
    var row = table.row(tr);

    if (row.child.isShown()) {
        // This row is already open - close it
        row.child.hide();
        tr.removeClass('shown');
    }
    else {
        // Open this row
        row.child(format(row.data())).show();
        tr.addClass('shown');
    }
});

$('#ddlCategories').on('change', function () {
    loadData();
});

function loadData() {
    const categoryId = $('#ddlCategories').val();

    var url = `/api/CatalogItems/GetData/${categoryId}`;

    if (table !== undefined) {
        table.ajax.url(url).load();
        return;
    }

    table = $('#tblCatalogItems').DataTable({
        dom: '<<<"row"<"col"B><"col"f>>><t><"row"<"col"l><"col"i><"col"p>>>',
        proccessing: true,
        colReorder: true,
        ajax: {
            url: url,
            type: 'GET'
            //dataSrc: function(json) {
            //    console.log('Received', json.data);
            //    return json.data;
            //}
        },
        columns: [
            {
                "className": 'select-checkbox',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {
                title: "Category", 
                data: 'categoryName', 
                width: '15%'
            },
            { title: "Catalog Nummber", data: 'catalogNum', width: '10%' },
            { title: "Model Number", data: 'modelNum', width: '10%' },
            { title: "Description", data: 'descr' },
            { title: "Customer Description", data: 'customerDesc' },
            {
                title: "Price",
                data: 'price',
                render: currencyRender
            },
            { title: "Labor", data: 'labor' },
            {
                title: "Active",
                data: 'active',
                render: ma.web.datatables.renders.booleanRender
            }
        ],
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        rowGroup: {
            dataSrc: 'categoryName'
        },
        buttons: [
            {
                extend: "create",
                editor: editor
            },
            {
                extend: "edit",
                editor: editor
            },
            {
                extend: "remove",
                editor: editor
            },
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [0, ':visible']
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            },
            'colvis'
        ]
    });
}
