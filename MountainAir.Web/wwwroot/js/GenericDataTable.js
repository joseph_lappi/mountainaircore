﻿var editor; // use a global for the submit and return data rendering in the examples
var table;
const tableSelector = `#${config.tableId}`;

$(document).ready(function () {
    console.log(config);
    editor = new $.fn.dataTable.Editor({
        ajax: function (method, url, data, success, error) {
            $.ajax({
                type: "POST",
                url: config.editorUrl,
                data: data,
                dataType: "json",
                success: function (json) {
                    success(json);
                },
                error: function (xhr, error, thrown) {
                    //error(xhr, error, thrown);
                    console.log(thrown);
                }
            });
        },
        table: tableSelector,
        idSrc: config.idField,
        fields: config.editorColumns
    });

    table = $(tableSelector).DataTable({
        dom: '<<<"row"<"col"B><"col"f>>><t><"row"<"col"l><"col"i><"col"p>>>',
        ajax: {
            url: config.displayUrl,
            type: 'GET'
        },
        columns: config.displayColumns,
        select: {
            style: 'os',
            selector: 'td:first-child'
        },
        buttons: [
            {
                extend: "create",
                editor: editor
            },
            {
                extend: "edit",
                editor: editor
            },
            {
                extend: "remove",
                editor: editor
            },
            {
                extend: 'excelHtml5',
                exportOptions: { columns: ':visible' }
            },
            'colvis'
        ]
    });

    $(tableSelector).on('dblclick', 'tbody td:not(:first-child)',
        function (e) {
            editor.bubble(this,
                {
                    buttons: { label: '&gt;', fn: function () { this.submit(); } }
                });
        });
});