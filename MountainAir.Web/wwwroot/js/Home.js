﻿var table;

$(document).ready(function () {
    loadData();
});

const checkboxColumnRender = {
    display: function (data, type, row) {
        return (data === true) ? '<i class="fas fa-lg fa-check-circle btn-outline-success text-success"></i>' : '<i class="fas fa-lg fa-times-circle btn-outline-danger"></i>';
    },
    sort: function (data, type, row) { return data === true ? 1 : 0; }
};

const dateRender = {
    display: function (data, type, row) {
        if (data) {
            return luxon.DateTime.fromISO(data).toFormat('MM/dd/yyyy');
        }
        return '';
    },
    sort: function (data, type, row) {
        if (data) {
            return luxon.DateTime.fromISO(data).toFormat('yyyy/MM/dd');
        }
        return '';
    }
}

const dateTimeRender = {
    display: function (data, type, row) {
        if (data) {
            const dt = luxon.DateTime.fromISO(data);
            return dt.toFormat('MM/dd/yyyy hh:mm a');
        }
        return '';
    },

    sort: function (data, type, row) {
        if (data) {
            return luxon.DateTime.fromISO(data).toFormat('yyyy/MM/dd hh:mm a');
        }
        return '';
    }
}

function loadData() {
    var url = '/api/GetEstimateList';

    if (table !== undefined) {
        table.ajax.url(url).load();
        return;
    }

    table = $(document).ready(function () {
        $('#estimateTable').DataTable({
            dom: '<<<"row"<"col"B><"col"f>>><t><"row"<"col"l><"col"i><"col"p>>>',
            ajax: {
                url: '/api/GetEstimateList',
                type: 'GET',
                contentType: "application/json; charset=UTF-8",
                dataType: "json"
                //dataSrc: function (json) {
                //    console.log('Received', json);
                //    return json.data;
                //}
            },
            select: false,
            columns: [
                {
                    data: null,
                    title: "",
                    render: function (data, type, row) {
                        const estimateId = row['estimateId'];
                        const links = `<div class="h-100 d-flex justify-content-between">` +
                            `<a href="/Estimates/Edit/${estimateId}" data-toggle="tooltip" data-placement="top" title="Edit Estimate"><i class="fas fa-edit"></i></a> ` +
                            `<a href="/Estimates/Attachments/${estimateId}" data-toggle="tooltip" data-placement="top" title="Attachments"><i class="fas fa-paperclip"></i></a> ` +
                            `<a href="/WorkOrders/InstallPreview/${estimateId}" data-toggle="tooltip" data-placement="top" title="Install Workorder" target="_blank"><i class="fas fa-hard-hat"></i></a> ` +
                            `<a href="/WorkOrders/OfficePreview/${estimateId}" data-toggle="tooltip" data-placement="top" title="Office Workorder" target="_blank"><i class="fas fa-building"></i></a>` +
                            `<a href="/Estimates/AhriAddendum/${estimateId}" data-toggle="tooltip" data-placement="top" title="AHRI Addendum" target="_blank"><i class="fas fa-temperature-high"></i></a>` + 
                            `</div>`;
                        return links;
                    }
                },
                {
                    data: 'customerFullName',
                    title: "Customer",
                },
                { data: "companyName", title: "Company" },
                {
                    data: "createdDate",
                    title: "Created Date",
                    render: dateTimeRender
                },
                {
                    data: "assignedFullName",
                    title: "Assigned To"
                },
                {
                    data: "updatedDate",
                    title: "Last Modified",
                    render: dateTimeRender
                }
            ],
        });
    });
}
