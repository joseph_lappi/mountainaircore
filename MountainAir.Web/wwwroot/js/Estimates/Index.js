﻿var table;
$(document).ready(function () {
    getData();

    $('#estimateTable tbody').on('click', 'tr', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
        }
        else {
            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
        }
    });
});

$('select').on('change', function () {
    getData();
});

function formatCustomer(row) {
    var customerName = row['customerFullName'];
    var company = row['companyName'];

    if (customerName === '') {
        return company;
    }

    if (company !== '') {
        return customerName + ' - ' + company;
    }

    return customerName;
}

function getData() {
    const dateRender = {
	    display(d) {
		    const value = !d
			    ? ''
			    : luxon.DateTime.fromISO(d).toLocal().toFormat('MM/dd/yyyy hh:mm a');
		    return value;
	    },
	    sort: function (data, type, row) {
		    const value = !data ? '' : luxon.DateTime.fromISO(data).toLocal().toFormat('yyyy/MM/dd hh:mm a');
		    return value;
	    }
    };

    var active = $('#ddlActive').val();
    var dr = $('#ddlDateRange').val();

    var url = "/api/Estimates/GetEstimates?active=" + active + '&dateRange=' + dr;

    if (table !== undefined) {
        table.ajax.url(url).load();
        return;
    }

    table = new DataTable('#estimateTable', {
        dom: '<<<"row"<"col"B><"col"f>>><t><"row"<"col"l><"col"i><"col"p>>>',
        ajax: {
            url: url,
            type: 'GET',
            contentType: "application/json; charset=UTF-8",
            dataType: "json",
            dataSrc: function (json) {
                //console.log(json.data);
                return json.data;
            }
        },
        colReorder: true,
        //createdRow: function (row, data, dataIndex) {
        //    var ready = data['validationNotes'] === 'Ready';

        //    if (ready === true) {
        //        $(row).addClass('approved');
        //    } else {
        //        $(row).addClass('notapproved');
        //    }
        //},
        initComplete: function () {
            var tableFooter = $('#estimateTable tfoot');
            var footerRow = $('<tr />').appendTo(tableFooter);
            this.api().columns().every(function () {
                var cell = $('<th />').appendTo(footerRow);
                var column = this;
                var index = column.index();

                if (index == 4 || index === 5 || index === 6) {
                    var select = $('<select><option value="">All</option></select>')
                        .appendTo(cell)
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
                            column.search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                        });

                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>');
                    });
                } 
            });
        },
        columns: [
            {
                data: null,
                title: 'Estimate',
                render: function (data, type, row) {
                    const estimateId = row['estimateId'];
                    const links = `<div class="p-1"><a href="/Estimates/Edit/${estimateId}" data-toggle="tooltip" data-placement="top" title="Edit Estimate"><i class="fas fa-edit"></i></a> ` +
                        `<a href="/Estimates/AhriAddendum/${estimateId}" data-toggle="tooltip" data-placement="top" title="AHRI Addendum"><i class="fas fa-temperature-high"></i></a></div>`;
                    return links;
                }
            },
            {
                data: null,
                title: "Customer",
                render: function (data, type, row) {
                    //return row['customerFullName'] + ' ' + row['customerLastName'];
                    return formatCustomer(row);
                }
            },
            {
                data: 'createdDate',
                title: "Created",
                render: dateRender
            },
            {
                data: 'updatedDate',
                title: "Updated",
                render: dateRender
            },
            {
                data: "assignedFullName",
                title: "Assigned To"
            },
            {
                data: "createdByFullName",
                title: "Created By"
            },
            {
                data: "validationNotes",
                title: "Ready",
                render: function (data, type, row) {
                    if (data === 'Ready') {
                        return `<span class="approved"><strong>${data}</strong></span>`;
                    }
                    return `<span class="notapproved"><strong>${data}</strong></span>`;
                }
            }
        ],
        "columnDefs": [
            { "type": "datetime-moment", targets: 2 }
        ],
        buttons: [
            {
                extend: 'copyHtml5',
                exportOptions: {
                    columns: [0, ':visible']
                }
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            }
        ]
    });
}

$(function () {
    $('#estimateTable').contextMenu({
        selector: 'td',
        callback: function (key, options) {
            var data = table.row($(this).parents('tr')).data();

            var estimateid = data.estimateId;

            var url;

            switch (key) {
                case 'edit':
                    url = '/Estimates/Edit/';
                    break;
                case 'report':
                    url = '/Estimates/Report/';
                    break;
                case 'attachments':
                    url = '/Estimates/Attachments/';
                    break;
                case 'install':
                    url = '/WorkOrders/InstallPreview/';
                    break;
                case 'office':
                    url = '/WorkOrders/OfficePreview/';
                    break;
                case 'ahri':
                    url = '/Estimates/AhriAddendum/';
                    break;
            }
            url = url + estimateid;

            window.location = url;
        },
        items: {
            "edit": { name: "Edit" },
            "report": { name: "Report" },
            "attachments": { name: "Attachments" },
            "ahri": { name: "AHRI Addendum" },
            "sep1": "---------",
            "install": { name: "Install Workorder" },
            "office": { name: "Office Workorder" }
        }
    });
});