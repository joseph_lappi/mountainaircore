﻿var ma = ma || {};
ma.web = ma.web || {};

if (ma.web.datatables) {
	console.warn('ma.web.datatables already exists; not overwriting.');
}

//======================================
/*
 *			REQUIRES:
 *		luxon
 *		site.js
 */
//======================================

(function () {
    ma.web.datatables = {
		selectoptions: {
			booleanOptions:  [
                { label: 'Yes', value: true },
                { label: 'No', value: false }
            ]
        },
		renders: {
			//currencyRender: {
			//	display: ToUSDCurrencyFormat
			//},

			dateRender: {
				display(d) { return !d ? '' : luxon.DateTime.fromISO(d).toFormat('MM/dd/yyyy'); },
                sort: function (data, type, row) {
                    const value = !data ? '' : luxon.DateTime.fromISO(data).toFormat('yyyy/MM/dd');
                    return value;
                }
			},

			isoDateTimeRender: {
				display(d) { return !d ? '' : luxon.DateTime.fromISO(d).toFormat('MM/dd/yyyy hh:mm a') },
                sort: function (data, type, row) {
                    const value = !data ? '' : luxon.DateTime.fromISO(data).toFormat('yyyy/MM/dd hh:mm a');
                    return value;
                }
			},

			isoLocalDateTimeRender: {
				display(d) {
					const value = !d
						? ''
						: luxon.DateTime.fromISO(d, { zone: 'utc' }).toLocal().toFormat('MM/dd/yyyy hh:mm a');
					console.log('Original', d, 'Updated', value);
					return value;
				},
                sort: function (data, type, row) {
                    const value = !data ? '' : luxon.DateTime.fromISO(data, { zone: 'utc' }).toLocal().toFormat('yyyy/MM/dd hh:mm a');
                    return value;
                }
			},

			decimalRender: {
				display(d) { return ToPrecision(d, 2); }
			},

			getDecimalRender(decimalPrecision) {
				return {
					display(d) { return ToPrecision(d, decimalPrecision); }
				}
			},

			getCurrencyRender() {
				return {
					display(d) {
                        return $.fn.dataTable.render.number(',', '.', 2, '$');
                    }
                }
            },
			
            booleanRender: {
                display(d) {
                    return (d === true) ? '<i style="color:green" class="fa-solid fa-circle-check"></i>' : '<i style="color:red" class="fa-solid fa-circle-xmark"></i>';
                },
                sort: function (data) { return data === true ? 1 : 0; }
            },

			activeRender: {
                display(d) {
                    return (d === true) ? '<i style="color:green" class="fa-solid fa-circle-check"></i>' : '<i style="color:red" class="fa-solid fa-circle-xmark"></i>';
                },
                sort: function (data) { return data === true ? 1 : 0; }
            },

			// Handles a JSON string or an Array in the form of [{ key: '', value: '' }, ...]
			jsonKeyValuePairs: {
				display: function (d) {
					const data = typeof (d) === 'string' ? JSON.parse(d) : d;
					return data.length > 0
						? `<div class="tags-container">${data.map(kvp => `<div class="kvp"><span class="key">${kvp.key}:</span> <span class="value">${kvp.value}</span></div>`).sort().join('')}</div>`
						: [''];
				},

				filter: function (d) {
					const data = typeof (d) === 'string' ? JSON.parse(d) : d;
					return data.length > 0 ? data.map(kvp => `${kvp.key}:${kvp.value}`).sort() : [''];
				},

				sort: function (d) {
					const data = typeof (d) === 'string' ? JSON.parse(d) : d;
					return data.length > 0 ? data.map(kvp => `${kvp.key}:${kvp.value}`).sort() : [''];
				}
			},

			// Handles a JSON string or Object in any form.
			objectKeyValuePairs: {
				display: function (d) {
					const data = typeof (d) === 'string' ? JSON.parse(d) : d;

					if (!data) {
						return [''];
					}

					const kvps = [];
					for (let prop in data) {
						if (Object.prototype.hasOwnProperty.call(data, prop)) {
							kvps.push(`<div class="kvp-container"><div class="kvp"><span class="key">${prop}:</span> <span class="value">${data[prop].toString()}</span></div></div>`);
						}
					}

					return kvps.join('');
				},

				filter: function (d) {
					const data = typeof (d) === 'string' ? JSON.parse(d) : d;
					if (!data) {
						return '';
					}

					const kvps = [];
					for (let prop in data) {
						if (Object.prototype.hasOwnProperty.call(data, prop)) {
							kvps.push(`<div class="kvp-container"><div class="kvp"><span class="key">${prop}:</span> <span class="value">${data[prop].toString()}</span></div></div>`);
						}
					}

					return kvps.join(';');
				},

				sort: function (d) {
					const data = typeof (d) === 'string' ? JSON.parse(d) : d;
					if (!data) {
						return '';
					}

					const kvps = [];
					for (let prop in data) {
						if (Object.prototype.hasOwnProperty.call(data, prop)) {
							kvps.push(`<div class="kvp-container"><div class="kvp"><span class="key">${prop}:</span> <span class="value">${data[prop].toString()}</span></div></div>`);
						}
					}

					return kvps.join(';');
				}
			},

			percentRender: {
				display(d) { return ToPercent(d / 100); }
			},

			decimalPercentRender: {
				display(d) { return ToPercent(d); }
			}
		}
	}
})();