﻿// Write your JavaScript code.

$(document).ready(function () {

});

function formatPhone(data) {
    if (data === null || data === '')
        return '';

    var tPhone = data.toString().trim();

    if (tPhone.includes('(') || tPhone.includes(')'))
        return tPhone;

    tPhone = '(' + tPhone.substring(0, 3) + ') ' + tPhone.substring(3, 6) + '-' + tPhone.substring(6, 10);
    return tPhone;
}

$(function () {
    // Initialize numeric spinner input boxes
    //$(".numeric-spinner").spinedit();
    // Initialize modal dialog
    // attach modal-container bootstrap attributes to links with .modal-link class.
    // when a link is clicked with these attributes, bootstrap will display the href content in a modal dialog.
    $('body').on('click', '.modal-link', function (e) {
        e.preventDefault();
        $(this).attr('data-target', '#modal-container');
        $(this).attr('data-toggle', 'modal');
    });
    // Attach listener to .modal-close-btn's so that when the button is pressed the modal dialog disappears
    $('body').on('click', '.modal-close-btn', function () {
        $('#modal-container').modal('hide');
    });
    //clear modal cache, so that new content can be loaded
    $('#modal-container').on('hidden.bs.modal', function () {
        $(this).removeData('bs.modal');
    });
    $('#CancelModal').on('click', function () {
        return false;
    });
});

function AddStatusMessage(statusLevel, statusMessage, autoDismiss = true) {
    var level;
    switch (statusLevel.toLowerCase()) {
        case 'success':
            level = "success";
            break;
        case 'warn':
        case 'warning':
            level = "warning";
            break;
        case 'error':
        case 'danger':
            level = "danger";
            break;
        default:
            level = "info";
            break;
    }

    var alt = $('<div />').addClass('.inner-message alert alert-' + level + ' alert-dismissible fade show').html(statusMessage);
    var button = $('<button type="button" />').addClass('close').attr('data-dismiss', 'alert').attr('aria-label', 'Close');
    var span = $('<span />').attr('aria-hidden', 'true').html('&times;');
    button.append(span);
    alt.append(button);
    $('#alertArea').append(alt);
    if (autoDismiss) {
        $("#alertArea").fadeTo(4000, 500).slideUp(500, function () {
            $("#alertArea").slideUp(500);
            $("#alertArea").empty();
        });
    }
}

var waitingDialog = waitingDialog || (function ($) {
    return {
        show: function (message, options) {
            // Opening dialog
            $('#busyModal').modal();
        },
        hide: function () {
            //Closes dialog
            $('#busyModal').modal('hide');
        }
    };
})(jQuery);
