﻿var table;

$(document).ready(function () {
    LoadData();
});

function LoadData() {
    var url = '/api/Packages/GetPackageMaterials/' + packageItemId;

    if (table !== undefined) {
        table.ajax.url(url).load();
        return;
    }

    table = $('#tblCatalogItems').DataTable({
        proccessing: true,
        colReorder: true,
        ajax: {
            url: url,
            type: 'GET'
        },
        columns: [
            {
                title: "",
                data: null,
                render: function (data, type, row) {
                    var id = row['packageMaterialId'];
                    return '<div class="d-flex justify-content-between"><a href="/Packages/EditPackageMaterial/' + id + '"><i class="fa fa-edit btn-outline-warning"></i></a>' +
                        '<a href="/Packages/DeletePackageMaterial/' + id + '"><i class="fa fa-trash btn-outline-danger"></i></a></div>';
                }
            },
            {
                title: "Catalog Number",
                data: 'catalogNumber'
            },
            {
                title: "Model Number",
                data: "modelNumber"
            },
            {
                title: "Description",
                data: "description"
            },
            {
                title: "Price",
                data: "price",
                render: $.fn.dataTable.render.number(',', '.', 2, '$')
            },
            {
                title: "Labor",
                data: "labor",
                render: $.fn.dataTable.render.number(',', '.', 2)
            },
            {
                title: "Active",
                data: "active",
                render: function (data, type, row) {
                    return data === true ? "Yes" : "No";
                }
            }
        ]
    });
}

$('.custom-select').on('change', function () {
    LoadData();
});

$('#tblCatalogItems tbody').on('click', 'button', function () {
    var id = $(this).attr('data-id');

    var packageId = $('#hidPackageId').val();
    var group = $('#ddlCategories :selected').parent().attr('label');

    var url = '/api/Packages/AddItemToPackage';

    var data = JSON.stringify({
        'PackageId': packageId,
        'CatalogItemId': id,
        'Group': group
    });

    var posting = $.post({
        type: "POST",
        url: url,
        data: data,
        contentType: "application/json; charset=utf-8",
        dataType: 'json'
    });

    // Put the results in a div
    posting.done(function (d) {
        //console.log(d);
        if (d.isError) {
            AddStatusMessage('Error', d.message);
        } else {
            AddStatusMessage('Success', d.message);
        }
    });
});
