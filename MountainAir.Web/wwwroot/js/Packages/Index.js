﻿var table;
var editor;

$(document).ready(function () {
    editor = new $.fn.dataTable.Editor({
        ajax: function (method, url, data, success, error) {
            $.ajax({
                type: "POST",
                url: "/api/Packages/UpdatePackageData",
                data: data,
                dataType: "json",
                success: function (json) {
                    success(json);
                },
                error: function (xhr, error, thrown) {
                    //error(xhr, error, thrown);
                    console.log(thrown);
                }
            });
        },
        stateSave: true,
        stateSaveCallback: function (settings, data) {
            localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
        },
        stateLoadCallback: function (settings) {
            return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
        },
        table: "#tblPackages",
        idSrc: "packageId",
        fields: [
            { label: "Package Name", name: "packageName" },
            { label: "Profit Margin", name: "profitMargin", attr: { type: "number" } },
            { label: "AHRI Rating", name: "ahriRating" },
            {
                label: "Active",
                name: "active",
                type: "radio",
                options: [
                    { label: 'Yes', value: true },
                    { label: 'No', value: false }
                ]
            }
        ]
    });

    $('#tblPackages').on( 'dblclick', 'tbody td:not(:first-child)', function (e) {
        editor.inline( this, {
            onBlur: 'submit'
        });
    });

    LoadData();
});

function LoadData() {
    table = $('#tblPackages').DataTable({
        dom: '<<<"row"<"col"B><"col"f>>><t><"row"<"col"l><"col"i><"col"p>>>',
        proccessing: true,
        colReorder: true,
        ajax: {
            url: '/api/Packages/GetPackages',
            type: 'GET'
        },
        stateSave: true,
        stateSaveCallback: function (settings, data) {
            localStorage.setItem('DataTables_' + settings.sInstance, JSON.stringify(data));
        },
        stateLoadCallback: function (settings) {
            return JSON.parse(localStorage.getItem('DataTables_' + settings.sInstance));
        },
        columns: [
            {
                title: 'Items',
                data: null,
                render: function (data, type, row) {
                    const ctl = `<div><a href="/Packages/EditPackage/${row['packageId']}"><i class="fas fa-edit btn-outline-warning"></i></a>`+
                        `<a href="/Packages/DeletePackage/${row['packageId']}"><i class="fas fa-trash btn-outline-danger"></i></div>`;

                    return ctl;
                }
            },
            {
                title: "Package Name",
                data: 'packageName'
            },
            {
                title: "Active",
                data: "active",
                render: ma.web.datatables.renders.activeRender
            },
            {
                title: "AHRI Rating",
                data: "ahriRating"
            },
            {
                title: "Profit Margin",
                data: 'profitMargin',
                render: $.fn.dataTable.render.number( ',', '.', 2, '$' )
            }
        ],
        select: {
            style: 'os',
            selector: 'td:not(:first-child)',
            blurable: true
        },
        buttons: [
            {
                extend: "create",
                editor: editor
            },
            {
                extend: "edit",
                editor: editor
            },
            {
                extend: "remove",
                editor: editor
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            }
        ]
    });
}

