﻿var table;

$(document).ready(function () {
    LoadData();
});

function LoadData() {
    const value = $("#ddlCategories").val();
    console.log("Value: ", value);

    var url = `/api/Packages/GetCatalogItems/${value}`;

    console.log("Url: ", url);
    
    if (table !== undefined) {
        table.ajax.url(url).load();
        return;
    }

    table = $('#tblCatalogItems').DataTable({
        proccessing: true,
        colReorder: true,
        ajax: {
            url: url,
            type: 'GET'
        },
        columns: [
            {
                title: "Add",
                data: null,
                render: function (data, type, row) {
                    return '<button class="btn btn-primary" data-id="' + row['catalogItemId'] + '"><i class="fa fa-fw fa-plus-circle"></i></button>';
                }
            },
            {
                title: "Catalog Number",
                data: 'catalogNum'
            },
            {
                title: "Model Number",
                data: "modelNum"
            },
            {
                title: "Description",
                data: "descr"
            },
            {
                title: "Price",
                data: "price",
                render: $.fn.dataTable.render.number( ',', '.', 2, '$' )
            },
            { 
                title: "Labor", 
                data: "labor",
                render: $.fn.dataTable.render.number( ',', '.', 2 )
            }
        ]
    });
}

$('.custom-select').on('change', function () {
    LoadData();
});

$('#tblCatalogItems tbody').on('click', 'button', function () {
    var id = $(this).attr('data-id');

    var packageId = $('#hidPackageItemId').val();
    //var group = $('#ddlCategories :selected').parent().attr('label');

    var url = '/api/Packages/AddMaterialToPackage'; 

    var data = JSON.stringify({
        'PackageItemId': packageId,
        'CatalogItemId': id
    });

    console.log(data);

    var posting = $.post({
      type: "POST",
      url: url,
      data: data,
      contentType: "application/json; charset=utf-8",
      dataType: 'json'
    });

  // Put the results in a div
  posting.done(function( d ) {
    //console.log(d);
    if(d.isError) {
        AddStatusMessage('Error', d.message);
    } else {
        AddStatusMessage('Success', d.message);
    }
  });
});
