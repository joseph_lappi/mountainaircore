﻿var table;
var editor;

$(document).ready(function () {
    editor = new $.fn.dataTable.Editor({
        ajax: function (method, url, data, success, error) {
            $.ajax({
                type: "POST",
                url: "/api/Packages/UpdatePackageItemData",
                data: data,
                dataType: "json",
                success: function (json) {
                    success(json);
                },
                error: function (xhr, error, thrown) {
                    //error(xhr, error, thrown);
                    console.log(thrown);
                }
            });
        },
        table: "#tblPackageItems",
        idSrc: "packageItemId",
        fields: [
            {
                label: "Catalog Item", 
                name: "catalogItemId", 
                type: "select",
                options: catalogItems
            },
            { label: "Price", name: "price", attr: { type: "number" } },
            { label: "Labor", name: "labor", attr: { type: "number" } },
            { label: "Add-On Price", name: "additionalPrice", attr: { type: "number" } }
        ]
    });

    $('#tblPackages').on( 'dblclick', 'tbody td:not(:first-child)', function (e) {
        editor.inline( this, {
            onBlur: 'submit'
        });
    });

    editor.on("open",
        function(e, mode, action) {
            //Make Dialog Bigger - Bootstrap
            $(".modal-dialog").addClass("modal-lg");
        });

    LoadData();
});

function LoadData() {
    table = $('#tblPackageItems').DataTable({
        dom: '<<<"row"<"col"B><"col"f>>><t><"row"<"col"l><"col"i><"col"p>>>',
        proccessing: true,
        colReorder: true,
        ajax: {
            url: `/api/Packages/GetPackageItems/` + packageId,
            type: 'GET'
        },
        columns: [
            {
                title: 'Material',
                data: null,
                render: function (data, type, row) {
                    return `<a href="/Packages/PackageMaterial/${row['packageItemId']}"><i class="fas fa-info-circle"></i></a>`;
                }
            },
            { title: "Catalog Number", data: 'catalogItem.catalogNum' },
            { title: "Model Number", data: 'catalogItem.modelNum' },
            { title: "Description", data: 'catalogItem.descr' },
            {
                title: "Active",
                data: "catalogItem.active",
                render: ma.web.datatables.renders.activeRender
            },
            {
                title: "Catalog Price",
                data: 'catalogItem.price',
                render: $.fn.dataTable.render.number( ',', '.', 2, '$' )
            },
            {
                title: "Catalog Labor",
                data: 'catalogItem.labor',
                render: $.fn.dataTable.render.number( ',', '.', 0 )
            },
            {
                title: "Package Price",
                data: 'price',
                render: $.fn.dataTable.render.number( ',', '.', 2, '$' )
            },
            {
                title: "Package Price",
                data: 'price',
                render: $.fn.dataTable.render.number( ',', '.', 2, '$' )
            },
            {
                title: "Add-On Price",
                data: 'additionalPrice',
                render: $.fn.dataTable.render.number( ',', '.', 2, '$' )
            }
        ],
        select: {
            style: 'os',
            selector: 'td:not(:first-child)',
            blurable: true
        },
        buttons: [
            {
                extend: "create",
                editor: editor
            },
            {
                extend: "edit",
                editor: editor
            },
            {
                extend: "remove",
                editor: editor
            },
            {
                extend: 'excelHtml5',
                exportOptions: {
                    columns: ':visible'
                }
            }
        ]
    });
}

