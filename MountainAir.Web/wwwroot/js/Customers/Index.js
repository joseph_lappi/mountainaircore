﻿var table;
var editor;

$(document).ready(function () {
    editor = new $.fn.dataTable.Editor({
        ajax: "/api/Customers/UpdateCustomer",
        table: "#tblCustomers",
        idSrc: "customerId",
        fields: [
            { label: "First Name", name: "firstName" },
            { label: "Last Name", name: "lastName" },
            { label: "Company", name: "companyName" },
            {
                label: "Email", name: "email",
                attr: {
                    type: "email"
                }
            },
            {
                label: "Phone Number 1", name: "phoneNumber1",
                attr: {
                    type: "phone"
                }
            },
            {
                label: "Phone Number 2", name: "phoneNumber2",
                attr: {
                    type: "phone"
                }
            }
        ]
    });

    table = $('#tblCustomers').DataTable({
        dom: '<<<"row"<"col"B><"col"f>>><t><"row"<"col"l><"col"i><"col"p>>>',
        ajax: {
            url: '/api/Customers/GetCustomers',
            type: 'GET',
            contentType: "application/json; charset=UTF-8",
            dataType: "json",
            //dataSrc: function (json) {
            //    console.log('Received', json);
            //    return json.data;
            //}
        },
        select: {
            style: 'single',
            'selector': 'td:not(:first-child)'
        },
        columns: [
            {
                "className": 'dt-control',
                "orderable": false,
                "data": null,
                "defaultContent": ''
            },
            {
                data: 'displayName',
                title: "Customer",
            },
            { data: "companyName", title: "Company" },
            {
                data: "phoneNumber1",
                title: "Phone Number 1",
                defaultContent: "",
                render: function (data, type, row) {
                    return formatPhone(data);
                }
            },
            {
                data: "phoneNumber2",
                title: "Phone Number 2",
                render: function (data, type, row) {
                    return formatPhone(data);
                }
            }
        ],
        buttons: [
            {
                extend: "create",
                editor: editor
            },
            {
                extend: "edit",
                editor: editor
            },
            {
                extend: "remove",
                editor: editor
            }
        ]
    });

    function getCustomerAddresses(row) {
        var rowData = row.data(); 
        var id = rowData["customerId"];
        var newTable = $(`<table id="tbl_${id}" class="table table-sm" />`);
        
        row.child(newTable).show();

        var siteEditor = new $.fn.dataTable.Editor({
            ajax: {
                url: '/api/Customers/UpdateAddress',
                data: function (d) {
                    console.log('Child Data: ', d);
                    d.customerId = rowData.customerId;
                }
            },
            table: newTable,
            idSrc: "addressId",
            fields: [
                { label: 'Line 1:', name: 'addressLine1' },
                { label: 'Line 2:', name: 'addressLine2' },
                { label: 'City:', name: 'city' },
                {
                    label: 'State:',
                    name: 'state',
                    type: 'select',
                    options: [
                        { label: 'Alabama', value: 'AL' },
                        { label: 'Alaska', value: 'AK' },
                        { label: 'Arizona', value: 'AZ' },
                        { label: 'Arkansas', value: 'AR' },
                        { label: 'California', value: 'CA' },
                        { label: 'Colorado', value: 'CO' },
                        { label: 'Connecticut', value: 'CT' },
                        { label: 'Delaware', value: 'DE' },
                        { label: 'Florida', value: 'FL' },
                        { label: 'Georgia', value: 'GA' },
                        { label: 'Hawaii', value: 'HI' },
                        { label: 'Idaho', value: 'ID' },
                        { label: 'Illinois', value: 'IL' },
                        { label: 'Indiana', value: 'IN' },
                        { label: 'Iowa', value: 'IA' },
                        { label: 'Kansas', value: 'KS' },
                        { label: 'Kentucky', value: 'KY' },
                        { label: 'Louisiana', value: 'LA' },
                        { label: 'Maine', value: 'MA' },
                        { label: 'Maryland', value: 'MD' },
                        { label: 'Massachusetts', value: 'MA' },
                        { label: 'Michigan', value: 'MI' },
                        { label: 'Minnesota', value: 'MN' },
                        { label: 'Mississippi', value: 'MI' },
                        { label: 'Missouri', value: 'MS' },
                        { label: 'Montana', value: 'MT' },
                        { label: 'Nebraska', value: 'NE' },
                        { label: 'Nevada', value: 'NV' },
                        { label: 'New Hampshire', value: 'NH' },
                        { label: 'New Jersey', value: 'NJ' },
                        { label: 'New Mexico', value: 'NM' },
                        { label: 'New York', value: 'NY' },
                        { label: 'North Carolina', value: 'NC' },
                        { label: 'North Dakota', value: 'ND' },
                        { label: 'Ohio', value: 'OH' },
                        { label: 'Oklahoma', value: 'OK' },
                        { label: 'Oregon', value: 'OR' },
                        { label: 'Pennsylvania', value: 'PA' },
                        { label: 'Rhode Island', value: 'RI' },
                        { label: 'South Carolina', value: 'SC' },
                        { label: 'South Dakota', value: 'SD' },
                        { label: 'Tennessee', value: 'TN' },
                        { label: 'Texas', value: 'TX' },
                        { label: 'Utah', value: 'UT' },
                        { label: 'Vermont', value: 'VT' },
                        { label: 'Virginia', value: 'VA' },
                        { label: 'Washington', value: 'WA' },
                        { label: 'West Virginia', value: 'WV' },
                        { label: 'Wisconsin', value: 'WI' },
                        { label: 'Wyoming', value: 'WY' }
                    ]
                },
                { label: 'Zip:', name: 'zip' },
                {
                    label: 'CustomerId:',
                    name: 'customerId',
                    type: 'hidden'
                },
            ]
        });

        siteEditor.on('submitSuccess', function (e, json, data, action) {
            console.log('json: ', json);
            console.log('data: ', data);
        });

        newTable.DataTable({
            dom: 'Bt',
            ajax: {
                url: `/api/Customers/GetCustomerAddresses/${id}`,
                type: 'GET',
                contentType: "application/json; charset=UTF-8",
                dataType: "json"
            },
            select: {
                style: 'single'
            },
            columns: [
                { data: 'addressLine1', title: "Line 1" },
                { data: 'addressLine2', title: "Line 2" },
                { data: 'city', title: "City" },
                { data: 'state', title: "State" },
                { data: 'zip', title: "Zip" }

            ],
            buttons: [
                {
                    extend: "create",
                    editor: siteEditor
                },
                {
                    extend: "edit",
                    editor: siteEditor
                },
                {
                    extend: "remove",
                    editor: siteEditor
                }
            ]
        });

    }
    
    function destroyChild(row) {
        var table = $("table", row.child());
        table.detach();
        table.DataTable().destroy();

        // And then hide the row
        row.child.hide();
    }

    // Add event listener for opening and closing details
    $('#tblCustomers tbody').on('click', 'td.dt-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row(tr);

        if (row.child.isShown()) {
            // This row is already open - close it
            destroyChild(row);
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            getCustomerAddresses(row);
            tr.addClass('shown');
        }
    });
});