﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using MountainAir.Web.Models.DataTables;
using Newtonsoft.Json;

namespace MountainAir.Web.Models;

public static class ModelExtensions
{
    public static IEnumerable<SelectListItem> ToListItems<T>(this List<T> items, Func<T, string> pickValue, Func<T, string> pickText, Func<T, bool> pickSelected = null, string defaultValue = "", string defaultText = "-- Select --")
    {
        yield return new SelectListItem
        {
            Value = defaultValue,
            Text = defaultText
        };

        if (items != null)
        {
            foreach (var item in items)
            {
                yield return new SelectListItem
                {
                    Value = pickValue(item),
                    Text = pickText(item),
                    Selected = pickSelected != null && pickSelected(item)
                };
            }
        }
    }
}

public static class DictionaryExtentions
{
    public static string ToSelectListJson<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, bool insertBlank = true, string blankLabel = "", string blankValue = "")
    {
        var list = ToSelectListOptions(dictionary, insertBlank, blankLabel, blankValue);

        return JsonConvert.SerializeObject(list);
    }

    public static List<SelectColumnOption> ToSelectListOptions<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, bool insertBlank = true, string blankLabel = "", string blankValue = "")
    {
        var list = dictionary.Select(i => new SelectColumnOption
        {
            Label = $"{i.Value}",
            Value = i.Key
        }).ToList();

        if (insertBlank)
        {
            list.Insert(0, new SelectColumnOption
            {
                Label = blankLabel,
                Value = blankValue
            });
        }

        return list;
    }

    public static SelectList ToSelectList<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, bool insertBlank = true, string blankLabel = "", string blankValue = "", object selectedValue = null)
    {
        var list = dictionary.Select(i => new SelectListItem
        {
            Text = $"{i.Value}",
            Value = $"{i.Key}"
        }).ToList();

        if (insertBlank)
        {
            list.Insert(0, new SelectListItem
            {
                Text = blankLabel,
                Value = blankValue
            });
        }

        return new SelectList(list, nameof(SelectListItem.Value), nameof(SelectListItem.Text), selectedValue);
    }
}