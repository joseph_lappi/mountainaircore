﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using MountainAir.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MountainAir.Web.Extensions;

public static class TempDataExtensions
{
    private const string TEMPDATA_STATUSMESSAGES_KEY = "Master:StatusMessages";

    public static List<StatusMessageViewModel> GetStatusMessages(this ITempDataDictionary tempData)
    {
        if (tempData == null)
        {
            throw new ArgumentNullException(nameof(tempData), $"{nameof(tempData)} is required");
        }

        tempData.TryGetValue(TEMPDATA_STATUSMESSAGES_KEY, out object jsonListObj);
        string jsonList = jsonListObj?.ToString();

        List<StatusMessageViewModel> statusMessages = string.IsNullOrWhiteSpace(jsonList)
            ? new List<StatusMessageViewModel>()
            : JsonConvert.DeserializeObject<List<StatusMessageViewModel>>(jsonList);

        return statusMessages;
    }

    public static StatusMessageViewModel AddStatusMessage(this ITempDataDictionary tempData, StatusLevels level, string message)
    {
        List<StatusMessageViewModel> currentStatusMessages = tempData.GetStatusMessages();
        StatusMessageViewModel statusMessage = new StatusMessageViewModel { Level = level, Message = message };
        currentStatusMessages.Add(statusMessage);
        tempData[TEMPDATA_STATUSMESSAGES_KEY] = JsonConvert.SerializeObject(currentStatusMessages);

        return statusMessage;
    }
}