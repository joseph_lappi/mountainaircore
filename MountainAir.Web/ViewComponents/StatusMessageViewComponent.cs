﻿using Microsoft.AspNetCore.Mvc;
using MountainAir.Web.Extensions;
using MountainAir.Web.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MountainAir.Web.ViewComponents;

public class StatusMessageViewComponent : ViewComponent
{
    public async Task<IViewComponentResult> InvokeAsync()
    {
        return await Task.Run(() =>
        {
            List<StatusMessageViewModel> model = this.TempData.GetStatusMessages();
            return View(model);
        });
    }
}