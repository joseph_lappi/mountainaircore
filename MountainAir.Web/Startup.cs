﻿using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MountainAir.Common.AzureStorage;
using MountainAir.Common.Configuration;
using MountainAir.Common.Data;
using MountainAir.Common.Security;
using MountainAir.Common.Services;
using NLog;
using NLog.Layouts;
using NLog.Targets;
using System;
using System.Globalization;

namespace MountainAir.Web;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        var connString = Configuration.GetConnectionString("DefaultConnection");

        AppSettings.Shared = new AppSettings()
        {
            DefaultConnectionString = connString
        };

        services.AddDbContext<ApplicationDbContext>(options =>
            options.UseSqlServer(connString));
            
        CreateNLog(connString);

        // Add authentication services
        services.AddAuthentication(options =>
        {
            options.DefaultAuthenticateScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = CookieAuthenticationDefaults.AuthenticationScheme;
        });
        //.AddMicrosoftAccount(microsoftOptions =>
        //{
        //    microsoftOptions.ClientId = Configuration["Authentication:Microsoft:ClientId"];
        //    microsoftOptions.ClientSecret = Configuration["Authentication:Microsoft:ClientSecret"];
        //});

        services.AddControllersWithViews()
            .AddNewtonsoftJson()
            .AddRazorRuntimeCompilation(); 

        services.AddSession();
        services.AddIdentity<ApplicationUser, ApplicationRole>()
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddDefaultTokenProviders();
        // Add application services.
        services
            .AddTransient<IEmailSender, SmtpEmailSender>()
            .AddTransient<IAzureBlobManager, AzureStorageManager>()
            .AddTransient<ISignatureManager, SignatureManager>()
            .AddSingleton<IHttpContextAccessor, HttpContextAccessor>();


        //Set the default authentication policy to require users to be authenticated. 
        //You can opt out of authentication at the controller or action method with the[AllowAnonymous] 
        //attribute. With this approach, any new controllers added will automatically require authentication,
        //which is safer than relying on new controllers to include the[Authorize] attribute.
        services.AddMvc(config =>
        {
            var policy = new AuthorizationPolicyBuilder()
                .RequireAuthenticatedUser()
                .Build();
            config.Filters.Add(new AuthorizeFilter(policy));
        });

        services.AddLocalization();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/Home/Error");
            app.UseHsts();
        }

        app.UseHttpsRedirection();
        app.UseStaticFiles();
        app.UseAuthentication();
        app.UseSession();
        app.UseRouting();
        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapDefaultControllerRoute();
        });

        var supportedCultures = new[]{
            new CultureInfo("en-US")
        };

        app.UseRequestLocalization(new RequestLocalizationOptions
        {
            DefaultRequestCulture = new RequestCulture("en-US"),
            SupportedCultures = supportedCultures,
            FallBackToParentCultures = false
        });
        CultureInfo.DefaultThreadCurrentCulture = CultureInfo.CreateSpecificCulture("en-US");
    }

    private static void CreateNLog(string connStr)
    {
        var config = new NLog.Config.LoggingConfiguration();

        var sqlLog = new NLog.Targets.DatabaseTarget
        {
            ConnectionString = connStr,
            Name = "logSql",
            CommandText = "spLogEntryWeb",
            CommandType = System.Data.CommandType.StoredProcedure
        };

        sqlLog.Parameters.Add(new DatabaseParameterInfo("@logged", Layout.FromString("${date}")));
        sqlLog.Parameters.Add(new DatabaseParameterInfo("@level", Layout.FromString("${level}")));
        sqlLog.Parameters.Add(new DatabaseParameterInfo("@message", Layout.FromString("${message}")));
        sqlLog.Parameters.Add(new DatabaseParameterInfo("@callSite", Layout.FromString("${callsite}")));
        sqlLog.Parameters.Add(new DatabaseParameterInfo("@exception", Layout.FromString("${exception:tostring}")));

#if DEBUG
        var logConsole = new NLog.Targets.ConsoleTarget
        {
            Name = "logconsole"
        };

        config.LoggingRules.Add(new NLog.Config.LoggingRule("*", NLog.LogLevel.Info, logConsole));

        LogManager.ThrowExceptions = true;
        LogManager.ThrowConfigExceptions = true;
#endif

        config.LoggingRules.Add(new NLog.Config.LoggingRule("*", NLog.LogLevel.Warn, sqlLog));

        LogManager.Configuration = config;
    }
}