﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using System;
using System.IO;

namespace MountainAir.Web;

public class Program
{
    static void ConfigConfiguration(IConfigurationBuilder configurationBuilder)
    {
        configurationBuilder.SetBasePath(Directory.GetCurrentDirectory());

        //var cfg = configurationBuilder.Build();
        //string hostName = cfg["KeyVaultName"];
        //string clientId = cfg["KeyVaultClientId"];
        //string clientSecret = cfg["KeyVaultClientSecret"];
            
        ////System.Diagnostics.Trace.TraceError($"KeyVaultName: {hostName}");

        ////if (string.IsNullOrEmpty(hostName))
        ////    throw new ArgumentNullException(nameof(hostName));

        ////if (string.IsNullOrEmpty(clientId))
        ////    throw new ArgumentNullException(nameof(clientId));

        ////if (string.IsNullOrEmpty(clientSecret))
        ////    throw new ArgumentNullException(nameof(clientSecret));

        //string url = $"https://{hostName}.vault.azure.net/";

        //configurationBuilder.AddAzureKeyVault(url, clientId, clientSecret);
    }

    public static void Main(string[] args)
    {
        CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration(ConfigConfiguration)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
}