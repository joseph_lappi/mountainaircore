﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;

namespace MountainAir.Web.Views.Administration;

public static class AdminNavPages
{
    public const string ActivePageKey = "AdminActivePage";
    public const string CatalogItems = "CatalogItems";
    public const string Categories = "Categories";
    public const string DropDownValues = "DropDownValues";
    public const string Features = "Features";
    public const string FileUpload = "FileUpload";
    public const string Financing = "Financing";
    public const string Index = "Index";
    public const string PaymentMethods = "PaymentMethods";

    public static string CatalogItemsNavClass(ViewContext viewContext) => PageNavClass(viewContext, CatalogItems);
    public static string CategoriesNavClass(ViewContext viewContext) => PageNavClass(viewContext, Categories);
    public static string DropDownValuesNavClass(ViewContext viewContext) => PageNavClass(viewContext, DropDownValues);
    public static string FeaturesNavClass(ViewContext viewContext) => PageNavClass(viewContext, Features);
    public static string FileUploadNavClass(ViewContext viewContext) => PageNavClass(viewContext, FileUpload);
    public static string FinancingNavClass(ViewContext viewContext) => PageNavClass(viewContext, Financing);
    public static string IndexNavClass(ViewContext viewContext) => PageNavClass(viewContext, Index);
    public static string PaymentMethodsNavClass(ViewContext viewContext) => PageNavClass(viewContext, PaymentMethods);

    public static string PageNavClass(ViewContext viewContext, string page)
    {
        var activePage = viewContext.ViewData[ActivePageKey] as string;
        return string.Equals(activePage, page, StringComparison.OrdinalIgnoreCase) ? "active" : null;
    }

    public static void AddActivePage(ViewDataDictionary viewData, string activePage)
    {
        viewData[ActivePageKey] = activePage;
    }
}