﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using System;
using MountainAir.Common.Data.Models;

namespace MountainAir.Web.Views.Shared;

public class NavPages
{
	public const string ActivePageKey = nameof(NavPages);

	public enum Pages
	{
		Home,
		Campaigns,
		Customers,
		Estimates,
		RoleAdmin,
		UserAdmin,
		Categories,
		CatalogItems,
		Features,
		Financing,
		PaymentMethods,
		Packages,
		WarrantyTypes,
		FileUpload,
		Reports,
		EstimateEquipmentReport,
        Locations
    }
    
	public static string PageNavClass(ViewContext viewContext, Pages page)
	{
		var activePage = viewContext.ViewData[ActivePageKey] as string;

		if (Enum.TryParse(activePage, out Pages pg))
		{
			return page == pg ? "active" : null;
		}
		return null;
	}

	public static void AddActivePage(ViewDataDictionary viewData, Pages activePage)
	{
		viewData[ActivePageKey] = activePage.ToString();
	}
}