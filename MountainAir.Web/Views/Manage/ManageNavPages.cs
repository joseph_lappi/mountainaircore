﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace MountainAir.Web.Views.Manage;

public static class ManageNavPages
{
    public const string ActivePageKey = nameof(ManageNavPages);

    public enum Pages
    {
        TwoFactorAuthentication,
        ExternalLogins,
        ChangePassword,
        Index
    }
    
    public static string PageNavClass(ViewContext viewContext, Pages page)
    {
        var activePage = viewContext.ViewData[ActivePageKey] as string;

        if (Enum.TryParse(activePage, out Pages pg))
        {
            return page == pg ? "active" : null;
        }
        return null;
    }

    public static void AddActivePage(this ViewDataDictionary viewData, Pages activePage)
    {
        viewData[ActivePageKey] = activePage.ToString();
    }
}