﻿using System;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace MountainAir.Web.Views.Reports;

public static class ReportsNavPages
{
    public static string ActivePageKey => "ActivePage";

    public static string Index => "Index";

    public static string EstimateEquipmentReport => "EstimateEquipmentReport";

    public static string IndexNavClass(ViewContext viewContext) => PageNavClass(viewContext, Index);

    public static string EstimateEquipmentReportNavClass(ViewContext viewContext) => PageNavClass(viewContext, EstimateEquipmentReport);

    public static string PageNavClass(ViewContext viewContext, string page)
    {
        var activePage = viewContext.ViewData["ActivePage"] as string;
        return string.Equals(activePage, page, StringComparison.OrdinalIgnoreCase) ? "active" : null;
    }

    public static void AddActivePage(this ViewDataDictionary viewData, string activePage) => viewData[ActivePageKey] = activePage;
}