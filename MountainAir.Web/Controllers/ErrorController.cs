﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace MountainAir.Web.Controllers;

[AllowAnonymous]
public class ErrorController : Controller
{
    [AllowAnonymous]
    public IActionResult Error()
    {
        Response.StatusCode = 503;

        return View();
    }
}