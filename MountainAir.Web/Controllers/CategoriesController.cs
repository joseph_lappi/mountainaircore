﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using MountainAir.Web.Models.DataTables;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MountainAir.Web.Controllers;

[RoleAuthorize(Roles.Administrators, Roles.CanViewCatalog)]
public class CategoriesController : BaseController
{
    public CategoriesController(ApplicationDbContext applicationDbContext, ILogger<CategoriesController> logger)
        : base(applicationDbContext)
    {
    }


    public IActionResult Index()
    {
        return View("~/Views/Administration/Categories.cshtml");
    }

    [HttpGet, Route("api/[Controller]/[Action]")]
    public JsonResult GetData()
    {
        var data = AppContext.Categories.ToList();

        return Json(new
        {
            data
        });
    }

    [HttpPost]
    [HttpPut]
    [HttpDelete]
    [Route("api/[Controller]/[Action]")]
    public JsonResult UpdateData(UpdateModel model)
    {
        switch (model.Action)
        {
            case UpdateActions.Create:
                return InsertData(model.Data);
            case UpdateActions.Edit:
                return UpdateData(model.Data);
            case UpdateActions.Remove:
                return DeleteData(model.Data);
            default:
                throw new NotImplementedException("Invalid Action");
        }
    }

    public JsonResult InsertData(DataBag data)
    {
        var inserted = new List<Category>();

        var keys = data.Keys;

        foreach (var key in keys)
        {
            var d = data[key];

            var row = new Category();

            FillObjectFromDictionary(data, key, row);

            AppContext.Add(row);
            AppContext.SaveChanges();
            inserted.Add(row);
        }

        return Json(new { data = inserted });
    }

    public JsonResult UpdateData(DataBag data)
    {
        var keys = data.Keys;

        var updated = new List<Category>();

        foreach (var key in keys)
        {
            var row = AppContext.Categories.FirstOrDefault(i => i.CategoryId == int.Parse(key));

            if (row == null)
                continue;

            var d = data[key];

            FillObjectFromDictionary(data, key, row);

            AppContext.Update(row);

            AppContext.SaveChanges();

            updated.Add(row);
        }

        return Json(new { data = updated });
    }

    public JsonResult DeleteData(DataBag data)
    {
        var retval = new List<Category>();
        var keys = data.Keys;

        foreach (var key in keys)
        {
            var id = int.Parse(key);

            var row = AppContext.Categories.FirstOrDefault(i => i.CategoryId == id);

            if (row != null)
            {
                retval.Add(row);

                AppContext.Remove(row);

                AppContext.SaveChanges();
            }
        }

        return Json(new { data = retval });
    }
}