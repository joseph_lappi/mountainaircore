﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using MountainAir.Web.Models.DataTables;

namespace MountainAir.Web.Controllers;

[RoleAuthorize(Roles.Administrators, Roles.CanViewCatalog)]
public class LocationController : DataTablesBaseController<Location>
{
	public LocationController(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
	{
	}

	[RoleAuthorize(Roles.Administrators, Roles.CanViewCatalog)]
	public IActionResult Index()
	{
		var model = new EditorViewModel<Location>
		{
			DisplayColumns = new List<DisplayColumn>
			{
				new CheckBoxSelectColumn(),
				new DisplayColumn<Location>(t => t.Code),
				new DisplayColumn<Location>(t => t.Description)
			},
			EditorColumns = new List<EditorColumn>
			{
				new EditorColumn<Location>(t => t.Code),
				new EditorColumn<Location>(t => t.Description)
			}
		};
		return View("~/Views/Administration/Location.cshtml", model);
	}
}