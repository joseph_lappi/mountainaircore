﻿using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.AzureStorage;
using MountainAir.Common.Data;
using NLog;
using System;
using System.Threading.Tasks;

namespace MountainAir.Web.Controllers;

public class AttachmentsController : BaseController
{
    private readonly ILogger _logger = LogManager.GetCurrentClassLogger();
    private readonly IAzureBlobManager _azureStorageManager;

    public AttachmentsController(
        IAzureBlobManager azureStorageManager,
        ApplicationDbContext applicationDbContext) 
        : base(applicationDbContext)
    {
        _azureStorageManager = azureStorageManager;
    }

    public async Task<IActionResult> Download(Guid estimateId, string fileName)
    {
        try
        {
            fileName = fileName.Replace($"{estimateId}/", "");

            var response = await _azureStorageManager.GetEstimateFile(estimateId, fileName);

            if (response == null || response.GetRawResponse().IsError)
            {
                return BadRequest();
            }
                
            return File(response.Value.Content, response.Value.ContentType);
        }
        catch (Exception ex)
        {
            _logger.Error(ex, $"Error downloading attachment for estimate {estimateId} - {fileName}");
        }

        return BadRequest();
    }
}