﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MountainAir.Common.Data;
using MountainAir.Common.Security;
using MountainAir.Web.Models.RoleAdmin;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MountainAir.Web.Controllers;

[RoleAuthorize(Roles.Administrators)]
public class RoleAdminController : BaseController
{
    private readonly RoleManager<ApplicationRole> _roleManager;
    private readonly UserManager<ApplicationUser> _userManager;

    public RoleAdminController(ApplicationDbContext applicationDbContext, RoleManager<ApplicationRole> roleManager, UserManager<ApplicationUser> userManager) : base(applicationDbContext)
    {
        _roleManager = roleManager;
        _userManager = userManager;
    }

    public IActionResult Index()
    {
        var roles = _roleManager.Roles.OrderBy(r => r.Name).ToArray();

        var model = new RoleAdminViewModel
        {
            Roles = roles.Select(r => new RoleModel(r)).ToArray(),
        };

        return View(model);
    }

    public IActionResult Edit(string id)
    {
        var role = _roleManager.FindByIdAsync(id).Result;

        if (role == null)
        {
            return NotFound();
        }

        var assigned = _userManager.GetUsersInRoleAsync(role.Name).Result.Select(u => u.Id).ToList();

        var users = _userManager.Users.Select(u => new UserModel(u)
        {
            Assigned = assigned.Contains(u.Id)
        });

        var model = new RoleModel(role, users);

        return View(model);
    }

    [HttpPost, Route("api/RoleAdmin/SaveRights")]
    public async Task<IActionResult> SaveRights([FromBody] JsonValues values)
    {
        try
        {
            var user = await _userManager.FindByIdAsync(values.UserId);
            var role = await _roleManager.FindByIdAsync(values.RoleId);
            bool isAssigned = await _userManager.IsInRoleAsync(user, role.Name); 

            IdentityResult result;
            string message = null;

            if (isAssigned)
            {
                result = await _userManager.RemoveFromRoleAsync(user, role.Name);
                message = "User removed from role";
            }
            else
            {
                result = await _userManager.AddToRoleAsync(user, role.Name);
                message = "User added to role";
            }

            if (result.Succeeded)
                return Json(new { Message = message });
            else
                return Json(new { Errors = result.Errors.Select(e => e.Description) });
        }
        catch (Exception ex)
        {
            return Json(new { ex.Message });
        }
    }

    protected override void Dispose(bool disposing)
    {
        if (disposing)
        {
            _userManager.Dispose();
        }
        base.Dispose(disposing);
    }

    public class JsonValues
    {
        public string RoleId { get; set; }
        public string UserId { get; set; }
    }
}