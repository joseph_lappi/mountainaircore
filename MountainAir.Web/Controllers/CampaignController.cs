﻿using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using MountainAir.Web.Models.DataTables;
using System.Collections.Generic;

namespace MountainAir.Web.Controllers;

[RoleAuthorize(Roles.Administrators, Roles.CanViewCatalog)]
public class CampaignController : DataTablesBaseController<Campaign>
{
    public CampaignController(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
    {
    }
        
    [RoleAuthorize(Roles.Administrators, Roles.CanViewCatalog)]
    public IActionResult Index()
    {
        var model = new EditorViewModel<Campaign>
        {
            DisplayColumns = new List<DisplayColumn>
            {
                new CheckBoxSelectColumn(),
                new DisplayColumn<Campaign>(f => f.Name),
                new DisplayColumn<Campaign>(f => f.Description),
                new DisplayColumn<Campaign>(f => f.StartDate),
                new DisplayColumn<Campaign>(f => f.EndDate),
                new BooleanDisplayColumn<Campaign>(f => f.Active)
            },
            EditorColumns = new List<EditorColumn>
            {
                new EditorColumn<Campaign>(f => f.Name),
                new EditorColumn<Campaign>(f => f.Description),
                new EditorColumn<Campaign>(f => f.StartDate),
                new EditorColumn<Campaign>(f => f.EndDate),
                new BooleanSelectColumn<Campaign>(f => f.Active)
            }
        };
        return View("~/Views/Administration/Campaigns.cshtml", model);
    }
}