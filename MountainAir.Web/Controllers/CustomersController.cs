﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using MountainAir.Web.Models;
using MountainAir.Web.Models.DataTables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MountainAir.Web.Controllers;

/// <summary>
/// 
/// </summary>
[RoleAuthorize(Roles.Administrators, Roles.CanViewCustomers)]
public class CustomersController : BaseController
{
    public CustomersController(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [RoleAuthorize(Roles.Administrators, Roles.CanViewCustomers)]
    public IActionResult Index()
    {
        return View();
    }

    #region Customers 

    [HttpGet, Route("api/[Controller]/[Action]")]
    public async Task<JsonResult> GetCustomers()
    {
        var data = await AppContext.Customers
            .AsNoTracking()
            .ToListAsync();

        data = data.OrderBy(c => c.DisplayName).ThenBy(c => c.CompanyName).ToList();

        return Json(new JsonResponseModel(data));
    }
                
    [HttpPost, Route("api/[Controller]/[Action]")]
    public async Task<JsonResult> UpdateCustomer(UpdateModel model)
    {
        try
        {

            var retval = new List<Customer>();

            switch (model.Action)
            {
                case UpdateActions.Create:
                    retval = await CreateCustomer(model.Data);
                    break;
                case UpdateActions.Remove:
                    await RemoveCustomer(model.Data);
                    break;
                case UpdateActions.Edit:
                    retval = await EditCustomer(model.Data);
                    break;
            }

            return Json(new JsonResponseModel(retval));
        }
        catch (Exception ex)
        {
            return Json(new JsonResponseModel()
            {
                Error = ex.Message
            });
        }
    }

    private async Task<List<Customer>> EditCustomer(DataBag data)
    {
        var retval = new List<Customer>();

        foreach (var key in data.Keys)
        {
            var customerId = new Guid(key);

            var customer = await AppContext.Customers.FirstOrDefaultAsync(c => c.CustomerId == customerId);

            if(customer == null)
                continue;

            data.FillObject(key, customer);

            AppContext.Customers.Update(customer);

            retval.Add(customer);
        }

        await AppContext.SaveChangesAsync();

        return retval;
    }

    private Task RemoveCustomer(DataBag data)
    {
        throw new ApplicationException("Delete not allowed at this time");
    }

    private async Task<List<Customer>> CreateCustomer(DataBag data)
    {
        var retval = new List<Customer>();

        foreach (var key in data.Keys)
        {
            var model = data[key];

            var newRow = new Customer();

            data.FillObject(key, newRow);

            newRow.CustomerId = Guid.NewGuid();

            AppContext.Customers.Add(newRow);
                
            retval.Add(newRow);
        }

        await AppContext.SaveChangesAsync();

        return retval;
    }

    #endregion

    #region Addresses

    [HttpGet, Route("api/[Controller]/[Action]/{id}")]
    public async Task<JsonResult> GetCustomerAddresses(Guid id)
    {
        var data = await AppContext.Addresses
            .Where(a => a.CustomerId == id)
            .ToListAsync();

        return Json(new JsonResponseModel(data));
    }

    [HttpPost, Route("api/[Controller]/[Action]")]
    public async Task<JsonResult> UpdateAddress(UpdateModel model)
    {
        try
        {

            var retval = new List<Address>();

            switch (model.Action)
            {
                case UpdateActions.Create:
                    retval = await CreateAddress(model.Data);
                    break;
                case UpdateActions.Remove:
                    await RemoveAddress(model.Data);
                    break;
                case UpdateActions.Edit:
                    retval = await EditAddress(model.Data);
                    break;
            }

            return Json(new JsonResponseModel(retval));
        }
        catch (Exception ex)
        {
            return Json(new JsonResponseModel()
            {
                Error = ex.Message
            });
        }
    }
        
    private async Task<List<Address>> EditAddress(DataBag data)
    {
        var retval = new List<Address>();

        foreach (var key in data.Keys)
        {
            var addressId = new Guid(key);

            var row = await AppContext.Addresses.FirstOrDefaultAsync(c => c.AddressId == addressId);

            if(row == null)
                continue;

            data.FillObject(key, row);

            AppContext.Addresses.Update(row);

            retval.Add(row);
        }

        await AppContext.SaveChangesAsync();

        return retval;
    }

    private Task RemoveAddress(DataBag data)
    {
        throw new ApplicationException("Delete not allowed at this time");
    }

    private async Task<List<Address>> CreateAddress(DataBag data)
    {
        var retval = new List<Address>();

        foreach (var key in data.Keys)
        {
            var model = data[key];

            var newRow = new Address();

            data.FillObject(key, newRow);

            if(model.TryGetValue("customerId", out string customerId))
                newRow.CustomerId = new Guid(customerId);

            AppContext.Addresses.Add(newRow);
                
            retval.Add(newRow);
        }

        await AppContext.SaveChangesAsync();

        return retval;
    }

    #endregion
}