﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using MountainAir.Common.Utility;
using MountainAir.Web.Extensions;
using MountainAir.Web.Models.DataTables;
using System.Collections.Generic;
using System.Linq;

namespace MountainAir.Web.Controllers;

[RoleAuthorize(Roles.Administrators, Roles.CanViewCatalog)]
public class FeatureController : DataTablesBaseController<Feature>
{
    public FeatureController(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
    {
    }

    [RoleAuthorize(Roles.Administrators, Roles.CanViewCatalog)]
    public IActionResult Index()
    {
        var model = new EditorViewModel<Feature>
        {
            DisplayColumns = new List<DisplayColumn>
            {
                new CheckBoxSelectColumn(),
                new DisplayColumn<Feature>(t => t.FeatureText),
                new DisplayColumn<Feature>(t => t.Cost),
                new DisplayColumn<Feature>(t => t.Labor),
                new DisplayColumn<Feature>(t => t.SortOrder)
            },
            EditorColumns = new List<EditorColumn>
            {
                new EditorColumn<Feature>(t => t.FeatureText) { PlaceHolder = "Enter feature text." },
                new NumericEditorColumn<Feature>(t => t.Cost),
                new NumericEditorColumn<Feature>(t => t.Labor),
                new NumericEditorColumn<Feature>(t => t.SortOrder)
            }
        };
        return View("~/Views/Administration/Features.cshtml", model);
    }
}