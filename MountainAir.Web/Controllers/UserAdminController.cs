﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Security;
using MountainAir.Web.Models;
using MountainAir.Web.Models.DataTables;
using MountainAir.Web.Models.UserAdmin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace MountainAir.Web.Controllers;

[RoleAuthorize(Roles.Administrators)]
public class UserAdminController : BaseController
{
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly IConfiguration _configuration;

    public UserAdminController(
        IConfiguration configuration,
        ApplicationDbContext applicationDbContext,
        UserManager<ApplicationUser> userManager)
        : base(applicationDbContext)
    {
        _userManager = userManager;
        _configuration = configuration;
    }

    public IActionResult Index()
    {
        return View();
    }

    [HttpGet, Route("api/[Controller]/[Action]")]
    public async Task<JsonResult> GetUsers()
    {
        var users = (await AppContext.Users.ToListAsync()).Select(u => new IndexUserModel(u)).OrderBy(u => u.LastName).ThenBy(u => u.FirstName).ToList();

        return Json(new JsonResponseModel(users));
    }

    [HttpPost, Route("api/[Controller]/[Action]")]
    public async Task<JsonResult> UpdateUser(UpdateModel model)
    {
        try
        {

            var retval = new List<IndexUserModel>();

            switch (model.Action)
            {
                case UpdateActions.Create:
                    retval = await CreateUser(model.Data);
                    break;
                case UpdateActions.Remove:
                    await RemoveUser(model.Data);
                    break;
                case UpdateActions.Edit:
                    retval = await EditUser(model.Data);
                    break;
            }

            return Json(new JsonResponseModel(retval));
        }
        catch (Exception ex)
        {
            return Json(new JsonResponseModel()
            {
                Error = ex.Message
            });
        }
    }

    private async Task<List<IndexUserModel>> EditUser(DataBag data)
    {
        var retval = new List<IndexUserModel>();

        var users = (await AppContext.Users.ToListAsync()).ToDictionary(i => i.Id, i => i);

        foreach (var key in data.Keys)
        {
            var model = data[key];

            var userId = new Guid(key);

            var user = users[userId];

            data.FillObject(key, user);

            var results = await _userManager.UpdateAsync(user);


            if (results.Succeeded)
            {
                if (model.TryGetValue("newPasswordValue", out string newPassword) && !string.IsNullOrWhiteSpace(newPassword))
                {
                    await _userManager.RemovePasswordAsync(user);
                    await _userManager.AddPasswordAsync(user, newPassword);
                }
                retval.Add(new IndexUserModel(user));
            }
            else
            {
                var errors = results.Errors;
                string errorMessage = string.Join(";", errors.Select(e => e.Description));
                throw new ApplicationException(errorMessage);
            }
        }

        return retval;
    }

    private Task RemoveUser(DataBag data)
    {
        return Task.CompletedTask;
    }

    private async Task<List<IndexUserModel>> CreateUser(DataBag data)
    {
        var retval = new List<IndexUserModel>();

        foreach (var key in data.Keys)
        {
            var model = data[key];

            var user = new ApplicationUser();

            data.FillObject(key, user);

            user.AddDate = DateTime.Now;

            if (model.TryGetValue("newPasswordValue", out string newPassword) && !string.IsNullOrWhiteSpace(newPassword))
            {
                newPassword = model["newPasswordValue"];
            }
            else
            {
                newPassword = Guid.NewGuid().ToString();
            }

            var results = await _userManager.CreateAsync(user, newPassword);

            if (results.Succeeded)
            {
                retval.Add(new IndexUserModel(user));
            }
            else
            {
                var errors = results.Errors;
                string errorMessage = string.Join(";", errors.Select(e => e.Description));
                throw new ApplicationException(errorMessage);
            }
        }

        return retval;
    }
}