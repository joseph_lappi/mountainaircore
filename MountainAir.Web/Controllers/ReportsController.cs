﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Web.Models;

namespace MountainAir.Web.Controllers;

public class ReportsController : BaseController
{
    public ReportsController(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
    {
    }

    public IActionResult Index()
    {
        return View();
    }

    public IActionResult EstimateEquipmentReport()
    {
        return View();
    }

    public class EstimateEquipmentReportPostModel
    {
        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }
    }

    [HttpPost, Route("api/[Controller]/[Action]")]
    public async Task<IActionResult> GetEstimateEquipmentReport([FromBody] EstimateEquipmentReportPostModel model)
    {
        try
        {
            var data = await AppContext.EstimateEquipmentReport
                .Where(e => e.Active && e.ReadyToProcess && e.CreatedDate >= model.StartDate && e.CreatedDate <= model.EndDate)
                .Include(e => e.AssignedTo)
                .Include(e => e.CreatedBy)
                .Include(e => e.UpdatedBy)
                .Include(e => e.Customer)
                .ToListAsync();

            return Ok(new JsonResponseModel(data));
        }
        catch (Exception ex)
        {
            return Conflict(new JsonResponseModel(null, ex));
        }
    }
}