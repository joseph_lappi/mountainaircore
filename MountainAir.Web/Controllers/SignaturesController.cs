﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.AzureStorage;
using MountainAir.Common.Data;

namespace MountainAir.Web.Controllers;

public class SignaturesController : BaseController
{
    private readonly ISignatureManager _signatureManager;

    public SignaturesController(ApplicationDbContext applicationDbContext, ISignatureManager signatureManager) : base(applicationDbContext)
    {
        this._signatureManager = signatureManager;
    }

    [AllowAnonymous]
    [HttpGet, Route("[Controller]/[Action]/{id}")]
    public async Task<IActionResult> Get(Guid id)
    {
        try
        {
            var results = await _signatureManager.GetAsync(id);

            return File(results, "image/png");
        }
        catch //(Exception ex)
        {
            return File(Array.Empty<byte>(), "image/png");
        }
    }
}