﻿using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using MountainAir.Web.Models.DataTables;

namespace MountainAir.Web.Controllers;

[RoleAuthorize(Roles.Administrators, Roles.CanViewCatalog)]
public class PaymentMethodController : DataTablesBaseController<PaymentMethod>
{
    public PaymentMethodController(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
    {
    }

    [RoleAuthorize(Roles.Administrators, Roles.CanViewCatalog)]
    public IActionResult Index()
    {
        var model = new EditorViewModel<PaymentMethod>
        {
            DisplayColumns =
            [
	            new CheckBoxSelectColumn(),
	            new DisplayColumn<PaymentMethod>(f => f.Method)
            ],
            EditorColumns = [new EditorColumn<PaymentMethod>(f => f.Method)],
        };
        return View("~/Views/Administration/PaymentMethods.cshtml", model);
    }
}