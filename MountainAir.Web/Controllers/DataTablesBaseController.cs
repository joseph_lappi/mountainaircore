﻿using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.Data;
using MountainAir.Web.Models.DataTables;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MountainAir.Web.Controllers;

public abstract class DataTablesBaseController<T> : BaseController
    where T: class
{
    public DataTablesBaseController(ApplicationDbContext applicationDbContext) 
        : base(applicationDbContext)
    {
    }

    protected List<T> GetDataInternal()
    {
        return AppContext.Set<T>().ToList();
    }

    protected T GetDataItemInternal(string key)
    {
        return AppContext.Find<T>(int.Parse(key));
    }

    [HttpGet, Route("api/[Controller]/[Action]")]
    public JsonResult GetData()
    {
        var data = GetDataInternal();

        return Json(new
        {
            data
        });
    }

    [HttpPost]
    [HttpPut]
    [HttpDelete]
    [Route("api/[Controller]/[Action]")]
    public JsonResult UpdateData(UpdateModel model)
    {
        var list = new List<T>();

        switch (model.Action)
        {
            case UpdateActions.Create:
                list = InsertData(model.Data);
                break;
            case UpdateActions.Edit:
                list = UpdateData(model.Data);
                break;
            case UpdateActions.Remove:
                list = DeleteData(model.Data);
                break;
            default:
                throw new NotImplementedException("Invalid Action");
        }

        return Json(new { data = list });
    }

    protected List<T> InsertData(DataBag data)
    {
        var list = new List<T>();

        foreach (var key in data.Keys)
        {
            var row = Activator.CreateInstance<T>();

            FillObjectFromDictionary(data, key, row);

            AppContext.Add(row);
            AppContext.SaveChanges();
            list.Add(row);
        }

        return list;
    }

    protected List<T> UpdateData(DataBag data)
    {
        var list = new List<T>();

        foreach (var key in data.Keys)
        {
            var row = GetDataItemInternal(key); 

            if (row == null)
                continue;

            FillObjectFromDictionary(data, key, row);

            AppContext.Update(row);
            AppContext.SaveChanges();
            list.Add(row);
        }

        return list;
    }

    protected List<T> DeleteData(DataBag data)
    {
        var list = new List<T>();

        foreach (var key in data.Keys)
        {
            var row = GetDataItemInternal(key);

            if (row == null)
                continue;

            AppContext.Remove(row);

            AppContext.SaveChanges();

            list.Add(row);
        }

        return list;
    }
}