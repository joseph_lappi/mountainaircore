﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using MountainAir.Web.Models;
using MountainAir.Web.Models.WorkOrders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MountainAir.Common.Services;
using NLog;

namespace MountainAir.Web.Controllers;

[RoleAuthorize(Roles.Administrators, Roles.CanViewProposals)]
public class WorkOrdersController : BaseController
{
    private readonly ILogger _logger = LogManager.GetCurrentClassLogger();

    public WorkOrdersController(
        ApplicationDbContext applicationDbContext) 
        : base(applicationDbContext)
    {
    }
        
    public IActionResult Index()
    {
        var workOrders = (from est in AppContext.Estimates
            join cust in AppContext.Customers on est.CustomerId equals cust.CustomerId
            select new WorkOrderListModel
            {
                Estimate = est,
                Customer = cust
            }).ToArray();

        return View(workOrders);
    }

    protected string GetBaseUrl()
    {
        return HttpContext.Request.PathBase;
        //var scheme = HttpContext.Request.Scheme;
        //var authority = HttpContext.Request.PathBase;
        //string url = string.Format("{0}://{1}{2}", scheme, Request.Url.Authority, Url.Content("~"));
        //return url;
    }

    private Customer GetCustomerForWorkOrder(Guid estimateid)
    {
        var customer = (from est in AppContext.Estimates
            join c in AppContext.Customers on est.CustomerId equals c.CustomerId
            where est.EstimateId == estimateid
            select c).FirstOrDefault();
        return customer;
    }
        
    [AllowAnonymous]
    public async Task<IActionResult> OfficePreview(Guid id)
    {
        return await GetOfficeWorkOrderModel(id);
    }
        
    [AllowAnonymous]
    public async Task<IActionResult> InstallPreview(Guid id)
    {
        return await GetInstallWorkOrderModel(id);
    }

    private async Task<IActionResult> GetInstallWorkOrderModel(Guid estimateid)
    {
        var estimate = await AppContext.Estimates
	        .AsNoTracking()
	        .Include(u => u.Customer)
	        .Include(a => a.BillingAddress)
	        .Include(a => a.Address)
	        .FirstOrDefaultAsync(i => i.EstimateId == estimateid);

        if (estimate == null)
            return NotFound();

        decimal totalLabor = 0;
        var options = new List<OptionItem>();

        if (estimate.SelectedOptionId == null)
        {
            AddStatusMessage(StatusLevels.Warn, "Selected Option Is Missing");
            //return View();
        }
        else
        {
            var estOption = await AppContext.EstimateOptions.FirstOrDefaultAsync(i => i.EstimateOptionId == estimate.SelectedOptionId.Value);

            //Heating, Cooling & No Markup Sections
            int[] sections = { 0, 1, 2 };

            options = await GetOptionItems(estOption.EstimateOptionId);

            totalLabor = estOption.LaborTotal.GetValueOrDefault(0);

            foreach (var option in options)
            {
                option.SpecialMaterial = await GetSpecialMaterials(option.OptionItemId);

                var data = await AppContext.SpGetMaterialList(option.OptionItemId);

                option.MaterialList.AddRange(data);
            }
        }

        var materialCategories = await AppContext.Categories.Where(o => o.MaterialCategory).OrderBy(o => o.CategoryName).ToListAsync();

        var m = new InstallWorkOrderModel
        {
            Estimate = estimate,
            OptionItems = options,
            Customer = estimate.Customer,
            BillingAddress = estimate.BillingAddress ?? new Address(), 
            InstallAddress = estimate.Address ?? new Address(), 
            TotalLabor = totalLabor,
            Categories = materialCategories,
        };

        ViewBag.ServerUrl = GetBaseUrl();

        return View(m);
    }
        
    private async Task<IActionResult> GetOfficeWorkOrderModel(Guid estimateId)
    {
	    var estimate = await AppContext.Estimates
		    .AsNoTracking()
		    .Include(u => u.Customer)
		    .Include(a => a.BillingAddress)
		    .Include(a => a.Address)
		    .FirstOrDefaultAsync(i => i.EstimateId == estimateId);

        if (estimate == null)
            return NotFound();

        decimal totalHours = 0, totalPrice = 0, totalCost = 0;
        List<OptionItem> options = new List<OptionItem>();

        if (estimate.SelectedOptionId == null)
        {
            AddStatusMessage(StatusLevels.Warn, "Selected Option Is Missing");
            //return View();
        }
        else
        {
            var estOption = await AppContext.EstimateOptions.Where(i => i.EstimateOptionId == estimate.SelectedOptionId.Value).FirstOrDefaultAsync();

            //Heating, Cooling & No Markup Sections
            int[] sections = { 0, 1, 2 };

            options = await GetOptionItems(estimate.SelectedOptionId.Value);

            foreach (var option in options)
            {
                option.SpecialMaterial = await GetSpecialMaterials(option.OptionItemId);
            }

            //var mat = await (from eo in AppContext.EstimateOptions
            //    join oi in AppContext.OptionItems on eo.EstimateOptionId equals oi.EstimateOptionId
            //    join mm in AppContext.OptionItemMaterials on oi.OptionItemId equals mm.OptionItemId
            //    where eo.EstimateId == estimateId
            //    select mm.Labor).ToListAsync();

            totalHours = estOption.LaborTotal.GetValueOrDefault(0);
            totalPrice = estOption.Total.GetValueOrDefault(0);
            totalCost = estOption.SubTotal.GetValueOrDefault(0);
        }

        var m = new OfficeWorkOrderModel
        {
            Estimate = estimate,
            OptionItems = options,
            Customer = estimate.Customer,
            BillingAddress = estimate.BillingAddress ?? new Address(), 
            InstallAddress = estimate.Address ?? new Address(), 
            TotalLabor = totalHours,
            TotalPrice = totalPrice,
            TotalCost = totalCost,
        };

        ViewBag.ServerUrl = GetBaseUrl();

        return View(m);
    }

    private async Task<List<OptionItem>> GetOptionItems(Guid estimateoptionid)
    {
        var options = await (from o in AppContext.OptionItems
            join c in AppContext.Categories on o.CategoryId equals c.CategoryId
            where c.MaterialCategory == false && o.EstimateOptionId == estimateoptionid
            select o).ToListAsync();
        return options;
    }

    private async Task<List<OptionItemMaterial>> GetSpecialMaterials(Guid optionItemId)
    {
        string[] categories = { "Coils", "Thermostats" };

        var data = await AppContext.OptionItemMaterials
	        .AsNoTracking()
	        .Include(i => i.Category)
	        .Where(i => categories.Contains(i.Category.CategoryName)
	                    && i.OptionItemId == optionItemId
	                    && i.CatalogNum != "Not Applicable")
	        .ToListAsync();

        //var data = await (from a in AppContext.OptionItemMaterials
        //    join b in AppContext.Categories on a.CategoryId equals b.CategoryId
        //    where categories.Contains(b.CategoryName) &&
        //          a.OptionItemId == optionItemId &&
        //          a.CatalogNum != "Not Applicable"
        //    select a).ToListAsync();

        //List<OptionItemMaterial> retval = new List<OptionItemMaterial>();

        //foreach (var item in data)
        //{
        //    //item.Category = AppContext.Categories.Where(m => m.CategoryId == item.CategoryId).First().CategoryName;
        //    retval.Add(item);
        //}

        return data;
    }
        
    private string GetServerUrl()
    {
        var request = HttpContext.Request;
        var host = request.Host.Value;
        var scheme = request.Scheme;
        string url = $"{scheme}://{host}";
        return url;
    }

    #region Downloading...

    private async Task<List<Guid>> GetOptionIds(Guid estimateoptionid)
    {
        var items = await (from e in AppContext.Estimates
            join eo in AppContext.EstimateOptions on e.SelectedOptionId equals eo.EstimateOptionId
            join oi in AppContext.OptionItems on eo.EstimateOptionId equals oi.EstimateOptionId
            where e.EstimateId == estimateoptionid && oi.Section < 2 //Only want cooling and heating
            select oi.OptionItemId).ToListAsync();
        return items;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [AllowAnonymous]
    public async Task<IActionResult> WorkOrderPage(Guid id)
    {
        var option = await AppContext.OptionItems.Where(i => i.OptionItemId == id).FirstOrDefaultAsync();

        if (option != null)
        {
            var data = await AppContext.SpGetMaterialList(id);

            option.MaterialList.AddRange(data);

            return PartialView("_WorkOrderPage", option);
        }
        return PartialView("_WorkOrderPage", new OptionItem());
    }

    #endregion
}