﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Web.Models;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace MountainAir.Web.Controllers;

public class HomeController : BaseController
{
    public HomeController(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
    {
    }

    public IActionResult Index()
    {
        return View();
    }

    [HttpGet, Route("api/[Action]")]
    public async Task<JsonResult> GetEstimateList()
    {
        try
        {
            var data = await AppContext.EstimateList.Where(i => i.Active == true
                                                                && i.ReadyToProcess == true
                                                                && i.TestData != true)
                .OrderByDescending(i => i.UpdatedDate)
                .Take(100)
                .ToListAsync();

            return Json(new JsonResponseModel(data));
        }
        catch (Exception ex)
        {
            return Json(new JsonResponseModel
            {
                Error = ex.Message
            });
        }
    }
        
    [AllowAnonymous]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}