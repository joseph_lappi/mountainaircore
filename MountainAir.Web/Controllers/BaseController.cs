﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.Data;
using MountainAir.Common.Security;
using MountainAir.Common.Utility;
using MountainAir.Web.Extensions;
using MountainAir.Web.Models;
using MountainAir.Web.Models.DataTables;
using System;
using System.Linq;
using System.Reflection;

namespace MountainAir.Web.Controllers;

public abstract class BaseController : Controller
{
    protected BaseController(ApplicationDbContext applicationDbContext)
    {
        AppContext = applicationDbContext;
    }

    protected ApplicationDbContext AppContext { get; }

    private ApplicationUser _appUser;
    protected ApplicationUser CurrentUser
    {
        get
        {
            if (_appUser == null)
            {
                var name = HttpContext.User.Identity.Name;
                _appUser = AppContext.Users.FirstOrDefault(i => i.UserName == name);
            }
            return _appUser;
        }
    }

    protected void FillObjectFromDictionary(DataBag data, string key, object item)
    {
        var d = data[key];

        var props = from prop in item.GetType().GetProperties() where prop.CanRead && prop.CanWrite select prop;

        foreach (PropertyInfo prop in props)
        {
            if (d.TryGetValue(GetSerializationName(item, prop.Name), out string value))
            {
                Type t = prop.PropertyType;

                if (value == null && t == typeof(string))
                    prop.SetValue(item, "", null);
                else if (t == typeof(string))
                    prop.SetValue(item, value, null);
                else if ((t == typeof(int) || t == typeof(int?)) && int.TryParse(value, out int i))
                    prop.SetValue(item, i, null);
                else if ((t == typeof(decimal) || t == typeof(decimal?)) && decimal.TryParse(value, out decimal de))
                    prop.SetValue(item, de, null);
                else if ((t == typeof(long) || t == typeof(long?)) && long.TryParse(value, out long l))
                    prop.SetValue(item, l, null);
                else if ((t == typeof(DateTime) || t == typeof(DateTime?)) && DateTime.TryParse(value, out DateTime dt))
                    prop.SetValue(item, dt, null);
                else if ((t == typeof(bool) || t == typeof(bool?)) && bool.TryParse(value, out bool b))
                    prop.SetValue(item, b, null);
                else if ((t == typeof(Guid) || t == typeof(Guid?)) && Guid.TryParse(value, out Guid g))
                    prop.SetValue(item, g, null);
                else if (t.BaseType == typeof(Enum))
                    prop.SetValue(item, Enum.Parse(t, value));
            }
        }
    }

    protected string GetSerializationName(object obj, string propertyName) => ReflectionHelper.GetSerializationName(obj.GetType(), propertyName);

    protected void AddErrors(IdentityResult result)
    {
        foreach (var error in result.Errors)
        {
            this.AddStatusMessage(StatusLevels.Error, error.Description);
        }
    }

    public void AddStatusMessage(Exception ex)
    {
        this.AddStatusMessage(StatusLevels.Error, ex.GetBaseException().Message);
    }

    public void AddStatusMessage(StatusLevels statusLevel, string statusMessage)
    {
        this.TempData.AddStatusMessage(statusLevel, statusMessage);
    }
}