﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using MountainAir.Web.Models.DataTables;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MountainAir.Common.Utility;

namespace MountainAir.Web.Controllers;

[RoleAuthorize(Roles.Administrators, Roles.CanViewCatalog)]
public class WarrantyTypeController : BaseController
{
    private readonly NLog.ILogger _logger = LogManager.GetCurrentClassLogger();

    public WarrantyTypeController(ApplicationDbContext applicationDbContext, ILogger<CategoriesController> logger)
        : base(applicationDbContext)
    {
    }

    public IActionResult Index()
    {            
        var model = new EditorViewModel<WarrantyType>
        {
            DisplayColumns = GetDisplayColumns(),
            EditorColumns = GetEditorColumns()
        };

        return View("~/Views/Administration/WarrantyTypes.cshtml", model);
    }

    public List<DisplayColumn> GetDisplayColumns()
    {
        var columns = new List<DisplayColumn>
        {
            new CheckBoxSelectColumn(),
            new DisplayColumn<WarrantyType>(t => t.TermText),
            new DisplayColumn<WarrantyType>(t => t.DisplayText),
            new DisplayColumn<WarrantyType>(t => t.ServiceFee),
            new DisplayColumn<WarrantyType>(t => t.TermLength),
            new DisplayColumn<WarrantyType>(t => t.Value)
        };

        return columns;
    }

    public List<EditorColumn> GetEditorColumns()
    {
        var columns = new List<EditorColumn>
        {
            new EditorColumn<WarrantyType>(t => t.TermText),
            new EditorColumn<WarrantyType>(t => t.DisplayText),
            new NumericEditorColumn<WarrantyType>(t => t.ServiceFee),
            new NumericEditorColumn<WarrantyType>(t => t.TermLength),
            new NumericEditorColumn<WarrantyType>(t => t.Value)
        };

        return columns;
    }

    [HttpGet, Route("api/[Controller]/[Action]")]
    public async Task<JsonResult> GetData()
    {
        var data = await AppContext.WarrantyTypes.AsNoTracking().ToListAsync();

        return Json(new
        {
            data
        });
    }

    [HttpPost]
    [Route("api/[Controller]/[Action]")]
    public async Task<IActionResult> UpdateData(UpdateModel model)
    {
        try
        {
            var data = model.Action switch
            {
                UpdateActions.Create => await InsertData(model.Data),
                UpdateActions.Edit => await UpdateData(model.Data),
                UpdateActions.Remove => await DeleteData(model.Data),
                _ => throw new NotImplementedException("Invalid Action")
            };
            
            var response = new EditorResponse<WarrantyType>(data);

            return Ok(response);
        }
        catch (Exception ex)
        {
            string message = $"Error updating warranty types: {ex.GetBaseException().Message}";

            _logger.Error(ex, message);

            return Conflict(message);
        }
    }

    public async Task<List<WarrantyType>> InsertData(DataBag data)
    {
        var inserted = new List<WarrantyType>();

        var keys = data.Keys;

        foreach (var key in keys)
        {
            var d = data[key];

            var row = new WarrantyType();

            FillObjectFromDictionary(data, key, row);

            AppContext.WarrantyTypes.Add(row);
            await AppContext.SaveChangesAsync();
            inserted.Add(row);
        }

        return inserted;
    }

    public async Task<List<WarrantyType>> UpdateData(DataBag data)
    {
        var keys = data.Keys;

        var updated = new List<WarrantyType>();

        foreach (var key in keys)
        {
            if (!Guid.TryParse(key, out Guid id))
                continue;

            var row = AppContext.WarrantyTypes.FirstOrDefault(i => i.Id == id);

            if (row == null)
                continue;

            var d = data[key];

            FillObjectFromDictionary(data, key, row);

            AppContext.WarrantyTypes.Update(row);
            
            await AppContext.SaveChangesAsync();

            updated.Add(row);
        }

        return updated;
    }

    public async Task<List<WarrantyType>> DeleteData(DataBag data)
    {
        var keys = data.Keys;

        foreach (var key in keys)
        {
            if (!Guid.TryParse(key, out Guid id))
                continue;

            var row = AppContext.WarrantyTypes.FirstOrDefault(i => i.Id == id);

            if (row != null)
            {
                AppContext.WarrantyTypes.Remove(row);

                await AppContext.SaveChangesAsync();
            }
        }

        return new List<WarrantyType>();
    }
}