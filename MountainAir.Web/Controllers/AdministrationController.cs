﻿using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.Security;

namespace MountainAir.Web.Controllers;

[RoleAuthorize(Roles.Administrators)]
public class AdministrationController : Controller
{
    public IActionResult Index()
    {
        return View();
    }
}