﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using MountainAir.Web.Models;
using MountainAir.Web.Models.Packages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Diagnostics;
using MountainAir.Web.Models.DataTables;
using NLog.Targets;

namespace MountainAir.Web.Controllers;

[RoleAuthorize(Roles.Administrators, Roles.CanEditCatalog)]
public class PackagesController : BaseController
{
    public PackagesController(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
    {
    }

    public IActionResult Index()
    {
        var data = AppContext.Packages.OrderBy(i => i.PackageName).ToList();

        return View(data);
    }

    public async Task<IActionResult> PackageItems(int id)
    {
        var package = await AppContext.Packages.FirstOrDefaultAsync(i => i.PackageId == id);

        var categories = await AppContext.Categories
            .Where(c => !c.MaterialCategory)
            .Include(p => p.CatalogItems.Where(c => !string.IsNullOrWhiteSpace(c.ModelNum)))
            .ToListAsync();

        var catalogItems = categories.SelectMany(ca => ca.CatalogItems.Select(ni => new EditorSelectOption()
        {
            Label = $"{ca.CategoryName} - {ni.ModelNum}",
            Value = ni.CatalogItemId
        })).OrderBy(c => c.Label).ToList();

        return View(new PackageEditingModel()
        {
            Package = package,
            CatalogItems = catalogItems
        });
    }

    #region Package API Endpoints

    [HttpGet, Route("api/[Controller]/[Action]")]
    public async Task<JsonResult> GetPackages()
    {
        try
        {
            var data = await AppContext.Packages.OrderBy(i => i.PackageName).ToListAsync();

            return Json(new JsonResponseModel(data));
        }
        catch (Exception ex)
        {
            return Json(new JsonResponseModel()
            {
                Error = ex.GetBaseException().Message
            });
        }
    }
        
    [HttpPost, Route("api/[Controller]/[Action]")]
    public async Task<JsonResult> UpdatePackageData(UpdateModel model)
    {
        try
        {
            var data = new List<Package>();

            switch (model.Action)
            {
                case UpdateActions.Remove:
                    await RemovePackageAsync(model.Data);
                    break;
                case UpdateActions.Edit:
                    data.AddRange(await UpdatePackage(model.Data));
                    break;
                case UpdateActions.Create:
                    data.AddRange(await CreatePackage(model.Data));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return Json(new EditorResponse<Package>(data));
        }
        catch (Exception ex)
        {
            return Json(new EditorResponse<Package>()
            {
                Error = ex.GetBaseException().Message
            });
        }
    }

    private async Task<IEnumerable<Package>> CreatePackage(DataBag data)
    {
        var retval = new List<Package>();

        foreach (var key in data.Keys)
        {
            var model = data[key];

            var newPackage = new Package();

            data.FillObject(key, newPackage);

            newPackage.AddDate = DateTime.Now;
            newPackage.AddBy = CurrentUser.Id;
            newPackage.Active = true;

            AppContext.Packages.Add(newPackage);

            retval.Add(newPackage);
        }

        await AppContext.SaveChangesAsync();

        return retval;
    }

    private async Task<IEnumerable<Package>> UpdatePackage(DataBag data)
    {
        var retval = new List<Package>();

        foreach (var key in data.Keys)
        {
            if(!int.TryParse(key, out int rowId))
                continue;

            var package = await AppContext.Packages.FirstOrDefaultAsync(p => p.PackageId == rowId);

            if (package == null)
                continue;
                
            var model = data[key];

            if (model.TryGetValue("packageName", out string packageName) && !string.IsNullOrWhiteSpace(packageName))
            {
                package.PackageName = packageName;
            }
                
            if (model.TryGetValue("ahriRating", out string ahriRating))
            {
                package.AhriRating = ahriRating;
            }
                
            if (model.TryGetValue("profitMargin", out string profitMargin) && decimal.TryParse(profitMargin, out decimal pm))
            {
                package.ProfitMargin = pm;
            }
                
            if (model.TryGetValue("active", out string active) && bool.TryParse(active, out bool act))
            {
                package.Active = act;
            }

            AppContext.Packages.Update(package);

            retval.Add(package);
        }

        await AppContext.SaveChangesAsync();

        return retval;
    }

    private async Task RemovePackageAsync(DataBag data)
    {
        var rowIds = new List<int>();

        foreach (var key in data.Keys)
        {
            if(!int.TryParse(key, out int rowId))
                continue;
            rowIds.Add(rowId);
        }

        var packages = await AppContext.Packages
            .Where(p => rowIds.Contains(p.PackageId))
            .Include(p => p.PackageItems)
            .ThenInclude(p => p.PackageMaterials)
            .ToListAsync();

        if (!packages.Any())
        {
            return;
        }

        await using var trans = await AppContext.Database.BeginTransactionAsync();

        try
        {
            var materials = packages.SelectMany(p => p.PackageItems.SelectMany(pi => pi.PackageMaterials)).ToList();

            if (materials.Any())
            {
                AppContext.PackageMaterials.RemoveRange(materials);
            }

            var packageItems = packages.SelectMany(p => p.PackageItems).ToList();

            if (packageItems.Any())
            {
                AppContext.PackageItems.RemoveRange(packageItems);
            }

            AppContext.Packages.RemoveRange(packages);
                
            await AppContext.SaveChangesAsync();

            await trans.CommitAsync();
        }
        catch
        {
            await trans.RollbackAsync();
            throw;
        }
    }

    #endregion

    #region API Endpoints

    [HttpGet, Route("api/[Controller]/[Action]/{categoryId}")]
    public async Task<JsonResult> GetCatalogItems(int categoryId)
    {
        var items = await AppContext.CatalogItems
            .Where(i => i.CategoryId == categoryId && i.Active == true)
            .OrderBy(i => i.CatalogNum)
            .ThenBy(i => i.Descr)
            .ToListAsync();

        return Json(new JsonResponseModel<CatalogItem>(items));
    }

    [HttpGet, Route("api/[Controller]/[Action]/{id}")]
    public async Task<JsonResult> GetPackageItems(int id)
    {
        try
        {
            var items = await AppContext.PackageItems
                .Where(i => i.PackageId == id)
                .Include(i => i.CatalogItem)
                .ToListAsync();

            return Json(new JsonResponseModel(items));
        }
        catch (Exception ex)
        {
            return Json(new JsonResponseModel()
            {
                Error = ex.GetBaseException().Message
            });
        }
    }
        
    [HttpGet, Route("api/[Controller]/[Action]/{id}")]
    public async Task<JsonResult> GetPackageMaterialItems(int id)
    {
        try
        {
            var items = await AppContext.PackageMaterials
                .Where(i => i.PackageMaterialId == id)
                .Include(i => i.CatalogItem)
                .ToListAsync();

            return Json(new JsonResponseModel(items));
        }
        catch (Exception ex)
        {
            return Json(new JsonResponseModel()
            {
                Error = ex.GetBaseException().Message
            });
        }
    }

    #endregion

    #region Packages

    [HttpGet]
    public IActionResult AddPackage()
    {
        return View(new Package());
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult AddPackage(Package model)
    {
        if(!ModelState.IsValid)
        {
            return View(model);
        }

        try
        {
            model.AddBy = CurrentUser.Id;
            model.AddDate = DateTime.Now;
            AppContext.Packages.Add(model);
            AppContext.SaveChanges();

            return RedirectToAction(nameof(Index));
        }
        catch(Exception ex)
        {
            AddStatusMessage(ex);
        }

        return View(model);
    }

    [HttpGet, Route("[Controller]/[Action]/{packageId}")]
    public async Task<IActionResult> EditPackage(int packageId)
    {
        var data = await AppContext.Packages
            .Include(i => i.PackageItems)
            .ThenInclude(i => i.CatalogItem)
            .FirstOrDefaultAsync(i => i.PackageId == packageId);

        if (data == null)
            return NotFound();

        //data.PackageItems.ForEach(item =>
        //{
        //    AppContext.Entry(item).Reference(nameof(PackageItem.CatalogItem)).Load();
        //});

        return View(data);
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult EditPackage(int id, Package model)
    {
        if(!ModelState.IsValid)
        {
            return View(model);
        }

        var exists = AppContext.Packages.Any(i => i.PackageId == id);

        if(!exists)
        {
            return BadRequest();
        }

        try
        {
            model.UpdatedBy = CurrentUser.Id;
            model.UpdatedDate = DateTime.Now;
            AppContext.Packages.Update(model);
            AppContext.SaveChanges();

            return RedirectToAction(nameof(Index));
        }
        catch(Exception ex)
        {
            AddStatusMessage(ex);
        }

        return View(model);
    }

    [HttpGet, Route("[Controller]/[Action]/{packageId}")]
    public IActionResult DeletePackage(int packageId)
    {
        var data = AppContext.Packages.FirstOrDefault(i => i.PackageId == packageId);

        if (data == null)
            return NotFound();

        return View(data);
    }

    [HttpPost, Route("[Controller]/DeletePackage/{packageId}")]
    [ValidateAntiForgeryToken]
    public IActionResult DeletePackageConfirmed(int packageId)
    {
        var data = AppContext.Packages.Include(i => i.PackageItems).Where(i => i.PackageId == packageId).FirstOrDefault();

        if (data == null)
            return BadRequest();

        try
        {
            data.PackageItems.ForEach(item =>
            {
                var material = AppContext.PackageMaterials.Where(i => i.PackageItemId == item.PackageItemId).ToList();

                if(material.Count > 0)
                {
                    AppContext.PackageMaterials.RemoveRange(material);
                }

                AppContext.PackageItems.Remove(item);
            });

            AppContext.Packages.Remove(data);
            AppContext.SaveChanges();

            return RedirectToAction(nameof(Index));
        }
        catch(Exception ex)
        {
            AddStatusMessage(ex);
        }

        return View(data);
    }

    #endregion

    #region Package Items

    [HttpGet, Route("[Controller]/[Action]/{packageId}")]
    public IActionResult AddPackageItem(int packageId)
    {
        var package = AppContext.Packages.FirstOrDefault(i => i.PackageId == packageId);

        return View(new AddPackageItemViewModel
        {
            Package = package,
            PackageItem = new PackageItem { PackageId = packageId },
            Categories = BuildCategorySelectList(false)
        });
    }
           
    private SelectList BuildCategorySelectList(bool getMaterialCategories)
    {
        var items = AppContext.Categories.Where(i => i.MaterialCategory == getMaterialCategories).OrderBy(i => i.CategoryName).ToList();

        var retval = new List<CategorySelectionModel>();

        retval.AddRange(items.Where(i => i.VisibleHeating).Select(i => new CategorySelectionModel
        {
            CategoryId = i.CategoryId,
            CategoryName = i.CategoryName,
            Group = "Heating"
        }));

        retval.AddRange(items.Where(i => i.VisibleCooling).Select(i => new CategorySelectionModel
        {
            CategoryId = i.CategoryId,
            CategoryName = i.CategoryName,
            Group = "Cooling"
        }));

        retval.AddRange(items.Where(i => i.VisibleNoMarkup).Select(i => new CategorySelectionModel
        {
            CategoryId = i.CategoryId,
            CategoryName = i.CategoryName,
            Group = "No Markup"
        }));


        retval.AddRange(items.Where(i => i.VisibleIncentive).Select(i => new CategorySelectionModel
        {
            CategoryId = i.CategoryId,
            CategoryName = i.CategoryName,
            Group = "Incentives"
        }));

        return new SelectList(retval, nameof(CategorySelectionModel.CategoryId), nameof(CategorySelectionModel.CategoryName), 0, nameof(CategorySelectionModel.Group));
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult AddPackageItem(int id, AddPackageItemViewModel model)
    {
        if (!ModelState.IsValid)
        {
            return View(model);
        }

        try
        {
            AppContext.PackageItems.Add(model.PackageItem);
            AppContext.SaveChanges();

            return RedirectToAction(nameof(EditPackage), new { id });
        }
        catch (Exception ex)
        {
            AddStatusMessage(ex);
        }

        return View(model);
    }

    [HttpGet]
    public IActionResult EditPackageItem(int id)
    {
        var data = AppContext.PackageItems
            .Include(i => i.CatalogItem)
            .Include(i => i.PackageMaterials)
            .FirstOrDefault(i => i.PackageItemId == id);

        if (data == null) 
            return NotFound();

        data.PackageMaterials.ForEach(p =>
        {
            AppContext.Entry(p).Reference(nameof(PackageMaterial.CatalogItem)).Load();
        });

        return View(data);
    }

    [HttpGet, Route("api/Packages/GetPackageMaterials/{id}")]
    public async Task<JsonResult> GetPackageMaterials(int id)
    {
        var material = await AppContext.PackageMaterials
            .Where(i => i.PackageItemId == id)
            .Include(i => i.CatalogItem)
            .ToListAsync();

        var data = material.Select(i => new PackageMaterialViewModel
        {
            Labor = i.Labor,
            Price = i.Price,
            Quantity = i.Quantity,
            Description = i.CatalogItem.Descr,
            ModelNumber = i.CatalogItem.ModelNum,
            CatalogNumber = i.CatalogItem.CatalogNum,
            PackageMaterialId = i.PackageMaterialId,
            Active = i.CatalogItem.Active
        }).ToList();


        return Json(new { data });
    }

    [HttpPost]
    public IActionResult EditPackageItem(int id, PackageItem model)
    {
        if (!ModelState.IsValid)
        {
            return View(model);
        }

        var packageItem = AppContext.PackageItems.FirstOrDefault(i => i.PackageItemId == id);

        if (packageItem == null)
        {
            return BadRequest();
        }

        try
        {
            packageItem.Quantity = model.Quantity;
            packageItem.Price = model.Price;
            packageItem.Labor = model.Labor;
            packageItem.AdditionalPrice = model.AdditionalPrice;
            AppContext.PackageItems.Update(packageItem);
            AppContext.SaveChanges();

            AddStatusMessage(StatusLevels.Success, "Data Saved");
            return RedirectToAction(nameof(EditPackageItem), id);
        }
        catch(Exception ex)
        {
            AddStatusMessage(ex);
            return View(model);
        }

    }

    [HttpGet]
    public IActionResult DeletePackageItem(int id)
    {
        var data = AppContext.PackageItems.Include(i => i.CatalogItem).FirstOrDefault(i => i.PackageItemId == id);

        if (data == null)
            return NotFound();

        return View(data);
    }

    [HttpPost, Route("[Controller]/DeletePackageItem/{id}")]
    [ValidateAntiForgeryToken]
    public IActionResult DeletePackageItemConfirmed(int id)
    {
        var data = AppContext.PackageItems.Include(i => i.PackageMaterials).FirstOrDefault(i => i.PackageItemId == id);

        if (data == null)
            return BadRequest();

        try
        {
            int packageId = data.PackageId;

            var material = data.PackageMaterials.Where(i => i.PackageItemId == id).ToList();

            if (material.Count > 0)
            {
                AppContext.PackageMaterials.RemoveRange(material);
                AppContext.SaveChanges();
            }

            AppContext.PackageItems.Remove(data);
            AppContext.SaveChanges();

            return RedirectToAction(nameof(EditPackage), new { id = packageId });
        }
        catch (Exception ex)
        {
            AddStatusMessage(ex);
        }

        return View(data);
    }

    [HttpPost, Route("api/[Controller]/[Action]")]
    public JsonResult AddItemToPackage([FromBody] dynamic model)
    {
        try
        {
            int packageId = model.PackageId;
            int catalogItemId = model.CatalogItemId;
            string group = model.Group;

            int section = 0;

            switch (group)
            {
                case "Heating": section = 0; break;
                case "Cooling": section = 1; break;
                case "No Markup": section = 2; break;
                case "Incentives": section = 3; break;
            }

            var catalogItem = AppContext.CatalogItems.FirstOrDefault(i => i.CatalogItemId == catalogItemId);

            var item = new PackageItem
            {
                CatalogItemId = catalogItemId,
                PackageId = packageId,
                Quantity = 1,
                Section = section,
                Price = catalogItem.Price,
                Labor = catalogItem.Labor
            };

            AppContext.PackageItems.Add(item);
            AppContext.SaveChanges();

            return Json(new
            {
                Message = "Equipment Added"
            });
        }
        catch (Exception ex)
        {
            return Json(new
            {
                ex.Message,
                IsError = true
            });
        }
    }

    #endregion

    #region Package Material

    [HttpGet, Route("[Controller]/[Action]/{packageItemId}")]
    public IActionResult AddPackageMaterial(int packageItemId)
    {
        var packageItem = AppContext.PackageItems
            .Include(i => i.Package)
            .Include(i => i.CatalogItem)
            .FirstOrDefault(i => i.PackageItemId == packageItemId);

        var items = AppContext.Categories.Where(i => i.MaterialCategory == true).OrderBy(i => i.CategoryName).ToList();
            
        return View(new AddPackageMaterialViewModel
        {
            Package = packageItem.Package,
            PackageItem = packageItem,
            CatalogItem = packageItem.CatalogItem,
            Categories = BuildCategorySelectList(true)
        });
    }

    [HttpPost, Route("api/[Controller]/[Action]")]
    public JsonResult AddMaterialToPackage([FromBody] dynamic model)
    {
        try
        {
            int packageItemId = model.PackageItemId;
            int catalogItemId = model.CatalogItemId;

            var catalogItem = AppContext.CatalogItems.FirstOrDefault(i => i.CatalogItemId == catalogItemId);

            var item = new PackageMaterial
            {
                CatalogItemId = catalogItemId,
                PackageItemId = packageItemId,
                Quantity = 1,
                Price = catalogItem.Price,
                Labor = catalogItem.Labor
            };

            AppContext.PackageMaterials.Add(item);
            AppContext.SaveChanges();

            return Json(new
            {
                Message = "Material Added"
            });
        }
        catch (Exception ex)
        {
            return Json(new
            {
                ex.Message,
                IsError = true
            });
        }
    }

    [HttpGet]
    public IActionResult EditPackageMaterial(int id)
    {
        var data = AppContext.PackageMaterials
            .Include(i => i.CatalogItem)
            .FirstOrDefault(i => i.PackageMaterialId == id);

        if (data == null)
            return NotFound();

        return View(data);
    }

    [HttpPost]
    public IActionResult EditPackageMaterial(int id, PackageMaterial model)
    {
        if (!ModelState.IsValid)
        {
            return View(model);
        }

        var packageItem = AppContext.PackageMaterials.FirstOrDefault(i => i.PackageMaterialId == id);

        if (packageItem == null)
        {
            return BadRequest();
        }

        try
        {
            packageItem.Quantity = model.Quantity;
            packageItem.Price = model.Price;
            packageItem.Labor = model.Labor;
            AppContext.PackageMaterials.Update(packageItem);
            AppContext.SaveChanges();

            AddStatusMessage(StatusLevels.Success, "Data Saved");
        }
        catch (Exception ex)
        {
            AddStatusMessage(ex);
        }

        return View(model);
    }

    [HttpGet]
    public IActionResult DeletePackageMaterial(int id)
    {
        var data = AppContext.PackageMaterials.Include(i => i.CatalogItem).FirstOrDefault(i => i.PackageMaterialId == id);

        if (data == null)
            return NotFound();

        return View(data);
    }

    [HttpPost, Route("[Controller]/DeletePackageMaterial/{id}")]
    [ValidateAntiForgeryToken]
    public IActionResult DeletePackageMaterialConfirmed(int id)
    {
        var data = AppContext.PackageMaterials.FirstOrDefault(i => i.PackageMaterialId == id);

        if (data == null)
            return BadRequest();

        try
        {
            int packageItemId = data.PackageItemId;

            AppContext.PackageMaterials.Remove(data);
            AppContext.SaveChanges();

            return RedirectToAction(nameof(EditPackageItem), new { id = packageItemId });
        }
        catch (Exception ex)
        {
            AddStatusMessage(ex);
        }

        return View(data);
    }

    #endregion
}