﻿using Microsoft.AspNetCore.Mvc;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using MountainAir.Common.Utility;
using MountainAir.Web.Models.DataTables;
using System.Collections.Generic;

namespace MountainAir.Web.Controllers;

[RoleAuthorize(Roles.Administrators, Roles.CanViewCatalog)]
public class FinancingController : DataTablesBaseController<Financing>
{
    public FinancingController(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
    {
    }
        
    [RoleAuthorize(Roles.Administrators, Roles.CanViewCatalog)]
    public IActionResult Index()
    {
        var model = new EditorViewModel<Financing>
        {
            DisplayColumns = new List<DisplayColumn>
            {
                new CheckBoxSelectColumn(),
                new NumericDisplayColumn<Financing>(f => f.Rate),
                new DisplayColumn<Financing>(f => f.Term),
                new DisplayColumn<Financing>(f => f.PaymentFactor)
            },
            EditorColumns = new List<EditorColumn>
            {
                new EditorColumn<Financing>(f => f.Rate),
                new EditorColumn<Financing>(f => f.Term),
                new EditorColumn<Financing>(f => f.PaymentFactor)
            },
        };
        return View("~/Views/Administration/Financing.cshtml", model);
    }
}