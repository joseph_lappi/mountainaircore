﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using MountainAir.Web.Models;
using MountainAir.Web.Models.DataTables;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MountainAir.Web.Controllers;

[RoleAuthorize(Roles.Administrators, Roles.CanViewCatalog)]
public class CatalogItemsController : BaseController
{
    public CatalogItemsController(ApplicationDbContext context)
        : base(context)
    {
    }

    public async Task<IActionResult> Index()
    {
        var categories = await AppContext.Categories.ToListAsync();

        var model = new CatalogItemsViewModel()
        {
            Categories = categories
        };

        return View("~/Views/Administration/CatalogItems.cshtml", model);
    }

    [HttpGet, Route("api/[Controller]/[Action]/{categoryId:int}")]
    public async Task<IActionResult> GetData(int categoryId)
    {
        try
        {
            var query = AppContext.CatalogItems
                .AsNoTracking();

            if (categoryId > 0)
            {
                query = query.Where(c => c.CategoryId == categoryId);
            }

            var data = await query
                .Include(c => c.Category)
                .OrderBy(o => o.Category.CategoryName)
                .ToListAsync();

            return Ok(new JsonResponseModel(data));
        }
        catch (Exception ex)
        {
            return Conflict(new JsonResponseModel(null, ex));
        }
    }

    [HttpPost]
    [HttpPut]
    [HttpDelete]
    [Route("api/[Controller]/[Action]")]
    public async Task<JsonResult> UpdateData(UpdateModel model)
    {
        switch (model.Action)
        {
            case UpdateActions.Create:
                return await InsertData(model.Data);
            case UpdateActions.Edit:
                return await UpdateData(model.Data);
            case UpdateActions.Remove:
                return await DeleteData(model.Data);
            default:
                throw new NotImplementedException("Invalid Action");
        }
    }

    protected async Task<JsonResult> InsertData(DataBag data)
    {
        var inserted = new List<CatalogItem>();

        var keys = data.Keys;

        foreach (var key in keys)
        {
            var d = data[key];
                
            var row = new CatalogItem();

            FillObjectFromDictionary(data, key, row);

            AppContext.Add(row);
            await AppContext.SaveChangesAsync();
            inserted.Add(row);
        }

        return Json(new { data = inserted });
    }

    protected async Task<JsonResult> UpdateData(DataBag data)
    {
        var keys = data.Keys;

        var updated = new List<CatalogItem>();

        foreach (var key in keys)
        {
            var row = await AppContext.CatalogItems.FirstOrDefaultAsync(i => i.CatalogItemId == int.Parse(key));

            if (row == null)
                continue;

            var d = data[key];
                
            FillObjectFromDictionary(data, key, row);

            AppContext.Update(row);
                
            await AppContext.SaveChangesAsync();

            updated.Add(row);
        }

        return Json(new { data = updated });
    }

    protected async Task<JsonResult> DeleteData(DataBag data)
    {
        var retval = new List<CatalogItem>();
        var keys = data.Keys;

        foreach (var key in keys)
        {
            var id = int.Parse(key);

            var row = await AppContext.CatalogItems.FirstOrDefaultAsync(i => i.CatalogItemId == id);

            if (row != null)
            {
                retval.Add(row);

                AppContext.Remove(row);
                    
                await AppContext.SaveChangesAsync();
            }
        }

        return Json(new { data = retval });
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [RoleAuthorize(Roles.Administrators, Roles.CanEditCatalog)]
    public IActionResult FileUpload()
    {
        ViewBag.CategoryId = new SelectList(AppContext.Categories.OrderBy(c => c.CategoryName), "CategoryId", "CategoryName");

        return View();
    }
}