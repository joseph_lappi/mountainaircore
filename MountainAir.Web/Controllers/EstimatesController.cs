﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.AzureStorage;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using MountainAir.Web.Models;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace MountainAir.Web.Controllers;

[RoleAuthorize(Roles.Administrators, Roles.CanViewProposals)]
public class EstimatesController : BaseController
{
    private readonly IAzureBlobManager _azureBlobManager;
    private readonly UserManager<ApplicationUser> _userManager;

    public EstimatesController(
        ApplicationDbContext applicationDbContext,
        IAzureBlobManager azureBlobManager,
        UserManager<ApplicationUser> userManager)
        : base(applicationDbContext)
    {
        _azureBlobManager = azureBlobManager;
        _userManager = userManager;
    }

    [RoleAuthorize(Roles.Administrators, Roles.CanViewProposals)]
    public IActionResult Index()
    {
        return View();
    }

    [HttpGet, Route("api/Estimates/GetEstimates")]
    public async Task<JsonResult> GetEstimates([FromQuery] bool active, [FromQuery] int dateRange)
    {
        var estimates = new List<vEstimateList>();
        var user = CurrentUser;
        DateTime date = DateTime.Now.AddYears(-10);

        if (dateRange > 0)
            date = DateTime.Now.AddDays(dateRange * -1);

        if (await _userManager.IsInRoleAsync(user, Roles.Administrators.ToString()) || await _userManager.IsInRoleAsync(user, Roles.CanViewAllProposals.ToString()))
        {
            estimates = await AppContext.EstimateList
                .Where(i => i.Active == active && i.TestData != false && i.CreatedDate >= date)
                .ToListAsync();
        }
        else
        {
            estimates = await AppContext.EstimateList
                .Where(i => i.AssignedTo == user.Id && i.Active == active && i.CreatedDate >= date)
                .ToListAsync();
        }

        foreach (var est in estimates)
        {
            if (est.ReadyToProcess == true && est.SelectedOptionId != null)
            {
                est.ValidationNotes = "Ready";
            }
            else if (est.SelectedOptionId == null)
            {
                est.ValidationNotes = "Missing Selected Option";
            }
            else
            {
                est.ValidationNotes = "Not Ready";
            }
        }

        return Json(new JsonResponseModel<vEstimateList>(estimates));
    }

    //private List<vEstimateList> GetData(bool active = true)
    //{
    //    var estimates = new List<vEstimateList>();
    //    var user = CurrentUser;

    //    if (_userManager.IsInRoleAsync(user, Roles.Administrators.ToString()).Result || _userManager.IsInRoleAsync(user, Roles.CanViewAllProposals.ToString()).Result)
    //    {
    //        estimates = AppContext.EstimateList.Where(i => i.Active == active && i.TestData != false).ToList();
    //    }
    //    else
    //    {
    //        estimates = AppContext.EstimateList.Where(i => i.AssignedTo == user.Id && i.Active == active).ToList();
    //    }

    //    foreach (var est in estimates)
    //    {
    //        if (est.ReadyToProcess == true && est.SelectedOptionId != null)
    //        {
    //            est.ValidationNotes = "Ready";
    //        }
    //        else if (est.SelectedOptionId == null)
    //        {
    //            est.ValidationNotes = "Missing Selected Option";
    //        }
    //        else
    //        {
    //            est.ValidationNotes = "Not Ready";
    //        }
    //    }

    //    return estimates;
    //}

    private async Task<List<OptionItemMaterial>> GetSpecialMaterials(Guid optionItemId)
    {
        string[] categories = { "Coils", "Thermostats" };

        var data = await AppContext.OptionItemMaterials
	        .AsNoTracking()
	        .Include(i => i.Category)
	        .Where(i => categories.Contains(i.Category.CategoryName)
	                    && i.OptionItemId == optionItemId
	                    && i.CatalogNum != "Not Applicable")
	        .ToListAsync();

        //var data = await (from a in AppContext.OptionItemMaterials
        //    join b in AppContext.Categories on a.CategoryId equals b.CategoryId
        //    where categories.Contains(b.CategoryName) &&
        //          a.OptionItemId == optionItemId &&
        //          a.CatalogNum != "Not Applicable"
        //    select a).ToListAsync();

        //var retval = new List<OptionItemMaterial>();

        //foreach (var item in data)
        //{
        //    item.Category = AppContext.Categories.First(m => m.CategoryId == item.CategoryId).CategoryName;
        //    retval.Add(item);
        //}

        return data;
    }

    [RoleAuthorize(Roles.Administrators, Roles.CanEditProposals)]
    public async Task<IActionResult> Edit(Guid? id)
    {
        if (id == null)
        {
            return BadRequest();
        }

        var estimate = await AppContext.Estimates
            .Include(e => e.Created)
            .Include(e => e.Updated)
            .Include(c => c.Customer)
            .Include(c => c.Warranties)
            .Include(eo => eo.EstimateOptions)
            .ThenInclude(oi => oi.OptionItems)
            .ThenInclude(oi => oi.Category)
            .Include(r => r.Rooms)
            .ThenInclude(d => d.Doors)
            .Include(r => r.Rooms)
            .ThenInclude(w => w.Windows)
            .Where(e => e.EstimateId == id)
            .FirstOrDefaultAsync();

        if (estimate == null)
        {
            return NotFound();
        }

        if (estimate.SelectedOption != null)
        {
            foreach (var option in estimate.SelectedOption.OptionItems)
            {
                option.SpecialMaterial = await GetSpecialMaterials(option.OptionItemId);

                var data = await AppContext.SpGetMaterialList(option.OptionItemId);

                option.MaterialList.AddRange(data);
            }
        }

        var results = await _azureBlobManager.GetEstimateFiles(id.Value);
        estimate.Attachments = results.ToList();

        var users = (await _userManager.Users.ToListAsync()).OrderBy(i => i.LastName).ThenBy(i => i.FirstName);

        var model = new EstimateEditModel
        {
            Estimate = estimate,
            AssignedTo = new SelectList(users, "Id", "FullName", estimate.AssignedTo)
        };

        return View(model);
    }

    public class SaveEstimatePostModel
    {
        public Guid EstimateId { get; set; }

        public Guid Assigned { get; set; }

        public bool Active { get; set; }
    }

    [HttpPost, Route("api/Estimates/SaveEstimate")]
    public async Task<JsonResult> SaveEstimate([FromBody] SaveEstimatePostModel obj)
    {
        try
        {
            Guid estimateid = obj.EstimateId;
            Guid assigned = obj.Assigned;
            bool active = obj.Active;

            var estimate = await AppContext.Estimates.FirstOrDefaultAsync(e => e.EstimateId == estimateid);

            if (estimate != null)
            {
                estimate.AssignedTo = assigned;
                estimate.Active = active;
                await AppContext.SaveChangesAsync();

                return Json(new { Message = "Estimate Updated" });
            }

            return Json(new { Message = "Invalid Estimate" });
        }
        catch (Exception ex)
        {
            return Json(new { ex.Message });
        }
    }

    private int[] GetIntArray(string[] array)
    {
        List<int> retval = new List<int>();

        foreach (var str in array)
        {
            if (int.TryParse(str, out int value))
            {
                retval.Add(value);
            }
        }

        return retval.ToArray();
    }

    private int[] GetIntArray(string stringArray, char delimeter)
    {
        var array = stringArray.Split(delimeter);

        return GetIntArray(array);
    }

    [RoleAuthorize(Roles.Administrators, Roles.CanViewProposals)]
    public async Task<IActionResult> Attachments(Guid id)
    {
        var estimate = await AppContext.EstimateList.FirstOrDefaultAsync(e => e.EstimateId == id);

        if (estimate == null)
        {
            return BadRequest();
        }

        try
        {
            var results = await _azureBlobManager.GetEstimateFiles(id);
            estimate.Attachments = results.ToList();
        }
        catch (Exception ex)
        {
            AddStatusMessage(ex);
        }
        return View(estimate);
    }

    [RoleAuthorize(Roles.Administrators, Roles.CanViewProposals)]
    public async Task<IActionResult> Report(Guid id)
    {
        var estimate = await AppContext.Estimates//.Where(e => e.EstimateId == id)
            .Include(e => e.Customer)
            .Include(e => e.Address)
            .Include(e => e.Rooms)
            .FirstOrDefaultAsync(e => e.EstimateId == id);

        if (estimate == null)
        {
            return NotFound();
        }

        return View(estimate);
    }
        
    [AllowAnonymous]
    public async Task<IActionResult> WarrantyAgreement(Guid id)
    {
        var warranty = await AppContext.Warranties
            .AsNoTracking()
            .Include(w => w.Estimate)
            .FirstOrDefaultAsync(i => i.Id == id);

        if (warranty == null)
        {
            warranty = new Warranty();
        }

        return View(warranty);
    }

    [AllowAnonymous]
    public async Task<IActionResult> AhriAddendum(Guid id)
    {
        var estimate = await AppContext.Estimates
            .Include(e => e.Customer)
            .Include(e => e.Address)
            .Include(e => e.EstimateOptions)
            .FirstOrDefaultAsync(e => e.EstimateId == id);

        if (estimate == null)
        {
            return NotFound();
        }

        return View(estimate);
    }

    [AllowAnonymous]
    public async Task<IActionResult> AhriPdf(Guid id)
    {
        var baseUrl = $"{Request.Scheme}://{Request.Host}"; //, Request.PathBase);

        using var client = new HttpClient
        {
            BaseAddress = new Uri(baseUrl)
        };

        var css = await client.GetStringAsync("lib/bootstrap/css/bootstrap.min.css");

        var html = await client.GetStringAsync($"Estimates/AhriAddendum/{id}");

        html = html.Replace("/*Placeholder*/", css);

        var converter = new HtmlToPdf();

        await using var ms = new MemoryStream();

        var doc = converter.ConvertHtmlString(html);

        doc.Save(ms);

        var buffer = ms.ToArray();

        doc.Close();

        return File(buffer, "application/pdf", $"{id}.pdf");
    }
}