﻿using EFCore.BulkExtensions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MountainAir.Common.Data;
using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;
using MountainAir.Common.Services;
using MountainAir.Web.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace MountainAir.Web.Controllers;

/// <summary>
/// 
/// </summary>
[RoleAuthorize(Roles.Administrators, Roles.CanEditCatalog)]
public class FileUploadController : BaseController
{
    public FileUploadController(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    [RoleAuthorize(Roles.Administrators, Roles.CanEditCatalog)]
    public IActionResult Index(int? id)
    {
        if (id == null)
            id = 1;

        var categories = AppContext.Categories.Select(i => new
            {
                i.CategoryId,
                i.CategoryName,
                Group = i.MaterialCategory ? "Material" : "Equipment"
            })
            .OrderBy(i => i.CategoryName);

        var list = new SelectList(categories, nameof(Category.CategoryId), nameof(Category.CategoryName), id, "Group");

        var model = new FileUploadModel
        {
            CategoryId = id.Value,
            Categories = list
        };

        return View(model);
    }

    [HttpPost, Route("[Controller]/[Action]/{id}")]
    public async Task<IActionResult> UploadFile(int id, IFormFile file)
    {
        if (file.Length > 0)
        {
            try
            {
                string message = await ProcessFile(id, file);

                return Ok(message);
            }
            catch (Exception ex)
            {
                AddStatusMessage(ex);
                return BadRequest(ex.Message);
            }
        }
        return BadRequest();
    }

    [HttpPost, Route("[Controller]/[Action]")]
    public async Task<IActionResult> UploadFile(IFormFile file)
    {
        if (file.Length > 0)
        {
            try
            {
                var categories = AppContext.Categories.ToList();

                string message = await ProcessFile(file, categories);

                return Ok(message);
            }
            catch (Exception ex)
            {
                AddStatusMessage(ex);
            }
        }
        return BadRequest();
    }

    private async Task<string> ProcessFile(int categoryId, IFormFile file)
    {
        var items = new List<CatalogItem>();

        using (var ms = file.OpenReadStream())
        {
            var reader = new StreamReader(ms);
            string data = reader.ReadToEnd();
            items.AddRange(new UploadService().ConvertFileData(categoryId, data));
        }

        if (items.Count > 0)
        {
            var comp = new CatalogComparer();

            items = items.Distinct(comp).ToList();

            foreach (var item in items)
            {
                var foundItem = GetCatalogItem(item.CategoryId, item.ModelNum);
                if (foundItem != null)
                {
                    item.CatalogItemId = foundItem.CatalogItemId;
                }
            }

            BulkConfig config = new BulkConfig
            {
                CalculateStats = true
            };

            await AppContext.BulkInsertOrUpdateAsync(items, config);

            return $"{config.StatsInfo.StatsNumberUpdated} rows updated. {config.StatsInfo.StatsNumberInserted} rows inserted.";
        }
        return $"Nothing uploaded. The file appears to be empty.";
    }

    private async Task<string> ProcessFile(IFormFile file, List<Category> categories)
    {
        var items = new List<CatalogItem>();

        using (var ms = file.OpenReadStream())
        {
            var reader = new StreamReader(ms);
            string data = reader.ReadToEnd();

            var result = new UploadService().ConvertFileData(data, categories);

            if(result.BadRows.Count > 0)
            {
                string errorMessage = "Missing the following model numbers: " + string.Join(',', result.BadRows.Select(i => i.ModelNum).ToArray());
                throw new ApplicationException(errorMessage);
            }

            items.AddRange(result.GoodRows);
        }

        if (items.Count > 0)
        {
            foreach (var item in items)
            {
                var foundItem = GetCatalogItem(item.CategoryId, item.ModelNum);
                if (foundItem != null)
                {
                    item.CatalogItemId = foundItem.CatalogItemId;
                }
            }

            BulkConfig config = new BulkConfig
            {
                CalculateStats = true
            };

            var comp = new CatalogComparer();

            await AppContext.BulkInsertOrUpdateAsync(items.Distinct(comp).ToList(), config);

            return $"{config.StatsInfo.StatsNumberUpdated} rows updated. {config.StatsInfo.StatsNumberInserted} rows inserted.";
        }

        return $"Nothing uploaded. The file appears to be empty.";
    }

    private Dictionary<int, List<CatalogItem>> _items = new Dictionary<int, List<CatalogItem>>();
    private CatalogItem GetCatalogItem(int categoryid, string modelNumber)
    {
        if (!_items.TryGetValue(categoryid, out List<CatalogItem> items))
        {
            items = AppContext.CatalogItems.AsNoTracking().Where(i => i.CategoryId == categoryid && i.Active).ToList();
            _items[categoryid] = items;
        }
        return items.Where(i => i.ModelNum == modelNumber).FirstOrDefault();
    }

    private class CatalogComparer : IEqualityComparer<CatalogItem>
    {
        public bool Equals(CatalogItem x, CatalogItem y)
        {
            return (x.CategoryId == y.CategoryId && x.ModelNum.Equals(y.ModelNum, StringComparison.OrdinalIgnoreCase));
        }

        public int GetHashCode(CatalogItem obj)
        {
            return $"{obj.CategoryId}::{obj.ModelNum}".GetHashCode(StringComparison.Ordinal);
        }
    }
}