﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace MountainAir.Web.Models;

public class JsonResponseModel
{
    public JsonResponseModel() { }

    public JsonResponseModel(object data)
    {
        this.Data = data;
    }

    public JsonResponseModel(object data, Exception ex)
        : this(data, ex.Message)
    {
        this.Data = data;
        this.Error = ex.Message;
    }
        
    public JsonResponseModel(object data, string error)
    {
        this.Data = data;
        this.Error = error;
    }

    [JsonProperty("message")]
    public string Message { get; set; }

    [JsonProperty("error")]
    public string Error { get; set; }

    [JsonProperty("data")]
    public object Data { get; set; }
}

public class JsonResponseModel<T>
{
    public JsonResponseModel() { }

    public JsonResponseModel(List<T> data)
    {
        this.Data = data;
    }

    [JsonProperty("message")]
    public string Message { get; set; }

    [JsonProperty("error")]
    public string Error { get; set; }

    [JsonProperty("data")]
    public List<T> Data { get; set; } = new List<T>();
}