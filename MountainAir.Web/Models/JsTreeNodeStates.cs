﻿namespace MountainAir.Web.Models;

public enum JsTreeNodeStates
{
    opened,
    Disabled,
    Selected
}