﻿using MountainAir.Common.Data.Models;
using System.Collections.Generic;

namespace MountainAir.Web.Models;

/// <summary>
/// 
/// </summary>
public class CategoryIndexModel
{
    /// <summary>
    /// 
    /// </summary>
    public bool ShowMaterial { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public IEnumerable<Category> Categories { get; set; }
}