﻿using System.Collections.Generic;
using MountainAir.Common.Data.Models;

namespace MountainAir.Web.Models;

public class HomeViewModel
{
    public List<vEstimateList> Estimates { get; internal set; }
}