﻿using Newtonsoft.Json;

namespace MountainAir.Web.Models.DataTables;

public class SelectColumnOption
{
    public SelectColumnOption()
    {
    }

    public SelectColumnOption(string label, object value)
    {
        Label = label;
        Value = value;
    }

    [JsonProperty("value")]
    public object Value { get; set; }

    [JsonProperty("label")]
    public string Label { get; set; }
}