﻿using MountainAir.Common.Utility;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MountainAir.Web.Models.DataTables;

public class EditorColumn<T> : EditorColumn
        where T : class
{
    public EditorColumn(Expression<Func<T, object>> expression, EditorColumnTypes columnType = EditorColumnTypes.Text)
        : base(columnType)
    {
        Label = ReflectionHelper.GetDisplayValue<T>(expression);
        NameField = ReflectionHelper.GetSerializationName<T>(expression);
    }
}

public class EditorColumn : Column
{
    public EditorColumn()
        : this(EditorColumnTypes.Text)
    {
    }

    public EditorColumn(EditorColumnTypes columnType)
    {
        //Attributes.Add("type", columnType.ToString().ToLower());
        Type = columnType;
    }

    public EditorColumn(EditorColumnTypes columnType, string label, string name)
    {
        //Attributes.Add("type", columnType.ToString().ToLower());
        Type = columnType;
        Label = label;
        NameField = name;
    }


    [JsonProperty("label")]
    public string Label { get; set; }

    [JsonProperty("name")]
    public string NameField { get; set; }

    //[JsonProperty("def")]
    [JsonIgnore]
    public virtual string Definition { get; set; }

    [JsonProperty("attr")]
    public Dictionary<string, string> Attributes { get; } = new();

    [JsonIgnore]
    public EditorColumnTypes Type { get; set; }

    [JsonProperty("type")]
    public string ColumnType => Type.ToString().ToLower();

    [JsonIgnore]
    public string PlaceHolder
    {
        get
        {
            if (Attributes.TryGetValue("placeholder", out string value))
                return value;
            return null;
        }
        set
        {
            Attributes["placeholder"] = value;
        }
    }
}