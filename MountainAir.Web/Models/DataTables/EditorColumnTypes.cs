﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace MountainAir.Web.Models.DataTables;

[JsonConverter(typeof(StringEnumConverter))]
public enum EditorColumnTypes
{
    Checkbox,
    DateTime,
    Hidden,
    Password,
    Radio,
    Readonly,
    Select,
    Text,
    [Obsolete("Does not work at this time. Do not use.", true)]
    TextArea,
    Upload,
    UploadMany
}