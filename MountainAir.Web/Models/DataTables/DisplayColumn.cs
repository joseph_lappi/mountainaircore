﻿using MountainAir.Common.JsonConverters;
using MountainAir.Common.Utility;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;
using System.Reflection;

namespace MountainAir.Web.Models.DataTables;

public class DisplayColumn<T> : DisplayColumn
    where T : class
{
    public DisplayColumn(Expression<Func<T, object>> expression)
    {
        Title = ReflectionHelper.GetDisplayValue<T>(expression);
        Data = ReflectionHelper.GetSerializationName<T>(expression);
    }

    public DisplayColumn(PropertyInfo propertyInfo)
        : base(typeof(T), propertyInfo)
    {
    }

    public DisplayColumn(string fieldName)
        : base(typeof(T), fieldName)
    {
    }
}

public class DisplayColumn : Column
{
    public DisplayColumn() { }

    public DisplayColumn(Type type, string fieldName)
    {
        var displayAttr = GetAttribute<DisplayAttribute>(type, fieldName);
        var jsonAttr = GetAttribute<JsonPropertyAttribute>(type, fieldName);

        Title = displayAttr != null ? displayAttr.Name : fieldName;
        Data = jsonAttr != null ? jsonAttr.PropertyName : fieldName;
        Hidden = displayAttr != null && displayAttr.GetAutoGenerateField() != null ? !displayAttr.AutoGenerateField : false;
    }

    public DisplayColumn(Type type, PropertyInfo propertyInfo)
        : this(type, propertyInfo.Name)
    {
    }

    [JsonProperty("title")]
    public string Title { get; set; }

    [JsonProperty("data")]
    public string Data { get; set; }

    [JsonConverter(typeof(PlainJsonStringConverter))]
    [JsonProperty("render")]
    public virtual string Render { get; set; }

    [JsonProperty("className")]
    public string ClassName { get; set; }

    [JsonProperty("defaultContent")]
    public string DefaultContent { get; set; }

    [JsonProperty("orderable")]
    public bool Orderable { get; set; } = true;

    [JsonIgnore]
    public bool Hidden { get; set; } = false;

    public static T GetAttribute<T>(Type type, string propertyName)
        where T : Attribute
    {
        var propertyInfo = type.GetProperty(propertyName);

        if (propertyInfo == null)
            throw new ArgumentOutOfRangeException(nameof(propertyName), string.Format("{0} is not a property of {1}.", (object)propertyName, (object)type.Name));

        var attr = propertyInfo.GetCustomAttribute<T>();

        return attr;
    }
}