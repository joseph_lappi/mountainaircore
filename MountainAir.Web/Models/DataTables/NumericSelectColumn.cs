﻿using System;
using System.Linq.Expressions;

namespace MountainAir.Web.Models.DataTables;

public class NumericSelectColumn<T> : SelectColumn<T>
    where T : class
{
    public NumericSelectColumn(Expression<Func<T, object>> expression, int from, int to) : base(expression)
    {
        for(int i = from; i <= to; i++)
        {
            this.Options.Add(new SelectColumnOption($"{i}", i));
        }
    }
}

public class NumericSelectColumn : SelectColumn
{
    public NumericSelectColumn(int from, int to)
        : base()
    {
        for (int i = from; i <= to; i++)
        {
            this.Options.Add(new SelectColumnOption($"{i}", i));
        }
    }
}