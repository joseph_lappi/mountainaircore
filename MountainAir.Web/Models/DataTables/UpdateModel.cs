﻿using MountainAir.Common.JsonConverters;
using MountainAir.Common.Utility;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MountainAir.Web.Models.DataTables;

public class UpdateModel
{
    [JsonProperty("action")]
    [JsonConverter(typeof(StringEnumConverter))]
    public UpdateActions Action { get; set; }

    [JsonProperty("data")]
    public DataBag Data { get; set; }
}

public class UpdateModel<TKeyType, TObj>
{
    [JsonProperty("action")]
    [JsonConverter(typeof(StringEnumConverter))]
    public UpdateActions Action { get; set; }

    [JsonProperty("data")]
    public DataBag<TKeyType, TObj> Data { get; set; }
}

public class DataBag<TKeyType, TObj> : Dictionary<TKeyType, TObj>
{
}

public class DataBag : Dictionary<string, DataBagEntity>
{
    public List<T> ToObjects<T>()
        where T : new()
    {
        return this.Values.Select(i => i.ToObject<T>()).ToList();
    }

    public void FillObject(string key, object item)
    {
        if (this.TryGetValue(key, out DataBagEntity entity))
        {
            entity.FillObject(item);
        }
    }
}

public class DataBagEntity : Dictionary<string, string>
{
    private string GetKey(string keyName)
    {
        var key = this.Keys.FirstOrDefault(k => k.Equals(keyName, StringComparison.OrdinalIgnoreCase));

        return string.IsNullOrEmpty(key) ? keyName : key;
    }

    public void FillObject(object obj)
    {
        var props = from prop in obj.GetType().GetProperties() where prop.CanRead && prop.CanWrite select prop;

        foreach (PropertyInfo prop in props)
        {
            string fieldName = ReflectionHelper.GetSerializationName(obj.GetType(), prop.Name); 

            if (TryGetValue(GetKey(fieldName), out string value))
            {
                var t = prop.PropertyType;

                string stringValue = value?.ToString();

                if (value == null && t == typeof(string))
                    prop.SetValue(obj, "", null);
                else if (t == typeof(string))
                    prop.SetValue(obj, value, null);
                else if ((t == typeof(int) || t == typeof(int?)) && int.TryParse(stringValue, out int i))
                    prop.SetValue(obj, i, null);
                else if ((t == typeof(decimal) || t == typeof(decimal?)) && decimal.TryParse(stringValue, out decimal de))
                    prop.SetValue(obj, de, null);
                else if ((t == typeof(long) || t == typeof(long?)) && long.TryParse(stringValue, out long l))
                    prop.SetValue(obj, l, null);
                else if ((t == typeof(DateTime) || t == typeof(DateTime?)) && DateTime.TryParse(stringValue, out DateTime dt))
                    prop.SetValue(obj, dt, null);
                else if ((t == typeof(DateTimeOffset) || t == typeof(DateTimeOffset?)) && DateTimeOffset.TryParse(stringValue, out DateTimeOffset dto))
                    prop.SetValue(obj, dto, null);
                else if ((t == typeof(bool) || t == typeof(bool?)) && bool.TryParse(stringValue, out bool b))
                    prop.SetValue(obj, b, null);
                else if ((t == typeof(Guid) || t == typeof(Guid?)) && Guid.TryParse(stringValue, out Guid g))
                    prop.SetValue(obj, g, null);
                else if ((t.BaseType == typeof(Enum) || IsNullableEnum(t)) && Enum.TryParse(t, stringValue, out var enumValue))
                    prop.SetValue(obj, enumValue, null);
                else
                    prop.SetValue(obj, value);
            }
        }
    }

    private static bool IsNullableEnum(Type t)
    {
        Type u = Nullable.GetUnderlyingType(t);
        return (u != null) && u.IsEnum;
    }

    //public bool TryGetStringValue(string key, out string value)
    //{
    //    if (TryGetValue(key, out object obj))
    //    {
    //        value = obj?.ToString();
    //        return true;
    //    }
    //    value = null;
    //    return false;
    //}

    public T ToObject<T>()
        where T : new()
    {
        var newItem = Activator.CreateInstance<T>();

        FillObject(newItem);

        return newItem;
    }
}