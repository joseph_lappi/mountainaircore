﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System.Collections.Generic;

namespace MountainAir.Web.Models.DataTables;

public class DisplayViewModel
{
    public string TableId { get; set; }

    public string DisplayUrl { get; set; }
    
    public List<DisplayColumn> DisplayColumns { get; set; }
}

public class DisplayViewModel<T>
    where T : class
{
    public DisplayViewModel()
    {
    }

    public string TableId => $"tbl{typeof(T).Name}";

    public string DisplayUrl => $"/api/{typeof(T).Name.Replace("Controller", "")}/GetData";

    public List<DisplayColumn> DisplayColumns { get; set; }

    public string DisplayColumnsJson => JsonString(DisplayColumns);

    protected static string JsonString(object value)
    {
        var settings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            NullValueHandling = NullValueHandling.Ignore,
            Formatting = Formatting.None
        };

        return  JsonConvert.SerializeObject(value, settings);
    }
}
