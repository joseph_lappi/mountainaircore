﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MountainAir.Web.Models.DataTables;

public class SelectColumn<T> : EditorColumn<T>
    where T : class
{
    public SelectColumn(Expression<Func<T, object>> expression) : base(expression)
    {
        this.Type = EditorColumnTypes.Select;
    }

    [JsonProperty("options")]
    public List<SelectColumnOption> Options { get; set; } = new();
}

public class SelectColumn : EditorColumn
{
    public SelectColumn()
        : base()
    {
        this.Type = EditorColumnTypes.Select;
    }

    [JsonProperty("options")]
    public List<SelectColumnOption> Options { get; set; } = new();
}
