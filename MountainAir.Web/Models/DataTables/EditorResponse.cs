﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace MountainAir.Web.Models.DataTables;

/// <summary>
/// Return response for the datatables editor.
/// </summary>
/// <see cref="https://editor.datatables.net/manual/server"/>
/// <typeparam name="T"></typeparam>
public class EditorResponse<T>
{
    public EditorResponse() { }

    public EditorResponse(IEnumerable<T> data)
    {
        Data = new List<T>(data);
    }

    /// <summary>
    /// On create: Data of the added / edited row(s)
    /// On edit: Data of the added / edited row(s)
    /// On remove: Not used / not required
    /// When performing the create and edit actions, this parameter will contain the data that 
    /// represents the new or updated rows in the database - i.e.you would query the database, 
    /// after doing the insert, to get the very latest data for just this row.
    /// </summary>
    [JsonProperty("data")]
    public List<T> Data { get; set; } = new List<T>();

    /// <summary>
    /// This property is used to indicate when an error has occurred in processing the data sent by the form submission, 
    /// but is an error which cannot be attributed to any individual field (for which fieldErrors is used). The string 
    /// returned in this parameter is shown to the end user as the error message for the submission. If no error has 
    /// occurred, this parameter can either be omitted entirely or set to be an empty string.
    /// </summary>
    [JsonProperty("error")]
    public string Error { get; set; }

    /// <summary>
    /// This property is used to indicate that one or more fields are in error (typically due to failed validation) and tell Editor what the error message to show to the end user for that field is.
    /// </summary>
    [JsonProperty("fieldErrors")]
    public Dictionary<string, string> FieldErrors { get; set; } = new Dictionary<string, string>();

    [JsonProperty("cancelled")]
    public List<object> Cancelled { get; set; } = new List<object>();
}