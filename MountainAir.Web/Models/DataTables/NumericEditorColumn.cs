﻿using System.Linq.Expressions;
using System;

namespace MountainAir.Web.Models.DataTables;

public class NumericEditorColumn : EditorColumn
{
    public NumericEditorColumn()
    {
        Attributes["type"] = "number";
    }
}

public class NumericEditorColumn<T> : EditorColumn<T>
    where T : class
{
    public NumericEditorColumn(Expression<Func<T, object>> expression)
        : base(expression)
    {
        Attributes["type"] = "number";
    }
}