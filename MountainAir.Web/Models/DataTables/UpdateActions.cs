﻿namespace MountainAir.Web.Models.DataTables;

public enum UpdateActions
{
    Remove,
    Edit,
    Create
}