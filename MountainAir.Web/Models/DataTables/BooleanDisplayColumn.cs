﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace MountainAir.Web.Models.DataTables;

public class BooleanDisplayColumn : DisplayColumn
{
    protected const string RenderFunction = "ma.web.datatables.renders.booleanRender";

    public BooleanDisplayColumn()
        : base()
    {
    }

    public override string Render => RenderFunction;
}

public class BooleanDisplayColumn<T> : DisplayColumn<T>
    where T : class
{
    protected const string RenderFunction = "ma.web.datatables.renders.booleanRender";

    public BooleanDisplayColumn(Expression<Func<T, object>> expression) : base(expression)
    {
    }

    public BooleanDisplayColumn(PropertyInfo propertyInfo) : base(propertyInfo)
    {
    }

    public BooleanDisplayColumn(string fieldName) : base(fieldName)
    {
    }

    public override string Render => RenderFunction;
}