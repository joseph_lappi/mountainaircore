﻿using System;
using System.Linq.Expressions;

namespace MountainAir.Web.Models.DataTables;

public class NumericDisplayColumn : DisplayColumn
{
    protected const string RenderString = "$.fn.dataTable.render.number(',', '.', 2, '', '%')";
    
    public override string Render => RenderString;
}

public class NumericDisplayColumn<T> : DisplayColumn<T>
    where T : class
{
    protected const string RenderString = "$.fn.dataTable.render.number(',', '.', 2, '', '%')";

    public NumericDisplayColumn(Expression<Func<T, object>> expression) : base(expression)
    {
    }

    public override string Render => RenderString;
}