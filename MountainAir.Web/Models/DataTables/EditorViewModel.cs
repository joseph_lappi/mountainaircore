﻿using System.Collections.Generic;
using MountainAir.Common.Utility;
using Newtonsoft.Json;

namespace MountainAir.Web.Models.DataTables;

public class EditorViewModel : DisplayViewModel
{
    public string IdField { get; set; }

    public string EditorUrl { get; set; }
    
    public List<EditorColumn> EditorColumns { get; set; } 
}

public class EditorViewModel<T> : DisplayViewModel<T>
    where T : class
{
    public string IdField => ReflectionHelper.GetKeyColumnName<T>();

    public string EditorUrl => $"/api/{typeof(T).Name.Replace("Controller", "")}/UpdateData";

    public List<EditorColumn> EditorColumns { get; set; } 

    public string EditorColumnsJson => JsonString(EditorColumns);

    public EditorViewModel EditorConfiguration
    {
        get
        {
            var model = new EditorViewModel()
            {
                TableId = this.TableId,
                EditorUrl = this.EditorUrl,
                EditorColumns = this.EditorColumns,
                DisplayColumns = this.DisplayColumns,
                DisplayUrl = this.DisplayUrl,
                IdField = this.IdField
            };
            return model;
        }
    }

    public string EditorConfigurationJson => JsonString(EditorConfiguration);
}
