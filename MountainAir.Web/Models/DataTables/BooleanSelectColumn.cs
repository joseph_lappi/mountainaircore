﻿using System;
using System.Linq.Expressions;

namespace MountainAir.Web.Models.DataTables;

public class BooleanSelectColumn<T> : SelectColumn<T>
    where T : class
{
    public BooleanSelectColumn(Expression<Func<T, object>> expression) 
        : base(expression)
    {
        this.Type = EditorColumnTypes.Select;

        this.Options.Add(new SelectColumnOption("Yes", true));
        this.Options.Add(new SelectColumnOption("No", false));
    }
}

public class BooleanSelectColumn : SelectColumn
{
    public BooleanSelectColumn() 
    {
        this.Type = EditorColumnTypes.Select;

        this.Options.Add(new SelectColumnOption("Yes", true));
        this.Options.Add(new SelectColumnOption("No", false));
    }
}