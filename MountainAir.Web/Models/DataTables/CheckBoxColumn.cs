﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace MountainAir.Web.Models.DataTables;

public class CheckBoxSelectColumn : DisplayColumn
{
    public CheckBoxSelectColumn()
        : base()
    {
        ClassName = "select-checkbox";
        Orderable = false;
        DefaultContent = "";
    }
}