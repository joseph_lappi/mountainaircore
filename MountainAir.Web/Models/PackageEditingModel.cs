﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MountainAir.Common.Data.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using MountainAir.Web.Models.DataTables;

namespace MountainAir.Web.Models;

public class PackageEditingModel
{
    public Package Package { get; set; }

    public List<EditorSelectOption> CatalogItems { get; set; } = new();

    public string CatalogItemsJson => JsonConvert.SerializeObject(CatalogItems);
}

public class EditorSelectOption
{
    [JsonProperty("label")]
    public string Label { get; set; }
        
    [JsonProperty("value")]
    public object Value { get; set; }
}

public class CatalogItemViewModel
{
    public int CategoryId { get; set; }

    public string CategoryName { get; set; }

    public int CatalogItemId { get; set; }

    public string CatalogNum { get; set; }

    public string ModelNum { get; set; }
}