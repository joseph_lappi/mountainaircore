﻿namespace MountainAir.Web.Models;

public class PhotoViewModel
{
    public string EstimateId { get; internal set; }

    public string Name { get; internal set; }

    public string Url { get; internal set; }
}