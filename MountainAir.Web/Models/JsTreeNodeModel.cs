﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MountainAir.Web.Models;

[DataContract]
public class JsTreeNodeModel
{
    [DataMember(EmitDefaultValue = false)]
    public string Id { get; set; }

    [DataMember(EmitDefaultValue = false)]
    public string Text { get; set; }

    [DataMember(EmitDefaultValue = false)]
    public string Parent { get; set; }

    [DataMember(EmitDefaultValue = false)]
    public string Icon { get; set; }

    [DataMember(EmitDefaultValue = false)]
    public string[] li_attr { get; set; }

    [DataMember(EmitDefaultValue = false)]
    public string[] a_attr { get; set; }

    [DataMember(EmitDefaultValue = false)]
    public Dictionary<JsTreeNodeStates, bool> State { get; set; } = new Dictionary<JsTreeNodeStates, bool>();

    [DataMember(EmitDefaultValue = false)]
    public object Children { get; set; }

    [DataMember(EmitDefaultValue = false)]
    public string Type { get; set; }
}