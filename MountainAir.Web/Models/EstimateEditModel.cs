﻿using Microsoft.AspNetCore.Mvc.Rendering;
using MountainAir.Common.Data.Models;
using System.Collections.Generic;

namespace MountainAir.Web.Models;

public class EstimateEditModel
{
    public Estimate Estimate { get; set; }

    public SelectList AssignedTo { get; set; }

    public Dictionary<int, string> Sections
    {
        get => new Dictionary<int, string>()
        {
            { 0, "Heating" },
            { 1, "Cooling" },
            { 2, "No Markup" },
            { 3, "Incentives" }
        };
    }
}