﻿using System;

namespace MountainAir.Web.Models;

public enum StatusLevels
{
    Success,
    Info,
    Warn,
    Error
}

public static class StatusLevelExtensions
{
    public static string ToCssLevel(this StatusLevels statusLevels)
    {
        switch (statusLevels)
        {
            case StatusLevels.Success:
                return "success";
            case StatusLevels.Info:
                return "info";
            case StatusLevels.Warn:
                return "warning";
            case StatusLevels.Error:
                return "danger";
            default:
                throw new ArgumentOutOfRangeException(nameof(statusLevels), statusLevels, null);
        }
    }
}

public class StatusMessageViewModel
{
    public string Message { get; set; }

    public StatusLevels Level { get; set; }

    public bool AutoHide { get; set; } = false;
}