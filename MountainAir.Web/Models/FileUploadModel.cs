﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MountainAir.Web.Models;

/// <summary>
/// 
/// </summary>
public class FileUploadModel
{
    /// <summary>
    /// 
    /// </summary>
    [Required]
    [Display(Name = "File to Upload")]
    public IFormFile File { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [Required]
    [Display(Name = "Category")]
    public int CategoryId { get; set; }
    public SelectList Categories { get; internal set; }

    ///// <summary>
    ///// 
    ///// </summary>
    //public Category[] Categories { get; set; }
}