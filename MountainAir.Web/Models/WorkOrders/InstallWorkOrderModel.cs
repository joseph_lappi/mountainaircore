﻿using MountainAir.Common.Data.Models;
using System.Collections.Generic;

namespace MountainAir.Web.Models.WorkOrders;

/// <summary>
/// 
/// </summary>
public class InstallWorkOrderModel
{
    /// <summary>
    /// 
    /// </summary>
    public Customer Customer { get; set; }
        
    /// <summary>
    /// 
    /// </summary>
    public Address InstallAddress { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public Address BillingAddress { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public List<OptionItem> OptionItems { get; set; } = new List<OptionItem>();
                
    /// <summary>
    /// 
    /// </summary>
    public Estimate Estimate { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public decimal TotalLabor { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public List<Category> Categories { get; internal set; } = new List<Category>();
}