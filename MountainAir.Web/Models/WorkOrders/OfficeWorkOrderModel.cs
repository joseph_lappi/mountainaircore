﻿using MountainAir.Common.Data.Models;
using System.Collections.Generic;

namespace MountainAir.Web.Models.WorkOrders;

/// <summary>
/// 
/// </summary>
public class OfficeWorkOrderModel
{
    /// <summary>
    /// 
    /// </summary>
    public Customer Customer { get; set; }
        
    /// <summary>
    /// 
    /// </summary>
    public Address InstallAddress { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public Address BillingAddress { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public IEnumerable<OptionItem> OptionItems { get; set; }
                
    /// <summary>
    /// 
    /// </summary>
    public Estimate Estimate { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public decimal TotalLabor { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public decimal TotalPrice { get; internal set; }

    /// <summary>
    /// 
    /// </summary>
    public decimal TotalCost { get; internal set; }
}