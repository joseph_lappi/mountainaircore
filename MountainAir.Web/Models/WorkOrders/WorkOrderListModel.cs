﻿using MountainAir.Common.Data.Models;

namespace MountainAir.Web.Models.WorkOrders;

/// <summary>
/// 
/// </summary>
public class WorkOrderListModel
{
    /// <summary>
    /// 
    /// </summary>
    public Customer Customer { get; internal set; }

    /// <summary>
    /// 
    /// </summary>
    public Estimate Estimate { get; internal set; }
}