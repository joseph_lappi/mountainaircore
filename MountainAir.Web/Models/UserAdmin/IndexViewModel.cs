﻿using MountainAir.Common.Security;
using System;

namespace MountainAir.Web.Models.UserAdmin;

public class IndexUserModel
{
    public IndexUserModel() { }

    public IndexUserModel(ApplicationUser user)
    {
        if (user == null)
            return;

        FirstName = user.FirstName;
        LastName = user.LastName;
        Email = user.Email;
        UserName = user.UserName;
        Active = user.Active;
        Id = user.Id;
        DefaultLaborRate = user.DefaultLaborRate;
        DefaultProfitMargin = user.DefaultProfitMargin;
        DefaultSalesTax = user.DefaultSalesTax;
        PhoneNumber = user.PhoneNumber;
    }

    public Guid Id { get; set; }

    public decimal? DefaultLaborRate { get; set; }
        
    public decimal? DefaultProfitMargin { get; set; }
        
    public decimal? DefaultSalesTax { get; set; }

    public string PhoneNumber { get; set; }
        
    public string FirstName { get; set; }

    public string LastName { get; set; }

    public string Email { get; set; }

    public string UserName { get; set; }

    public bool Active { get; set; }
        
    public string NewPasswordValue { get; set; }
}