﻿using MountainAir.Common.Data.Models;
using System.Collections.Generic;

namespace MountainAir.Web.Models;

public class EstimateListModel
{
    public IEnumerable<vEstimateList> Estimates { get; set; }

    public string SearchTerm { get; set; }

    public bool Active { get; set; } = true;
}