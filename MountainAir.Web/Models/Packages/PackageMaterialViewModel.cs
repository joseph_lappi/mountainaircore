﻿namespace MountainAir.Web.Models.Packages;

public class PackageMaterialViewModel
{
    public int? Labor { get; set; }

    public decimal? Price { get; set; }

    public int Quantity { get; set; }

    public string Description { get; set; }

    public string CatalogNumber { get; set; }

    public string ModelNumber { get; set; }

    public int PackageMaterialId { get; set; }

    public bool Active { get; set; }
}