﻿using System;
using System.Linq;

namespace MountainAir.Web.Models.RoleAdmin;

/// <summary>
/// 
/// </summary>
public class RoleAdminViewModel
{
    public RoleModel[] Roles { get; internal set; }
}