﻿using MountainAir.Common.Security;
using System;
using System.ComponentModel.DataAnnotations;

namespace MountainAir.Web.Models.RoleAdmin;

public class UserModel
{
    public UserModel() { }

    public UserModel(ApplicationUser i)
    {
        if (i == null)
            return;

        this.Active = i.Active;
        this.Email = i.Email;
        this.FirstName = i.FirstName;
        this.LastName = i.LastName;
        this.PhoneNumber = i.PhoneNumber;
        this.UserId = i.Id;
        this.UserName = i.UserName;
    }

    public bool Assigned { get; set; }

    public bool OriginalAssigned { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [Display(Name = "UserName")]
    public string UserName { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [Display(Name = "Email")]
    public string Email { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [Display(Name = "First Name")]
    public string FirstName { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [Display(Name = "Last Name")]
    public string LastName { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [Display(Name = "Phone Number")]
    public string PhoneNumber { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [Display(Name = "Active")]
    public bool Active { get; set; }

    /// <summary>
    /// 
    /// </summary>
    [Display(Name = "UserId")]
    public Guid UserId { get; set; }
}