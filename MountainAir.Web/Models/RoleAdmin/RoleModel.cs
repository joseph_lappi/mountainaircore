﻿using Microsoft.AspNetCore.Identity;
using MountainAir.Common.Security;
using System;
using System.Collections.Generic;

namespace MountainAir.Web.Models.RoleAdmin;

public class RoleModel
{
    public RoleModel() { }

    public RoleModel(ApplicationRole r)
    {
        this.Id = r.Id;
        this.Name = r.Name;
    }

    public RoleModel(ApplicationRole r, IEnumerable<UserModel> users)
    {
        this.Id = r.Id;
        this.Name = r.Name;
        this.Users.AddRange(users);
    }

    public Guid Id { get; set; }

    public string Name { get; set; }

    public List<UserModel> Users { get; set; } = new List<UserModel>();
}