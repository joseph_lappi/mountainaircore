﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using MountainAir.Common.Data.Models;

namespace MountainAir.Web.Models;

public class AddPackageItemViewModel
{
    public Package Package { get; set; }

    public SelectList Categories { get; set; }

    public PackageItem PackageItem { get; internal set; }
}

public class AddPackageMaterialViewModel
{
    public Package Package { get; set; }

    public SelectList Categories { get; set; }

    public PackageItem PackageItem { get; internal set; }

    public CatalogItem CatalogItem { get; internal set; }
}

public class CategorySelectionModel 
{
    public int CategoryId { get; set; }

    public string CategoryName { get; set; }

    public string Group { get; set; }
}