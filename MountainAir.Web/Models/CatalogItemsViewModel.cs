﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc.Rendering;
using MountainAir.Common.Data.Models;
using MountainAir.Web.Models.DataTables;
using Newtonsoft.Json;

namespace MountainAir.Web.Models;

public class CatalogItemsViewModel
{
    public List<Category> Categories { get; set; }

    private List<CategoryViewModel> TransformedData
    {
        get
        {
            var list = Categories.Select(c => new CategoryViewModel(c.CategoryId, c.CategoryName, c.Group)).ToList();

            list.Insert(0, new CategoryViewModel(0, "--All--", "All"));

            return list;
        }
    }

    public string CategoryOptionsJson => JsonConvert.SerializeObject(CategoryOptions);

    public List<SelectColumnOption> CategoryOptions => GetCategoryOptions();

    private List<SelectColumnOption> GetCategoryOptions()
    {
        var options = Categories.Select(c => new SelectColumnOption(GetName(c), c.CategoryId)).OrderBy(c => c.Label).ToList();

        return options;
    }

    public SelectList CategoriesSelectList => new SelectList(TransformedData, nameof(CategoryViewModel.CategoryId), nameof(CategoryViewModel.CategoryName), null, nameof(CategoryViewModel.Group));

    private static string GetName(Category c)
    {
        string prefix = c.MaterialCategory ? "Material" : "Equipment";
        return $"{prefix} - {c.CategoryName}";
    }
}

public class CategoryViewModel
{
    public CategoryViewModel(int categoryId, string categoryName, string group)
    {
        CategoryId = categoryId;
        CategoryName = categoryName;
        Group = group;
    }

    public int CategoryId { get; set; }

    public string CategoryName { get; set; }

    public string Group { get; set; }
}