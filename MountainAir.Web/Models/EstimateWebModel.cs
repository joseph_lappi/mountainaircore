﻿using MountainAir.Common.Data.Models;
using MountainAir.Common.Security;

namespace MountainAir.Web.Models;

/// <summary>
/// 
/// </summary>
public class EstimateWebModel
{
    /// <summary>
    /// 
    /// </summary>
    public Estimate Estimate { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public ApplicationUser AssignedTo { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public Customer Customer { get; set; }
}